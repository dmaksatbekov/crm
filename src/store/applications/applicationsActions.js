import axios from '../../Api/axios_api';
import {
    ROUTE_ADVERTISEMENT,
    ROUTE_BLOGGER_APPLICATIONS,
    ROUTE_COMPANY_APPLICATIONS,
    ROUTE_SOCIAL_NETWORKS
} from "../../Api/Routes";
import {ERROR_TEXT} from "../../global/constants";
import {NotificationManager} from 'react-notifications';
import {history} from "../configureStore";

const ROUTE_TYPES = {
    company: ROUTE_COMPANY_APPLICATIONS,
    blogger: ROUTE_BLOGGER_APPLICATIONS
}

// Company

export const FETCH_APPLICATIONS_COMPANY_LIST_SUCCESS = 'FETCH_APPLICATIONS_COMPANY_LIST_SUCCESS';
export const FETCH_APPLICATIONS_COMPANY_LIST_FAILURE = 'FETCH_APPLICATIONS_COMPANY_LIST_FAILURE';

const fetchApplicationsCompanyListSuccess = applications => ({
    type: FETCH_APPLICATIONS_COMPANY_LIST_SUCCESS,
    applications
});
const fetchApplicationsCompanyListFailure = error => ({type: FETCH_APPLICATIONS_COMPANY_LIST_FAILURE, error});

export const fetchCompanyApplications = (page = 1, status = "", search, date, is_archive = false) => {
    let url = `?page=${page}&is_archive=${is_archive}`;
    if (status && status !== "") {
        url += `&request_status=${status}`
    }
    if (date) {
        url += `&created_date=${date}`
    }
    if (search && search !== "") {
        url += `&search=${search}`
    }

    return dispatch => {
        return axios.get(ROUTE_COMPANY_APPLICATIONS + url).then(
            response => {
                dispatch(fetchApplicationsCompanyListSuccess(response.data))
            }, error => {
                dispatch(fetchApplicationsCompanyListFailure(error.response))
            }
        ).catch(err => console.log(err))
    }
}

export const fetchCreateApplicationCompany = data => {
    return dispatch => {
        return axios.post(ROUTE_COMPANY_APPLICATIONS, data).then(
            () => {
                NotificationManager.success('Заявка успешно создана');
                dispatch(fetchCompanyApplications(1, "N"))
            },
            err => {
                NotificationManager.error(ERROR_TEXT);
            }
        ).catch(err => console.log(err))
    }
}

export const fetchEditApplicationCompany = (id, data) => {
    return dispatch => {
        return axios.put(ROUTE_COMPANY_APPLICATIONS + id + "/", data).then(
            (response) => {
                NotificationManager.success('Заявка успешно изменена');
                dispatch(fetchCompanyApplications(1,response.data.request_status))
            },
            err => {
                NotificationManager.error(ERROR_TEXT);
            }
        ).catch(err => console.log(err))
    }
}

export const fetchEditApplicationStatus = (id, status, type = "company") => {
    return dispatch => {
        return axios.put(ROUTE_TYPES[type] + id + "/", {request_status: status}).then(
            () => {
                NotificationManager.success('Статус заявки успешно изменен');
            },
            err => {
                NotificationManager.error(ERROR_TEXT);
            }
        ).catch(err => console.log(err))
    }
}

export const fetchEditApplicationArchive = (id, type = "company") => {
    return dispatch => {
        return axios.put(ROUTE_TYPES[type] + id + "/", {is_archive: true}).then(
            () => {
                NotificationManager.success('Заявка успешно отправлена в архив');
                dispatch(fetchBloggerApplications())
                dispatch(fetchCompanyApplications())
            },
            err => {
                NotificationManager.error(ERROR_TEXT);
            }
        ).catch(err => console.log(err))
    }
}

export const fetchSendToApplication = (id, type = "company") => {
    return dispatch => {
        return axios.put(ROUTE_TYPES[type] + id + "/", {is_archive: false}).then(
            () => {
                NotificationManager.success('Заявка успешно отправлена в заявки');
                dispatch(fetchBloggerApplications())
                dispatch(fetchCompanyApplications())
            },
            err => {
                NotificationManager.error(ERROR_TEXT);
            }
        ).catch(err => console.log(err))
    }
}

export const fetchCreateAdvertiseCompany = data => {
    return dispatch => {
        return axios.post(ROUTE_ADVERTISEMENT, data).then(
            response => {
                history.push("/advertisement/" + response.data.id)
            },
            err => {
                NotificationManager.error(ERROR_TEXT);
            }
        ).catch(err => console.log(err))
    }
}

// Bloggers
export const FETCH_APPLICATIONS_BLOGGER_LIST_SUCCESS = 'FETCH_APPLICATIONS_BLOGGER_LIST_SUCCESS';
export const FETCH_APPLICATIONS_BLOGGER_LIST_FAILURE = 'FETCH_APPLICATIONS_BLOGGER_LIST_FAILURE';
const fetchApplicationsBloggerListSuccess = applications => ({
    type: FETCH_APPLICATIONS_BLOGGER_LIST_SUCCESS,
    applications
});
const fetchApplicationsBloggerListFailure = error => ({type: FETCH_APPLICATIONS_BLOGGER_LIST_FAILURE, error});

export const fetchBloggerApplications = (page = 1, status = "", search, date, is_archive = false) => {
    let url = `?page=${page}&is_archive=${is_archive}`;
    if (status && status !== "") {
        url += `&request_status=${status}`
    }
    if (date) {
        url += `&created_date=${date}`
    }
    if (search && search !== "") {
        url += `&search=${search}`
    }

    return dispatch => {
        return axios.get(ROUTE_BLOGGER_APPLICATIONS + url).then(
            response => {
                dispatch(fetchApplicationsBloggerListSuccess(response.data))
            }, error => {
                dispatch(fetchApplicationsBloggerListFailure(error.response))
            }
        ).catch(err => console.log(err))
    }
}

export const fetchCreateApplicationBlogger = (data) => {
    return dispatch => {
        return axios.post(ROUTE_BLOGGER_APPLICATIONS, data).then(
            () => {
                NotificationManager.success('Заявка успешно создана');
                dispatch(fetchBloggerApplications())
            },
            err => {
                NotificationManager.error(ERROR_TEXT);
            }
        ).catch(err => console.log(err))
    }
}

export const fetchEditApplicationBlogger = (id, data) => {
    return dispatch => {
        return axios.put(ROUTE_BLOGGER_APPLICATIONS + id + "/", data).then(
            () => {
                NotificationManager.success('Заявка успешно изменена');
                dispatch(fetchBloggerApplications())
            },
            err => {
                NotificationManager.error(ERROR_TEXT);
            }
        ).catch(err => console.log(err))
    }
}

export const fetchRemoveApplication = (id, type) => {
    return dispatch => {
        axios.delete(ROUTE_TYPES[type] + id + "/").then(
            () => {
                dispatch(fetchBloggerApplications())
                dispatch(fetchCompanyApplications())
                NotificationManager.success('Заявка успешно удалена');
            }
        ).catch(err => console.log(err))
    }
}

export const FETCH_SOCIAL_NETWORK_LIST_SUCCESS = 'FETCH_SOCIAL_NETWORK_LIST_SUCCESS'

const fetchSocialNetworkListSuccess = socials => ({type: FETCH_SOCIAL_NETWORK_LIST_SUCCESS, socials});

export const fetchSocialNetworks = () => {
    return dispatch => {
        return axios.get(ROUTE_SOCIAL_NETWORKS).then(
            response => {
                dispatch(fetchSocialNetworkListSuccess(response.data))
            }
        ).catch(err => console.log(err))
    }
}

export const FETCH_APPLICATION_BY_ID = 'FETCH_APPLICATION_BY_ID';
const fetchApplicationByIdSuccess = application => ({type: FETCH_APPLICATION_BY_ID, application});
export const fetchApplicationById = id => {
    return dispatch => {
        return axios.get(ROUTE_COMPANY_APPLICATIONS + id + "/").then(
            response => {
                dispatch(fetchApplicationByIdSuccess(response.data))
            },
            err => {
                NotificationManager.error(ERROR_TEXT);
            }
        ).catch(err => console.log(err))
    }
}

export const SET_NOTIFICATIONS = 'SET_NOTIFICATIONS';
export const setNotifications = data => ({type: SET_NOTIFICATIONS, data})