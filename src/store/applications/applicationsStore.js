import {
    FETCH_APPLICATION_BY_ID,
    FETCH_APPLICATIONS_BLOGGER_LIST_FAILURE,
    FETCH_APPLICATIONS_BLOGGER_LIST_SUCCESS,
    FETCH_APPLICATIONS_COMPANY_LIST_FAILURE,
    FETCH_APPLICATIONS_COMPANY_LIST_SUCCESS,
    FETCH_SOCIAL_NETWORK_LIST_SUCCESS, SET_NOTIFICATIONS
} from "./applicationsActions";

const initialState = {
    companyApplicationsList: null,
    companyFailure: null,

    bloggerApplications: null,
    bloggersFailure: null,

    socialNetworks: null,

    application: null,

    notifications: null
}


export default function applicationsReducer
    (
        state = initialState,
        action
    ) {
    switch (action.type) {
        case FETCH_APPLICATIONS_COMPANY_LIST_SUCCESS:
            return {...state, companyApplicationsList: action.applications, companyFailure: null}
        case FETCH_APPLICATIONS_COMPANY_LIST_FAILURE:
            return {...state, companyFailure: action.error}

        case FETCH_APPLICATIONS_BLOGGER_LIST_SUCCESS:
            return {...state, bloggerApplications: action.applications, bloggersFailure: null};
        case FETCH_APPLICATIONS_BLOGGER_LIST_FAILURE:
            return {...state, bloggersFailure: action.error}

        case FETCH_SOCIAL_NETWORK_LIST_SUCCESS:
            return {...state, socialNetworks: action.socials}

        case FETCH_APPLICATION_BY_ID:
            return {...state, application: action.application}

        case SET_NOTIFICATIONS:
            return {...state, notifications: action.data}

        default: {
            return state;
        }
    }
}