import axios from '../../Api/axios_api';
import {NotificationManager} from 'react-notifications';
import {
    ROUTE_ADVERTISEMENT_TYPES,
    ROUTE_AUDIENCE_AGE, ROUTE_BLOGGER_FILTERS, ROUTE_BLOGGER_OFFER,
    ROUTE_BLOGGER_STATUSES,
    ROUTE_BLOGGERS, ROUTE_CHECK_TELEGRAM_LOGIN,
    ROUTE_CONTENT_TYPES,
    ROUTE_COUNTRIES
} from "../../Api/Routes";
import {ERROR_TEXT} from "../../global/constants";
import {fetchBloggerApplications} from "../applications/applicationsActions";
import {history} from "../configureStore";
import {dateFormatter} from "../../global/helpers";

export const FETCH_BLOGGERS_REQUEST = 'FETCH_BLOGGERS_REQUEST';
export const FETCH_BLOGGERS_SUCCESS = 'FETCH_BLOGGERS_SUCCESS';
export const FETCH_BLOGGERS_FAILURE = 'FETCH_BLOGGERS_FAILURE';
const fetchBloggersRequest = () => ({type: FETCH_BLOGGERS_REQUEST});
const fetchBloggersSuccess = bloggers => ({type: FETCH_BLOGGERS_SUCCESS, bloggers});
const fetchBloggersFailure = error => ({type: FETCH_BLOGGERS_FAILURE, error});
export const fetchBloggers = (filters) => {
    let url = `?social_network=${filters.social || ""}`
    url += `&page=${filters.page || "1"}`;
    url += `&price=${filters.price || ""}`;
    url += `&country=${filters.country || ""}`
    url += `&order=${filters.rating || ""}`
    url += `&tags=${filters.tags || ""}`
    url += `&search=${filters.search || ""}`
    url += `&telegram_status=${filters.telegram_status || ""}`
    if (filters.date) {
        url += `?date=${dateFormatter(filters.date)}`
    }
    if (filters.all_pages) {
        url += `&page_size=${filters.all_pages}`
    }
    return dispatch => {
        dispatch(fetchBloggersRequest())
        return axios.get(ROUTE_BLOGGERS + url).then(
            response => {
                dispatch(fetchBloggersSuccess(response.data))
            }, err => {
                dispatch(fetchBloggersFailure(err.response))
            }
        ).catch(err => console.log(err))
    }
}

export const FETCH_BLOGGER_STATUSES = 'FETCH_BLOGGER_STATUSES';
const fetchBloggerStatusesSuccess = statuses => ({type: FETCH_BLOGGER_STATUSES, statuses});
export const fetchBloggerStatuses = () => {
    return dispatch => {
        return axios.get(ROUTE_BLOGGER_STATUSES).then(
            response => {
                dispatch(fetchBloggerStatusesSuccess(response.data))
            },
            err => {
                NotificationManager.error("Не удалось загрузить статусы блогеров");
            }
        ).catch(err => console.log(err))
    }
}

export const FETCH_COUNTRIES_STATUSES = 'FETCH_COUNTRIES_STATUSES';
const fetchCountriesSuccess = countries => ({type: FETCH_COUNTRIES_STATUSES, countries});
export const fetchCountriesList = () => {
    return dispatch => {
        return axios.get(ROUTE_COUNTRIES).then(
            response => {
                dispatch(fetchCountriesSuccess(response.data))
            },
            err => {
                NotificationManager.error("Не удалось загрузить страны");
            }
        ).catch(err => console.log(err))
    }
}

export const FETCH_CONTENT_TYPES = 'FETCH_CONTENT_TYPES';
const fetchContentTypesSuccess = content => ({type: FETCH_CONTENT_TYPES, content});
export const fetchContentTypes = () => {
    return dispatch => {
        return axios.get(ROUTE_CONTENT_TYPES).then(
            response => {
                dispatch(fetchContentTypesSuccess(response.data))
            }
        ).catch(err => console.log(err))
    }
}

export const FETCH_AUDIENCE_AGE = 'FETCH_AUDIENCE_AGE';
const fetchAudienceAgesSuccess = data => ({type: FETCH_AUDIENCE_AGE, data});
export const fetchAudienceAges = () => {
    return dispatch => {
        return axios.get(ROUTE_AUDIENCE_AGE).then(
            response => {
                dispatch(fetchAudienceAgesSuccess(response.data))
            }
        ).catch(err => console.log(err))
    }
}


export const FETCH_ADVERTISEMENT_TYPES = 'FETCH_ADVERTISEMENT_TYPES';
const fetchAdvertTypesSuccess = types => ({type: FETCH_ADVERTISEMENT_TYPES, types});
export const fetchAdvertTypes = () => {
    return dispatch => {
        return axios.get(ROUTE_ADVERTISEMENT_TYPES).then(
            response => {
                dispatch(fetchAdvertTypesSuccess(response.data))
            }
        ).catch(err => console.log(err))
    }
}

export const FETCH_CREATE_BLOGGER_FAILURE = 'FETCH_CREATE_BLOGGER_FAILURE';
const fetchCreateBloggerFailure = error => ({type: FETCH_CREATE_BLOGGER_FAILURE, error})
export const fetchCreateBlogger = (data) => {
    return dispatch => {
        return axios.post(ROUTE_BLOGGERS, data).then(
            (response) => {
                dispatch(fetchBloggers({}))
                dispatch(fetchBloggerApplications())
                NotificationManager.success("Блогер успешно создан");
            }, (error) => {
                dispatch(fetchCreateBloggerFailure(error.response.data))
                NotificationManager.error(error.response.data.telegram_login || ERROR_TEXT);
            }
        ).catch(err => console.log(err))
    }
}

export const FETCH_CLEAR_BLOGGER_STORE = 'FETCH_CLEAR_BLOGGER_STORE';
export const fetchClearBloggerStore = () => ({type: FETCH_CLEAR_BLOGGER_STORE});

export const fetchEditBlogger = (id, data) => {
    return dispatch => {
        return axios.put(ROUTE_BLOGGERS + id + "/", data).then(
            () => {
                NotificationManager.info("Данные успешно изменены");
                // dispatch(fetchClearBloggerStore())
            },
            err => {
                // dispatch(fetchClearBloggerStore())
                NotificationManager.error(ERROR_TEXT);
            }
        ).catch(err => console.log(err))
    }
}

export const FETCH_BLOGGER_FILTER_OPTIONS = 'FETCH_BLOGGER_FILTER_OPTIONS';
const fetchFilterOptions = filters => ({type: FETCH_BLOGGER_FILTER_OPTIONS, filters});
export const fetchBloggerFilterOptions = () => {
    return dispatch => {
        return axios.get(ROUTE_BLOGGER_FILTERS).then(
            response => {
                dispatch(fetchFilterOptions(response.data))
            },
            err => {
                NotificationManager.error('Не удалось загрузить фильтры');
            }
        ).catch(err => console.log(err))
    }
}

export const FETCH_BLOGGER_BY_ID_REQUEST = 'FETCH_BLOGGER_BY_ID_REQUEST';
export const FETCH_BLOGGER_BY_ID_SUCCESS = 'FETCH_BLOGGER_BY_ID_SUCCESS';
export const FETCH_BLOGGER_BY_ID_FAILURE = 'FETCH_BLOGGER_BY_ID_FAILURE';
const fetchBloggerRequest = () => ({type: FETCH_BLOGGER_BY_ID_REQUEST});
const fetchBloggerSuccess = blogger => ({type: FETCH_BLOGGER_BY_ID_SUCCESS, blogger});
const fetchBloggerFailure = (error) => ({type: FETCH_BLOGGER_BY_ID_FAILURE, error})
export const fetchBloggerById = id => {
    return dispatch => {
        dispatch(fetchBloggerRequest())
        return axios.get(ROUTE_BLOGGERS + id + "/").then(
            response => {
                dispatch(fetchBloggerSuccess(response.data))
            }, err => {
                fetchBloggerFailure(err.response)
            }
        ).catch(err => console.log(err))
    }
}

export const CLEAR_BLOGGER_INFO = 'CLEAR_BLOGGER_INFO';
export const clearBloggerInfo = () => ({type: CLEAR_BLOGGER_INFO});

export const fetchRemoveBlogger = bloggerId => {
    return dispatch => {
        return axios.delete(ROUTE_BLOGGERS + bloggerId).then(
            () => {
                NotificationManager.success("Блогер успешно удален");
            },
            err => {
                NotificationManager.error(ERROR_TEXT);
            }
        ).catch(err => console.log(err))
    }
}

export const createBloggerRemark = (id, text) => {
    return dispatch => {
        return axios.put(ROUTE_BLOGGERS + id + "/", text).then(
            () => {
                dispatch(fetchBloggerById(id))
            },
            err => {
                NotificationManager.error(ERROR_TEXT);
            }
        ).catch(err => console.log(err))
    }
}


export const FETCH_BLOGGER_OFFER = 'FETCH_BLOGGER_OFFER';
const fetchBloggerOfferSuccess = offer => ({type: FETCH_BLOGGER_OFFER, offer});

export const fetchBloggerOffer = id => {
    return dispatch => {
        return axios.get(ROUTE_BLOGGER_OFFER + id + "/").then(
            response => {
                dispatch(fetchBloggerOfferSuccess(response.data))
            }, err => {
                NotificationManager.error(ERROR_TEXT);
                history.push("/")
            }
        ).catch(err => console.log(err))
    }
}

export const FETCH_CHECK_TELEGRAM_LOGIN = 'FETCH_CHECK_TELEGRAM_LOGIN';
const checkTelegramLogin = existsLogin => ({type: FETCH_CHECK_TELEGRAM_LOGIN, existsLogin});

export const fetchCheckTelegramLogin = login => {
    return dispatch => {
        return axios.post(ROUTE_CHECK_TELEGRAM_LOGIN, login).then(
            response => {
                dispatch(checkTelegramLogin(response.data.exists))
            }
        ).catch(err => console.log(err))
    }
}