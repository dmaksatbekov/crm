import {
    FETCH_ADVERTISEMENT_REQUEST, FETCH_ADVERTISEMENTS_REQUEST,
    FETCH_ADVERTISEMENTS_SUCCESS,
    FETCH_GET_ADVERTISE_BY_ID
} from "./advertisementActions";

const initialState = {
    requested: false,
    advertise: null,

    advertisements: null
}

export default function advertisementReducer
    (
        state = initialState,
        action
    ) {
    switch (action.type) {
        case FETCH_ADVERTISEMENT_REQUEST:
            return {...state, requested: true};
        case FETCH_GET_ADVERTISE_BY_ID:
            return {...state, advertise: action.data, requested: false};
        case FETCH_ADVERTISEMENTS_REQUEST:
            return {requested: true}
        case FETCH_ADVERTISEMENTS_SUCCESS:
            return {...state, advertisements: action.data, requested: false}
        default: {
            return state;
        }
    }
}