import {history} from "./configureStore";
import {ROUTE_AUTH} from "../global/Routes";

export const saveState = state => {
    try {
        const serializedState = JSON.stringify(state);
        localStorage.setItem('bl_key', serializedState);
    } catch (e) {
        console.log('Could not save state')
    }
};

export const loadState = () => {
    try {
        const serializedStorage = localStorage.getItem('bl_key');
        if (serializedStorage === null) {
            return undefined
        }

        return JSON.parse(serializedStorage)

    } catch (e) {
        return undefined
    }
};
