import axios from '../../Api/axios_api';
import {ROUTE_USER_AUTHORIZATION, ROUTE_USER_LOGOUT} from "../../Api/Routes";
import {history} from "../configureStore";
import {ROUTE_ADMIN, ROUTE_AUTH} from "../../global/Routes";

export const FETCH_USER_AUTHORIZATION_SUCCESS = 'FETCH_USER_AUTHORIZATION_SUCCESS';
export const FETCH_USER_AUTHORIZATION_FAILURE = 'FETCH_USER_AUTHORIZATION_FAILURE';

const fetchUserAuthSuccess = auth => ({type: FETCH_USER_AUTHORIZATION_SUCCESS, auth});
const fetchUserAuthFailure = error => ({type: FETCH_USER_AUTHORIZATION_FAILURE, error});

export const fetchUserAuthorization = data => {
    return dispatch => {
        return axios.post(ROUTE_USER_AUTHORIZATION, data).then(
            response => {
                dispatch(fetchUserAuthSuccess(response.data));
                history.push(ROUTE_ADMIN)
            },
            err => {
                dispatch(fetchUserAuthFailure(err.response.data))
            }
        ).catch(err => console.log(err))
    }
}

export const FETCH_USER_LOGOUT = 'FETCH_USER_LOGOUT';
const fetchUserLogout = () => ({type: FETCH_USER_LOGOUT})
export const fetchLogoutUser = () => {
    return dispatch => {
        return axios.post(ROUTE_USER_LOGOUT).then(
            () => {
                dispatch(fetchUserLogout())
            },
            () => {
                dispatch(fetchUserLogout())
            }
        ).catch(err => console.log(err))
    }
}