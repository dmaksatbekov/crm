import {
    FETCH_LANDING_BLOGGERS_SUCCESS,
    FETCH_LANDING_FAQ_SUCCESS,
    FETCH_LANDING_PARTNERS_SUCCESS, FETCH_LANDING_PROMO_BLOGGERS_SUCCESS,
    FETCH_LANDING_SUCCESS, FETCH_SEO
} from "./landingActions";
import {FETCH_PROJECT_SOCIAL_NETWORKS} from "../settings/settingsActions";

const initialState = {
    landingInfo: null,
    bloggers: null,
    partners: null,
    questions: null,
    promoBloggers: null,
    socials: null,

    seo: null
}

export default function landingReducer(state = initialState, action) {
    switch (action.type) {
        case FETCH_LANDING_SUCCESS:
            return {...state, landingInfo: action.data}
        case FETCH_LANDING_BLOGGERS_SUCCESS:
            return {...state, bloggers: action.bloggers}
        case FETCH_LANDING_PARTNERS_SUCCESS:
            return {...state, partners: action.partners}
        case FETCH_LANDING_FAQ_SUCCESS:
            return {...state, questions: action.data}
        case FETCH_LANDING_PROMO_BLOGGERS_SUCCESS:
            return {...state, promoBloggers: action.data}
        case FETCH_PROJECT_SOCIAL_NETWORKS:
            return {...state, socials: action.data}

        case FETCH_SEO:
            return {...state, seo: action.metaData}
        default:
            return state;
    }
}