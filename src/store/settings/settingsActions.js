import axios from '../../Api/axios_api';
import {
    ROUTE_ADVERTISEMENT_TYPES, ROUTE_AUDIENCE_AGE,
    ROUTE_BLOGGER_STATUS,
    ROUTE_CONTENT_TYPES, ROUTE_PROJECT_SOCIALS_GET,
    ROUTE_SOCIAL_NETWORKS
} from "../../Api/Routes";
import {NotificationManager} from "react-notifications";
import {ERROR_TEXT} from "../../global/constants";

export const FETCH_GET_ADVERTISEMENT_TYPE = 'FETCH_GET_ADVERTISEMENT_TYPE';
const getAdvertisementType = (data) => ({type: FETCH_GET_ADVERTISEMENT_TYPE, data});

export const fetchAdvertisements = () => {
    return dispatch => {
        return axios.get(ROUTE_ADVERTISEMENT_TYPES).then(
            response => {
                dispatch(getAdvertisementType(response.data))
            },
            err => {
                NotificationManager.error(ERROR_TEXT);
            }
        ).catch(err => console.log(err))
    }
}

export const fetchCreateAdvertType = data => {
    return dispatch => {
        return axios.post(ROUTE_ADVERTISEMENT_TYPES, data).then(
            response => {
                NotificationManager.success("Тип рекламы успешно создан");
                dispatch(fetchAdvertisements())
            },
            err => {
                NotificationManager.error(ERROR_TEXT);
            }
        ).catch(err => console.log(err))
    }
}

export const fetchEditAdvertType = (data, id) => {
    return dispatch => {
        return axios.put(ROUTE_ADVERTISEMENT_TYPES + id + "/", data).then(
            response => {
                NotificationManager.success("Тип рекламы успешно изменен");
                dispatch(fetchAdvertisements())
            },
            err => {
                NotificationManager.error(ERROR_TEXT);
            }
        ).catch(err => console.log(err))
    }
}

export const fetchRemoveAdvertType = id => {
    return dispatch => {
        return axios.delete(ROUTE_ADVERTISEMENT_TYPES + id + "/").then(
            () => {
                NotificationManager.success("Тип рекламы успешно удален");
                dispatch(fetchAdvertisements())
            },
            err => {
                NotificationManager.error(ERROR_TEXT);
            }
        ).catch(err => console.log(err))
    }
}

export const FETCH_SOCIAL_NETWORKS = 'FETCH_SOCIAL_NETWORKS'
const fetchSocialNetworksSuccess = (socials) => ({type: FETCH_SOCIAL_NETWORKS, socials});
export const fetchSocialsGet = () => {
    return dispatch => {
        return axios.get(ROUTE_SOCIAL_NETWORKS).then(
            response => {
                dispatch(fetchSocialNetworksSuccess(response.data))
            },
            err => {
                NotificationManager.error(ERROR_TEXT);
            }
        ).catch(err => console.log(err))
    }
}

export const fetchCreateSocialNetwork = data => {
    return dispatch => {
        return axios.post(ROUTE_SOCIAL_NETWORKS, data).then(
            response => {
                NotificationManager.success("Соц. сеть успешно создана");
                dispatch(fetchSocialsGet())
            },
            err => {
                NotificationManager.error(ERROR_TEXT);
            }
        ).catch(err => console.log(err))
    }
}

export const fetchEditSocialNetwork = (data, id) => {
    return dispatch => {
        return axios.put(ROUTE_SOCIAL_NETWORKS + id + "/", data).then(
            response => {
                NotificationManager.success("Соц. сеть успешно изменена");
                dispatch(fetchSocialsGet())
            },
            err => {
                NotificationManager.error(ERROR_TEXT);
            }
        ).catch(err => console.log(err))
    }
}

export const fetchRemoveSocial = id => {
    return dispatch => {
        return axios.delete(ROUTE_SOCIAL_NETWORKS + id + "/").then(
            () => {
                NotificationManager.success("Соц. сеть успешно удалена");
                dispatch(fetchSocialsGet())
            },
            err => {
                NotificationManager.error(ERROR_TEXT);
            }
        ).catch(err => console.log(err))
    }
}

export const FETCH_CONTENT_TYPES_SETTINGS = 'FETCH_CONTENT_TYPES_SETTINGS'
const fetchContentTypesSuccess = (content) => ({type: FETCH_CONTENT_TYPES_SETTINGS, content});
export const fetchContentTypesGet = () => {
    return dispatch => {
        return axios.get(ROUTE_CONTENT_TYPES).then(
            response => {
                dispatch(fetchContentTypesSuccess(response.data))
            },
            err => {
                NotificationManager.error(ERROR_TEXT);
            }
        ).catch(err => console.log(err))
    }
}

export const fetchCreateContentType = (data) => {
    return dispatch => {
        return axios.post(ROUTE_CONTENT_TYPES, data).then(
            () => {
                dispatch(fetchContentTypesGet())
                NotificationManager.success("Тег успешно создан");
            },
            err => {
                NotificationManager.error(ERROR_TEXT);
            }
        ).catch(err => console.log(err))
    }
}

export const fetchContentType = (data, id) => {
    return dispatch => {
        return axios.put(ROUTE_CONTENT_TYPES + id + "/", data).then(
            response => {
                NotificationManager.success("Тег успешно изменен");
                dispatch(fetchContentTypesGet())
            },
            err => {
                NotificationManager.error(ERROR_TEXT);
            }
        ).catch(err => console.log(err))
    }
}

export const fetchRemoveContentType = id => {
    return dispatch => {
        return axios.delete(ROUTE_CONTENT_TYPES + id + "/").then(
            () => {
                NotificationManager.success("Тег успешно удален");
                dispatch(fetchContentTypesGet())
            },
            err => {
                NotificationManager.error(ERROR_TEXT);
            }
        ).catch(err => console.log(err))
    }
}

export const FETCH_SETTINGS_AUDIENCE = 'FETCH_SETTINGS_AUDIENCE';
const fetchSettingsAudience = ages => ({type: FETCH_SETTINGS_AUDIENCE, ages});

export const fetchAudienceGet = () => {
    return dispatch => {
        return axios.get(ROUTE_AUDIENCE_AGE).then(
            response => {
                dispatch(fetchSettingsAudience(response.data))
            },
            err => {
                NotificationManager.error(ERROR_TEXT);
            }
        ).catch(err => console.log(err))
    }
}

export const fetchCreateAudienceAge = (data) => {
    return dispatch => {
        return axios.post(ROUTE_AUDIENCE_AGE, data).then(
            () => {
                dispatch(fetchAudienceGet())
                NotificationManager.success("Возрастная аудитория успешно создана");
            },
            err => {
                NotificationManager.error(ERROR_TEXT);
            }
        ).catch(err => console.log(err))
    }
}

export const fetchEditAudienceAge = (data, id) => {
    return dispatch => {
        return axios.put(ROUTE_AUDIENCE_AGE + id + "/", data).then(
            () => {
                dispatch(fetchAudienceGet())
                NotificationManager.success("Возрастная аудитория успешно изменена");
            },
            err => {
                NotificationManager.error(ERROR_TEXT);
            }
        ).catch(err => console.log(err))
    }
}

export const fetchRemoveAudienceAge = id => {
    return dispatch => {
        return axios.delete(ROUTE_AUDIENCE_AGE + id + "/").then(
            () => {
                NotificationManager.success("Возрастная аудитория успешно удалена");
                dispatch(fetchAudienceGet())
            },
            err => {
                NotificationManager.error(ERROR_TEXT);
            }
        ).catch(err => console.log(err))
    }
}

export const FETCH_SETTINGS_STATUSES = 'FETCH_SETTINGS_STATUSES';
const fetchSettingsStatuses = statuses => ({type: FETCH_SETTINGS_STATUSES, statuses});

export const fetchStatusesGet = () => {
    return dispatch => {
        return axios.get(ROUTE_BLOGGER_STATUS).then(
            response => {
                dispatch(fetchSettingsStatuses(response.data))
            },
            err => {
                NotificationManager.error(ERROR_TEXT);
            }
        ).catch(err => console.log(err))
    }
}

export const fetchCreateStatus = (data) => {
    return dispatch => {
        return axios.post(ROUTE_BLOGGER_STATUS, data).then(
            () => {
                dispatch(fetchStatusesGet())
                NotificationManager.success("Статус успешно создан");
            },
            err => {
                NotificationManager.error(ERROR_TEXT);
            }
        ).catch(err => console.log(err))
    }
}

export const fetchEditStatus = (data, id) => {
    return dispatch => {
        return axios.put(ROUTE_BLOGGER_STATUS + id + "/", data).then(
            () => {
                dispatch(fetchStatusesGet())
                NotificationManager.success("Статус успешно изменен");
            },
            err => {
                NotificationManager.error(ERROR_TEXT);
            }
        ).catch(err => console.log(err))
    }
}

export const fetchRemoveStatus = id => {
    return dispatch => {
        return axios.delete(ROUTE_BLOGGER_STATUS + id + "/").then(
            () => {
                NotificationManager.success("Статус успешно удален");
                dispatch(fetchStatusesGet())
            },
            err => {
                NotificationManager.error(ERROR_TEXT);
            }
        ).catch(err => console.log(err))
    }
}

export const FETCH_PROJECT_SOCIAL_NETWORKS = 'FETCH_PROJECT_SOCIAL_NETWORKS'
const fetchProjectSocialNetworksSuccess = data => ({type:FETCH_PROJECT_SOCIAL_NETWORKS, data})
export const fetchProjectSocialNetworks = () => {
    return dispatch => {
        return axios.get(ROUTE_PROJECT_SOCIALS_GET).then(
            response => {
                dispatch(fetchProjectSocialNetworksSuccess(response.data))
            }
        ).catch(err => console.log(err))
    }
}



export const CHANGE_SETTING_TYPE = "CHANGE_SETTING_TYPE";
export const changeSettingType = typeId => ({type: CHANGE_SETTING_TYPE, typeId})
