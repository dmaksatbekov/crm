import {
    CHANGE_SETTING_TYPE,
    FETCH_CONTENT_TYPES_SETTINGS,
    FETCH_GET_ADVERTISEMENT_TYPE, FETCH_SETTINGS_AUDIENCE, FETCH_SETTINGS_STATUSES,
    FETCH_SOCIAL_NETWORKS
} from "./settingsActions";

const initialState = {
    advertisement_types: null,
    socials: null,
    contentTypes: null,
    statuses: null,

    audienceAges: null,

    settingTypeId: 1
}

export default function settingsReducer(state = initialState, action) {
    switch (action.type) {
        case FETCH_GET_ADVERTISEMENT_TYPE:
            return {...state, advertisement_types: action.data}
        case FETCH_SOCIAL_NETWORKS:
            return {...state, socials: action.socials}
        case FETCH_CONTENT_TYPES_SETTINGS:
            return {...state, contentTypes: action.content}
        case FETCH_SETTINGS_STATUSES:
            return {...state, statuses: action.statuses}

        case CHANGE_SETTING_TYPE:
            return {...state, settingTypeId: action.typeId}

        case FETCH_SETTINGS_AUDIENCE:
            return {...state, audienceAges: action.ages}
        default:
            return state;
    }
}