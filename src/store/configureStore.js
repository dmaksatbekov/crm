import {createBrowserHistory} from "history";
import {connectRouter, routerMiddleware} from "connected-react-router";
import thunkMiddleware from "redux-thunk";
import {applyMiddleware, combineReducers, compose, createStore} from "redux";
import {loadState, saveState} from "./localStorage";
import axios from '../Api/axios_api';
import landingReducer from "./landing/landingStore";
import applicationsReducer from "./applications/applicationsStore";
import bloggersReducer from "./bloggers/bloggersStore";
import advertisementReducer from "./advertisement/advertisementStore";
import settingsReducer from "./settings/settingsStore";
import reportReducer from "./report/reportStore";
import userReducer from "./user/userStore";

export const history = createBrowserHistory();

history.listen((location, action) => {
    window.scrollTo(0, 0);
});

const rootReducer = combineReducers({
    router: connectRouter(history),
    landingInfo: landingReducer,
    applicationsInfo: applicationsReducer,
    bloggers: bloggersReducer,
    advertisement: advertisementReducer,
    settings: settingsReducer,
    reports: reportReducer,
    user:userReducer
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const middleware = [thunkMiddleware, routerMiddleware(history)];


const enhancers = composeEnhancers(applyMiddleware(...middleware));

const persistedState = loadState();

const store = createStore(rootReducer, persistedState, enhancers);

store.subscribe(() => {
    saveState({
        user: {
            user: store.getState().user.user
        }
    })
});

axios.interceptors.request.use(config => {
    try {
        if (store.getState().user && store.getState().user.user && store.getState().user.user.key) {
            if (window.location.pathname !== "/") {
                config.headers["Authorization"] = 'Token ' + store.getState().user.user.key;
            }
        }
    } catch (e) {

    }

    return config;
});



export default store;