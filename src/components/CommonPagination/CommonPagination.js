import React from 'react';
import Pagination from '@material-ui/lab/Pagination';

import './index.scss';

const CommonPagination = ({count, currentPage, onPageChange}) => {
    return (
        <Pagination
            className="common-pagination-root"
            count={count}
            page={currentPage}
            onChange={onPageChange}
            variant="outlined"
            shape="rounded"/>
    );
};

export default CommonPagination;