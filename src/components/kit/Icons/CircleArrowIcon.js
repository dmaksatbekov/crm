import React from 'react';

const CircleArrowIcon = ({width = 90, height = 90, className}) => {
    return (
        <svg xmlns="http://www.w3.org/2000/svg"
             width={width}
             height={height}
             className={className}
             viewBox="0 0 90 90" fill="none">
            <g filter="url(#filter0_d)">
                <circle cx="45" cy="35" r="25" fill="#110421"/>
            </g>
            <path fillRule="evenodd" clipRule="evenodd"
                  d="M45.7071 29.7071C46.0976 29.3166 46.0976 28.6834 45.7071 28.2929C45.3166 27.9024 44.6834 27.9024 44.2929 28.2929L37.2929 35.2929C36.9024 35.6834 36.9024 36.3166 37.2929 36.7071L44.2929 43.7071C44.6834 44.0976 45.3166 44.0976 45.7071 43.7071C46.0976 43.3166 46.0976 42.6834 45.7071 42.2929L40.4142 37H52C52.5523 37 53 36.5523 53 36C53 35.4477 52.5523 35 52 35H40.4142L45.7071 29.7071Z"
                  fill="white"/>
            <defs>
                <filter id="filter0_d" x="0" y="0" width={width} height={height} filterUnits="userSpaceOnUse"
                        colorInterpolation="sRGB">
                    <feFlood floodOpacity="0" result="BackgroundImageFix"/>
                    <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"/>
                    <feOffset dy="10"/>
                    <feGaussianBlur stdDeviation="10"/>
                    <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.2 0"/>
                    <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow"/>
                    <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow" result="shape"/>
                </filter>
            </defs>
        </svg>
    );
};

export default CircleArrowIcon;