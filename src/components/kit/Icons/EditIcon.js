import React from 'react';

const EditIcon = (
    {
        width = 16,
        height = 16,
        color = '#0099FF',
        className,
        onClick
    }) => {
    return (
        <span style={{width: width, height: height}} onClick={onClick} className={className}>
            <svg xmlns="http://www.w3.org/2000/svg" width={width} height={height} viewBox="0 0 16 16" fill="none">
<path fillRule="evenodd" clipRule="evenodd"
      d="M10.1952 1.52864C10.4555 1.26829 10.8776 1.26829 11.138 1.52864L14.4713 4.86197C14.7317 5.12232 14.7317 5.54443 14.4713 5.80478L5.80466 14.4714C5.67963 14.5965 5.51006 14.6667 5.33325 14.6667H1.99992C1.63173 14.6667 1.33325 14.3682 1.33325 14V10.6667C1.33325 10.4899 1.40349 10.3203 1.52851 10.1953L10.1952 1.52864ZM2.66659 10.9429V13.3334H5.05711L13.0571 5.33337L10.6666 2.94285L2.66659 10.9429Z"
      fill={color}/>
</svg>
        </span>
    );
};

export default EditIcon;