import React from 'react';

const ArrowSimpleIcon = (
    {
        width = 13,
        height = 8,
        color = '#C9CED6',
        className,
        onClick
    }) => {
    return (
        <span style={{width: width, height: height}} onClick={onClick} className={className}>
            <svg xmlns="http://www.w3.org/2000/svg" width={width} height={height} viewBox="0 0 14 8"
                 fill="none">
                <path d="M1 1L7 7L13 1" stroke={color} strokeWidth="2" strokeLinecap="round"
                      strokeLinejoin="round"/>
            </svg>
        </span>
    );
};

export default ArrowSimpleIcon;