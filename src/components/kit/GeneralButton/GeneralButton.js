import React from 'react';

import './index.scss';

const GeneralButton = ({children, disabled, onSubmit, type = "submit"}) => {
    return (
        <button
            className={`general-btn 
            ${disabled ? "general-btn--dsiabled" : "general-btn--active"}`}
            disabled={disabled}
            type={type}
            onClick={onSubmit}>
            {children}
        </button>
    );
};

export default GeneralButton;