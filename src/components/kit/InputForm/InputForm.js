import React, {useState} from 'react';
import TextField from "@material-ui/core/TextField";
import {makeStyles} from "@material-ui/core/styles";

import './index.scss';
import FormControl from "@material-ui/core/FormControl";
import FormLabel from "@material-ui/core/FormLabel";
import withStyles from "@material-ui/core/styles/withStyles";

const CssTextField = withStyles({
    root: {
        '& label.Mui-focused': {
            color: '#24AF47',
        },
        '& .MuiInput-underline:after': {
            borderBottomColor: '#24AF47',
        },
        '& .MuiOutlinedInput-root': {
            '& fieldset': {
                borderColor: '#DADCE5',
            },
            '&:hover fieldset': {
                borderColor: '#7400FF',
            },
            '&.Mui-focused fieldset': {
                borderColor: '#24AF47',
                borderWidth: "1px"
            },
        },
    },
})(TextField);

const useStyles = makeStyles(theme => ({
    root: {
        borderRadius: '6px',
        margin: "10px 0"
    }
}));


const InputForm = ({propertyName, onChange, className, label, value, isError, ...props}) => {
    const classes = useStyles();
    const [isActive, setActive] = useState(false);
    return (
        <FormControl className={[classes.root, "input-form", className].join(' ')}>
            <FormLabel component="legend">
                <span style={{color: isActive ? "#24AF47" : isError ? "#FA445C" : "#110421"}}>
                {label}
            </span>
            </FormLabel>
            <CssTextField
                variant="outlined"
                name={propertyName}
                onChange={onChange}
                value={value}
                onFocus={() => setActive(prevValue => !prevValue)}
                onBlur={() => setActive(prevValue => !prevValue)}
                error={!!isError}
                {...props}
            />
        </FormControl>
    );
};

export default InputForm;