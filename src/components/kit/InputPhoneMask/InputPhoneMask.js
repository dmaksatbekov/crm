import React from 'react';
import Input from "@material-ui/core/Input";
import {makeStyles} from "@material-ui/core/styles";
import InputMask from 'react-input-mask';
import FormControl from "@material-ui/core/FormControl";
import FormLabel from "@material-ui/core/FormLabel";
import './index.scss';

const useStyles = makeStyles(theme => ({
    root: {
        position: 'relative'
    },
    placeHolder: {
        position: 'absolute',
        top: '43px',
        left: '17px',
        color: "black"
    }
}));

function TextMaskCustom(props) {
    const {inputRef, ...other} = props;
    return (
        <InputMask
            // mask="+\9\96 999-99-99-99"
            maskChar=" "
            {...other}
            placeholder=" +996 000-000-000"
            ref={ref => {
                inputRef(ref ? ref.inputElement : null);
            }}
        />
    );
}

const InputPhoneMask = (
    {
        value,
        propertyName,
        onChange,
        className,
        label,
        isError,
        helperText,
        ...props
    }) => {
    const classes = useStyles();
    return (
        <FormControl className={[classes.root, "input-form-mask", className].join(' ')}>
            <FormLabel component="legend">
                <span className={`${isError ? "label-error" : ""}`}>{label}</span>
            </FormLabel>
            <Input
                value={value}
                name={propertyName}
                onChange={onChange}
                inputComponent={TextMaskCustom}
                disableUnderline
                type="number"
                {...props}
                error={!!isError}
            />
            {isError &&
            <span className="error-text">{helperText}</span>}
        </FormControl>
    );
};

export default InputPhoneMask;