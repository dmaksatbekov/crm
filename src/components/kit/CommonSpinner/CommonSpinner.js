import React from 'react';
import {ClipLoader} from "react-spinners";
import './index.scss';

const CommonSpinner = ({isLoading, size = 20, color = "#3b3ea1", centered, className}) => {
    return (
        <div className={["common-spinner",
            `${centered ? "common-spinner--centered" : ""}`,
            className].join(" ")}>
            <ClipLoader
                size={size}
                color={color}
                loading={isLoading}
            />
        </div>
    );
};

export default CommonSpinner;