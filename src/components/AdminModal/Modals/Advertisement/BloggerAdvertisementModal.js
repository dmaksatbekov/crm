import React, {useEffect, useState} from 'react';
import AdminModal from "../../AdminModal";
import {Formik} from "formik";
import {isEmptyArray} from "../../../../global/helpers";
import CustomSelect from "../../../kit/SocialSelect/CustomSelect";
import InputForm from "../../../kit/InputForm/InputForm";
import TrashIcon from "../../../kit/Icons/TrashIcon";
import CommonSpinner from "../../../kit/CommonSpinner/CommonSpinner";
import GeneralButton from "../../../kit/GeneralButton/GeneralButton";
import {fetchBloggerById, fetchClearBloggerStore} from "../../../../store/bloggers/bloggersActions";
import {connect} from "react-redux";
import Input from "@material-ui/core/Input";
import InputAdornment from "@material-ui/core/InputAdornment";
import {fetchEditBloggerBill} from "../../../../store/advertisement/advertisementActions";

const BloggerAdvertisementModal = (
    {
        onClose,
        type,
        onSubmit,
        initData,
        blogger,
        fetchBloggerById,
        fetchClearBloggerStore,
        fetchEditBloggerBill,
        disabled
    }
) => {


    const [text_tz, setTz] = useState("");

    const [inputFields, setInputFields] = useState([
        {advertisement_type: '', price: '', commission: "", total: ""}
    ]);

    useEffect(function () {
        fetchBloggerById(initData.blogger);

        if (type !== "rejected" && initData) {
            const initFields = [];
            initData.bill && initData.bill.forEach(item => {
                initFields.push({
                    advertisement_type: item.advertisement_type,
                    price: item.price,
                    commission: item.commission,
                    total: item.total_sum
                })
            })

            if (initData.text_tz) {
                setTz(initData.text_tz)
            }

            if (!isEmptyArray(initFields)) {
                setInputFields(initFields)
            }
        }

        return function () {
            fetchClearBloggerStore()
        }
    }, [])

    const handleInputChange = (index, event, item) => {
        const values = [...inputFields];
        if (event.target.name === "commission") {
            values[index].commission = event.target.value > 100 ? 100 : event.target.value < 0 ? 0 : event.target.value;
            const total = Number(values[index].price) + (values[index].price * event.target.value / 100)
            values[index].total = !isNaN(Number(total)) ? Math.floor(Number(total)) : 0;
        } else if (event.target.name === "price") {
            values[index].price = !isNaN(Number(event.target.value)) ? Math.abs(Number(event.target.value)) : 0;
            const total = Number(values[index].price) + (values[index].price * Number(values[index].commission) / 100)
            values[index].total = !isNaN(Number(total)) ? Math.floor(Number(total)) : 0;
        } else {
            values[index].advertisement_type = item;
            const type = blogger.service.find(it => it.advertisement_type === item);
            values[index].price = type.price;
            const total = Number(type.price) + (type.price * values[index].commission / 100)
            values[index].total = !isNaN(Number(total)) ? Math.floor(Number(total)) : 0;

        }

        setInputFields(values);
    };

    function handleAddFields() {
        const values = [...inputFields];
        values.push({advertisement_type: '', price: '', commission: "", total: 0});
        setInputFields(values);
    }

    function handleRemoveField(ndx) {
        const values = [...inputFields];
        values.splice(ndx, 1)
        setInputFields(values);
    }


    async function onSubmitting() {
        if (type === "tz") {
            const editData = {
                text_tz: text_tz,
                campaign: initData.campaign
            }
            await fetchEditBloggerBill(editData, initData.id, initData.campaign);
        } else {
            const filtered = inputFields.map(it => {
                const copy = {...it};
                delete copy.total
                copy.commission = Number(copy.commission)
                return copy;
            })
            const editData = {
                bill: filtered,
                campaign: initData.campaign
            }
            await fetchEditBloggerBill(editData, initData.id, initData.campaign);
        }

        onClose();
    }

    function validateForm(values) {
        const errors = {};

        if (type !== "tz") {
            inputFields.forEach((field, ndx) => {
                if (!inputFields[ndx].advertisement_type || !inputFields[ndx].price) {
                    errors.inputFields = 'Обязательное поле';
                }
            })
        } else {
            if (!text_tz) {
                errors.text_tz = "Обязательно поле"
            }
        }

        return errors;
    }

    const changeData = [];
    blogger && blogger.service &&
    blogger.service.forEach(item => changeData.push({name: item.advertisement_type_name, id: item.advertisement_type}));

    return (
        <AdminModal
            onClose={onClose}
            title={type === "rejected" ? "Описание причины" : type === "tz" ? "Написать ТЗ" : "Выбрать рекламу"}
            backdrop={true}>
            <Formik
                initialValues={{}}
                validate={values => validateForm(values)}
                onSubmit={onSubmitting}
            >
                {
                    ({
                         values,
                         errors,
                         touched,
                         handleChange,
                         dirty,
                         handleSubmit,
                     }) => (
                        <>
                            {type !== "rejected" ?
                            <form className="modal-form__advertise-types" onSubmit={handleSubmit}>
                                {type !== "tz" ?
                                    <>
                                        <div className="modal-form__socials">
                                            <div className="modal-form__socials-texts">
                                                <p style={{color: errors.inputFields ? "#FA445C" : "#1A051D"}}>
                                                    вид рекламы *
                                                </p>
                                                <p style={{color: errors.inputFields ? "#FA445C" : "#1A051D"}}>
                                                    цена
                                                </p>
                                                <p style={{color: errors.inputFields ? "#FA445C" : "#1A051D"}}>
                                                    комиссия
                                                </p>
                                                <p style={{color: errors.inputFields ? "#FA445C" : "#1A051D"}}>
                                                    Итого
                                                </p>
                                            </div>
                                            {blogger &&
                                            !isEmptyArray(blogger.service) &&
                                            inputFields.map((field, ndx) => (
                                                <div className="modal-form__socials-fields" key={ndx}>
                                                    <div className="block">
                                                        <CustomSelect
                                                            data={changeData}
                                                            index={ndx}
                                                            value={field.advertisement_type}
                                                            onChange={handleInputChange}
                                                        />
                                                    </div>
                                                    <div className="block">
                                                        <InputForm
                                                            propertyName="price"
                                                            value={field.price}
                                                            onChange={event => handleInputChange(ndx, event)}
                                                            placeholder="00.00"
                                                        />
                                                    </div>
                                                    <div className="block">
                                                        <Input
                                                            name="commission"
                                                            value={field.commission}
                                                            className="total-sum"
                                                            onChange={event => handleInputChange(ndx, event)}
                                                            endAdornment={<InputAdornment
                                                                position="end">%</InputAdornment>}
                                                        />
                                                    </div>
                                                    <div className="block">
                                                        <InputForm
                                                            propertyName="total"
                                                            value={field.total}
                                                            onChange={event => handleInputChange(ndx, event)}
                                                            placeholder="00.00"
                                                            disabled
                                                        />
                                                    </div>

                                                    {ndx !== 0 && <TrashIcon
                                                        className="field_remove"
                                                        onClick={() => handleRemoveField(ndx)}/>}
                                                </div>
                                            ))}
                                            {blogger && !isEmptyArray(blogger.service) &&
                                            <span onClick={handleAddFields} className="modal-form__socials-add">
                                    Добавить еще
                            </span>}

                                            {errors.inputFields &&
                                            <span className="error-text">{errors.inputFields}</span>
                                            }
                                        </div>

                                        {!blogger && <CommonSpinner isLoading={!blogger} centered/>}

                                        {blogger && isEmptyArray(blogger.service) &&
                                        <h3 className="not-found-variants">
                                            У выбранного блогера нет доступных вариантов
                                        </h3>
                                        }
                                    </>
                                    :
                                    <div className="description-tz">
                                        <InputForm
                                            label="описание ТЗ *"
                                            placeholder="Опишите ТЗ"
                                            value={text_tz}
                                            propertyName="text_tz"
                                            onChange={e => setTz(e.target.value)}
                                            multiline
                                            rows={4}
                                            isError={!!errors.text_tz}
                                            helperText={errors.text_tz}
                                        />
                                    </div>
                                }
                                <GeneralButton
                                    type="submit"
                                    className="modal-form__btn"
                                    variant="outlined"
                                    disabled={blogger && isEmptyArray(blogger.service)}
                                >
                                    Подтвердить
                                </GeneralButton>
                            </form>
                                :
                                <p className="rejection_cause_text">
                                    {initData.rejection_cause || ""}
                                </p>
                            }
                        </>
                    )
                }
            </Formik>
        </AdminModal>
    );
};

const mapStateToProps = state => ({
    blogger: state.bloggers.blogger
})

const mapDispatchToProps = dispatch => ({
    fetchBloggerById: id => dispatch(fetchBloggerById(id)),
    fetchClearBloggerStore: () => dispatch(fetchClearBloggerStore()),
    fetchEditBloggerBill: (data, bloggerId, advertiseId) =>
        dispatch(fetchEditBloggerBill(data, bloggerId, advertiseId))
})

export default connect(mapStateToProps, mapDispatchToProps)(BloggerAdvertisementModal);