import React, {useEffect, useState} from 'react';
import AdminModal from "../../AdminModal";

import '../index.scss';
import {connect} from "react-redux";
import PersonalForm from "./PersonalForm";
import CheckIcon from "../../../kit/Icons/CheckIcon";
import SocialsForm from "./SocialsForm";
import AdvertisementForm from "./AdvertisementForm";
import {
    clearBloggerInfo,
    fetchBloggerById,
    fetchCreateBlogger,
    fetchEditBlogger
} from "../../../../store/bloggers/bloggersActions";
import CommonSpinner from "../../../kit/CommonSpinner/CommonSpinner";
import {CURRENT_DATE} from "../../../../global/constants";

const STEP_1 = 1;
const STEP_2 = 2;
const STEP_3 = 3;

const STEPS = [{numb: 1, name: "Личные данные"}, {numb: 2, name: "Соц.сети"}, {numb: 3, name: "Цена"}];


const MODAL_TYPES = {
    create: "Добавить блогера",
    edit: "Редактирование"
}
let formData = new FormData();

let PERSONAL = {
    image: "",
    surname: "",
    name: "",
    middle_name: "",
    status: "",
    gender: "M",
    birthday_date: CURRENT_DATE,
    country: "",
    phone: "",
    email: "",
    telegram_login: "",
}

let SOCIALS = {
    audience_age: "",
    social_network: [
        {social_network: '', link: ''}
    ],
    subject: []
}

const BloggerModal = (
    {
        onClose,
        type = "create",
        initValues,
        fetchCreateBlogger,
        fetchBloggerById,
        bloggerId,
        blogger,
        bloggerRequest,
        fetchEditBlogger,
        item,
        clearBloggerInfo
    }) => {

    const [step, setStep] = useState(STEP_1);
    const [isLoading, setLoading] = useState(true)

    useEffect(function () {
        if (bloggerId) {
            fetchBloggerById(bloggerId)
        }
        setLoading(false)
        return function () {
            clearBloggerInfo()
        }
    }, [step])

    useEffect(function () {
        if (item) {
            PERSONAL["surname"] = item.surname
            PERSONAL["name"] = item.name
            PERSONAL["middle_name"] = item.middle_name
            PERSONAL["phone"] = item.phone_number
            SOCIALS["social_network"] = [...item.social_network]
        }
        setLoading(false)
        return function () {
            PERSONAL = {
                image: "",
                surname: "",
                name: "",
                middle_name: "",
                status: "",
                gender: "M",
                birthday_date: CURRENT_DATE,
                country: "",
                phone: "",
                email: "",
                telegram_login: "",
            }

            SOCIALS = {
                audience_age: "",
                social_network: [
                    {social_network: '', link: ''}
                ],
                subject: []
            }
            formData = new FormData();
        }
    }, [])


    function createPersonalInfo(values) {
        if (type === "edit" && blogger) {
            const data = new FormData();
            Object.keys(values).forEach(key => {
                data.append(key, values[key])
            })
            fetchEditBlogger(blogger.id, data)
        } else {
            Object.keys(values).forEach(key => {
                PERSONAL[key] = values[key]
            })
        }
        setStep(STEP_2)

    }

    async function createSocials(values) {
        if (type === "edit" && blogger) {
            const data = new FormData();
            Object.keys(values).forEach(key => {
                if (key === "subject") {
                    const subjects = []
                    values[key].forEach(subject => {
                        subjects.push(subject)
                    })
                    data.append(key, JSON.stringify(subjects))

                } else if (key === "social_network") {
                    const networks = []
                    values[key].forEach(network => {
                        networks.push(network)
                    })
                    data.append(key, JSON.stringify(networks))

                } else {
                    data.append(key, values[key])
                }
            })
            await fetchEditBlogger(blogger.id, data)
        } else {
            SOCIALS = {...values};
        }
        setStep(STEP_3)

    }

    async function onSubmit(values) {

        if (type === "edit" && blogger) {
            const data = new FormData();
            Object.keys(values).forEach(key => {
                data.append(key, JSON.stringify(values[key]))
            })
            await fetchEditBlogger(blogger.id, data)
        } else {
            Object.keys(PERSONAL).forEach(key => {
                formData.append(key, PERSONAL[key])
            })

            Object.keys(SOCIALS).forEach(key => {
                if (key === "subject") {
                    const subjects = []
                    SOCIALS[key].forEach(subject => {
                        subjects.push(subject)
                    })
                    formData.append(key, JSON.stringify(subjects))

                } else if (key === "social_network") {
                    const networks = []
                    SOCIALS[key].forEach(network => {
                        networks.push(network)
                    })
                    formData.append(key, JSON.stringify(networks))

                } else {
                    formData.append(key, SOCIALS[key])
                }
            })
            Object.keys(values).forEach(key => {
                formData.append(key, JSON.stringify(values[key]))
            })
            await fetchCreateBlogger(formData)
        }

        onClose()

    }

    return (
        <AdminModal onClose={onClose} title={MODAL_TYPES[type]} application={initValues}>
            <div className="blogger-modal">
                <div className="steps">
                    {STEPS.map(item => (
                        <div className={`step 
                        ${(step >= item.numb) && "step--active"}`}
                             onClick={() => (step > item.numb || bloggerId) ? setStep(item.numb) : null}
                             key={item.numb}>
                        <span className={`step-numb 
                        ${(step >= item.numb || bloggerId) && "step-numb--active"}`}>
                            {(step > item.numb || bloggerId) ? <CheckIcon/> : item.numb}
                        </span>
                            <p className={`step-title 
                            ${(step >= item.numb || bloggerId) && "step-title--active"}`}>
                                {item.name}
                            </p>
                        </div>
                    ))}
                </div>
            </div>
            {(bloggerRequest || isLoading) ? <CommonSpinner isLoading={!bloggerRequest} centered/>
                :
                <>
                    {step === STEP_1 &&
                    <PersonalForm initValues={PERSONAL} onSubmit={createPersonalInfo}/>}
                    {step === STEP_2 &&
                    <SocialsForm initValues={SOCIALS} onSubmit={createSocials}/>}
                    {step === STEP_3 &&
                    <AdvertisementForm onSubmit={onSubmit}/>}
                </>
            }

        </AdminModal>
    );
};

const mapStateToProps = state => ({
    blogger: state.bloggers.blogger,
    bloggerRequest: state.bloggers.bloggerRequest,
    bloggerFailure: state.bloggers.bloggerFailure
})

const mapDispatchToProps = dispatch => ({
    fetchCreateBlogger: data => dispatch(fetchCreateBlogger(data)),
    fetchBloggerById: id => dispatch(fetchBloggerById(id)),
    fetchEditBlogger: (id, data) => dispatch(fetchEditBlogger(id, data)),
    clearBloggerInfo: () => dispatch(clearBloggerInfo())
})

export default connect(mapStateToProps, mapDispatchToProps)(BloggerModal);