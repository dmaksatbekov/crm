import React from 'react';

const PriceInfo = ({data}) => {
    return (
        <div className="blogger-info__prices">
            <ul>
                {data.service && data.service.map((item, ndx) => (
                    <li>
                        <span>{ndx + 1}) {item.advertisement_type_name}</span>
                        <span>{item.price && item.price.toLocaleString()} c</span>
                    </li>
                ))}
            </ul>
        </div>
    );
};

export default PriceInfo;