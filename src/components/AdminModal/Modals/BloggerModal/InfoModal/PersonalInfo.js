import React from 'react';

const PersonalInfo = ({data}) => {
    return (
        <div className="blogger-info__personal">
            <div className="blogger-info__personal-side">
                <p className="info-label">ФИО</p>
                <p className="info-value">
                    <span>{data.surname}</span>
                    <span>{data.name}</span>
                    <span>{data.middle_name}</span>
                </p>

                <p className="info-label">Пол</p>
                <p className="info-value">
                    {data.gender === "M" ? "Мужской" : "Женский"}
                </p>
                <p className="info-label">Телефон</p>
                <p className="info-value">
                    {data.phone}
                </p>
            </div>
            <div className="blogger-info__personal-side">
                <p className="info-label">Страна</p>
                <p className="info-value">
                    {data.country_name}
                </p>
                <p className="info-label">Дата рождения</p>
                <p className="info-value">
                    {data.birthday_date}
                </p>
                <p className="info-label">Электронная почта</p>
                <p className="info-value">
                    <span className="info-value__email">{data.email}</span>
                </p>
            </div>
        </div>
    );
};

export default PersonalInfo;