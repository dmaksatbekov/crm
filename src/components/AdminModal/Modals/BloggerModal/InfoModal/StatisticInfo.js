import React from 'react';
import LikeIcon from "../../../../kit/Icons/LikeIcon";

const StatisticInfo = ({data}) => {
    return (
        <div className="blogger-info__statistics">
            <p className="blogger-info__statistics-title">Статистика заказов</p>
            <div className="blogger-info__statistics-head">
                <p className="accepted-root">
                    <LikeIcon className="accepted-root__icon"/>
                    <span className="accepted-root__text">Принял(а)</span>
                    <span className="accepted-root__count">{data.accepted}</span>
                </p>
                <p className="accepted-root">
                    <LikeIcon className="accepted-root__icon--rotate" color="#FA445C"/>
                    <span className="accepted-root__text">Отказал(а)</span>
                    <span className="accepted-root__count">{data.refused}</span>
                </p>
            </div>
            <ul className="blogger-info__statistics-ul">
                <li className="blogger-info__statistics-li">
                    <span>Заработал(а)</span>
                    <span>{data.earned} c</span>
                </li>
                <li className="blogger-info__statistics-li">
                    <span>Заработал(а) для компании</span>
                    <span>{data.profit} c</span>
                </li>
                <li className="blogger-info__statistics-li">
                    <span>Возраст аудитории</span>
                    <span>{data.audience_age_name}</span>
                </li>
            </ul>
            <ul className="blogger-info__statistics-geo-ul">
                {data.geodata && data.geodata.map(country => (
                    <li className="blogger-info__statistics-geo-li" key={country.id}>
                        <span>{country.country_name}</span>
                        <span className={`${country.percent <= 10 ? "grey-text" : ""}`}>
                            {country.percent}%
                        </span>
                    </li>
                ))}
            </ul>
        </div>
    );
};

export default StatisticInfo;