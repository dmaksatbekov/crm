import React, {useEffect, useState} from 'react';
import InputForm from "../../../kit/InputForm/InputForm";
import InputPhoneMask from "../../../kit/InputPhoneMask/InputPhoneMask";
import GeneralButton from "../../../kit/GeneralButton/GeneralButton";
import {Formik, isEmptyArray} from "formik";
import IconButton from "@material-ui/core/IconButton";
import CameraIcon from "../../../kit/Icons/CameraIcon";
import {
    fetchBloggerStatuses,
    fetchCheckTelegramLogin,
    fetchCountriesList
} from "../../../../store/bloggers/bloggersActions";
import {connect} from "react-redux";
import CustomSelect from "../../../kit/SocialSelect/CustomSelect";
import CommonSpinner from "../../../kit/CommonSpinner/CommonSpinner";
import DatePickerForm from "../../../kit/DatePickerForm/DatePickerForm";
import moment from "moment";
import {dateFormatter} from "../../../../global/helpers";
import {CURRENT_DATE} from "../../../../global/constants";

let PREV_NICKNAME = '';

const PersonalForm = (
    {
        onSubmit,
        fetchBloggerStatuses,
        bloggerStatuses,
        fetchCountriesList,
        countries,
        blogger,
        initValues,
        fetchCheckTelegramLogin,
        existsLogin
    }) => {
    const [state, setState] = useState({
        image: initValues["image"],
        surname: blogger ? blogger.surname : initValues["surname"],
        name: blogger ? blogger.name : initValues["name"],
        middle_name: blogger ? blogger.middle_name : initValues["middle_name"],
        status: blogger ? blogger.status : initValues["status"],
        gender: blogger ? blogger.gender : initValues["gender"],
        birthday_date: blogger ? moment(blogger.birthday_date, "DD-MM-YYYY").toDate() : moment(initValues["birthday_date"], "DD-MM-YYYY").toDate(),
        country: blogger ? blogger.country : initValues["country"],
        phone: blogger ? blogger.phone : initValues["phone"],
        email: blogger ? blogger.email : initValues["email"],
        telegram_login: blogger ? blogger.telegram_login : initValues["telegram_login"],
    })

    useEffect(function () {
        fetchBloggerStatuses()
        fetchCountriesList()
        if (initValues["telegram_login"]) {
            PREV_NICKNAME = initValues["telegram_login"]
        }
    }, [])

    const fileHandler = setFieldValue => (e) => {
        e.persist();
        if (e.target.files[0]) {
            setState(prevState => {
                return {...prevState, image: e.target.files[0]}
            })
        }
        setFieldValue('image', e.target.files[0])
    }

    function statusHandler(e, ndx, id) {
        setState(prevState => {
            return {...prevState, status: id}
        })
    }

    function countryHandler(e, ndx, id) {
        setState(prevState => {
            return {...prevState, country: id}
        })
    }

    function genderHandler(value) {
        setState(prevState => {
            return {...prevState, gender: value}
        })
    }

    const dateHandleChange = setFieldValue => date => {
        setFieldValue('birthday_date', date)
    }

    async function onSubmitting(values) {
        const data = {...values};
        data.status = state.status;
        data.country = state.country;
        data.gender = state.gender;
        data.birthday_date = dateFormatter(data.birthday_date)
        if (state.image !== "") {
            data.image = state.image;
        } else {
            delete data.image
        }
        onSubmit(data)
    }

    const telegramLoginHandler = setFieldValue => e => {
        e.persist()
        setFieldValue('telegram_login', e.target.value)
        if (!blogger || (blogger && blogger.telegram_login !== e.target.value)) {
            fetchCheckTelegramLogin({login: e.target.value})
        }
    }

    function validateForm(values) {
        const errors = {};
        // if (!values.surname) {
        //     errors.surname = 'Обязательное поле';
        // }
        if (!values.name) {
            errors.name = 'Обязательное поле';
        }
        if (!state.status) {
            errors.status = 'Обязательное поле';
        }
        if (!state.country) {
            errors.country = 'Обязательное поле';
        }
        if (!values.birthday_date) {
            errors.birthday_date = 'Обязательное поле';
        }
        // if (!values.phone) {
        //     errors.phone = 'Обязательное поле';
        // }
        if (!values.telegram_login) {
            errors.telegram_login = 'Обязательное поле';
        }
        if (values.email && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
            errors.email = 'Некорректный адрес';
        }
        return errors;
    }

    return (
        <Formik
            initialValues={state}
            validate={values => validateForm(values)}
            onSubmit={(values) => {
                onSubmitting(values)
            }}
        >
            {
                ({
                     values,
                     errors,
                     touched,
                     handleChange,
                     dirty,
                     setFieldValue,
                     handleSubmit,
                 }) => (
                    <form className="modal-form" onSubmit={handleSubmit}>
                        <div className="modal-form__upload-root">
                            <>
                                <input accept="image/*"
                                       className="modal-form__upload"
                                       id="icon-button-file"
                                       onChange={fileHandler(setFieldValue)}
                                       type="file"/>
                                {!state.image && !blogger &&
                                <div className="circle">
                                    <label htmlFor="icon-button-file">
                                        <IconButton color="primary" aria-label="upload picture" component="span">
                                            <CameraIcon/>
                                        </IconButton>
                                    </label>
                                </div>
                                }
                                {state.image &&
                                <img className="uploaded-image"
                                     src={URL.createObjectURL(state.image)}
                                     alt=""/>
                                }
                                {!state.image && blogger &&
                                <img className="uploaded-image"
                                     src={blogger.image}
                                     alt=""/>}
                            </>

                            <label htmlFor="icon-button-file">
                                <p>Загрузить фото</p>
                            </label>
                        </div>
                        <InputForm
                            label="Фамилия"
                            placeholder="Введите фамилию"
                            propertyName="surname"
                            value={values.surname}
                            onChange={handleChange}
                            isError={errors.surname && touched.surname && errors.surname}
                            helperText={errors.surname && touched.surname && errors.surname}
                        />
                        <InputForm
                            label="Имя"
                            placeholder="Введите имя"
                            propertyName="name"
                            value={values.name}
                            onChange={handleChange}
                            isError={errors.name && touched.name && errors.name}
                            helperText={errors.name && touched.name && errors.name}
                        />
                        <InputForm
                            label="Отчество"
                            placeholder="Введите отчество"
                            propertyName="middle_name"
                            value={values.middle_name}
                            onChange={handleChange}
                        />
                        <div className="modal-form__status-select">
                            <p style={
                                {
                                    color: errors.status &&
                                    touched.status &&
                                    errors.status ? "#FA445C" : "#1A051D"
                                }
                            }
                            >Выбрать статус</p>
                            {bloggerStatuses && !isEmptyArray(bloggerStatuses) &&
                            <CustomSelect
                                className="select"
                                data={bloggerStatuses}
                                value={state.status}
                                onChange={statusHandler}
                            />}
                            {!bloggerStatuses && <CommonSpinner centered isLoading={true}/>}
                            {errors.status && touched.status && errors.status &&
                            <span className="error-text">{errors.status}</span>
                            }
                        </div>
                        <div className="modal-form__gender-select">
                            <div className="gender" onClick={() => genderHandler("M")}>
                                <div className={`gender-circle 
                                ${state.gender === "M" && "gender-circle__active"}`}>
                                    {state.gender === "M" && <span className="gender-active"/>}
                                </div>
                                <span className={`gender-value
                                 ${state.gender === "M" && "gender-value--active"}`}>Мужской</span>
                            </div>
                            <div className="gender" onClick={() => genderHandler("F")}>
                                <div className={`gender-circle 
                                ${state.gender === "F" && "gender-circle__active"}`}>
                                    {state.gender === "F" && <span className="gender-active"/>}
                                </div>
                                <span className={`gender-value
                                 ${state.gender === "F" && "gender-value--active"}`}>Женский</span>
                            </div>
                        </div>
                        <div className="modal-form__date-mask">
                            <p style={
                                {
                                    color: errors.birthday_date &&
                                    touched.birthday_date &&
                                    errors.birthday_date ? "#FA445C" : "#1A051D"
                                }
                            }>Дата рождения</p>
                            <DatePickerForm
                                className="modal-form__form-datetime-calendar"
                                selected={values.birthday_date}
                                showMonthDropdown
                                showYearDropdown
                                yearDropdownItemNumber={50}
                                scrollableYearDropdown
                                maxDate={CURRENT_DATE}
                                placeholder="ДД/ММ/ГГГГ"
                                onChange={dateHandleChange(setFieldValue)}
                            />
                            {errors.birthday_date && touched.birthday_date && errors.birthday_date &&
                            <span className="error-text">{errors.birthday_date}</span>
                            }
                        </div>
                        <div className="modal-form__status-select">
                            <p style={
                                {
                                    color: errors.country &&
                                    touched.country &&
                                    errors.country ? "#FA445C" : "#1A051D"
                                }
                            }>Выбрать страну</p>
                            {countries && !isEmptyArray(countries) &&
                            <CustomSelect
                                className="select"
                                data={countries}
                                value={state.country}
                                onChange={countryHandler}
                            />}
                            {!countries && <CommonSpinner centered isLoading={true}/>}
                            {errors.country && touched.country && errors.country &&
                            <span className="error-text">{errors.country}</span>
                            }
                        </div>
                        <InputPhoneMask
                            label="Телефон"
                            propertyName="phone"
                            value={values.phone}
                            onChange={handleChange}
                            isError={errors.phone && touched.phone && errors.phone}
                            helperText={errors.phone && touched.phone && errors.phone}/>
                        <InputForm
                            label="электронная почта"
                            placeholder="Введите почту"
                            value={values.email}
                            propertyName="email"
                            onChange={handleChange}
                            isError={errors.email && touched.email && errors.email}
                            helperText={errors.email && touched.email && errors.email}
                        />
                        <InputForm
                            label="Аккаунт в телеграмм для рассылки тз"
                            placeholder="example"
                            value={values.telegram_login}
                            propertyName="telegram_login"
                            onChange={telegramLoginHandler(setFieldValue)}
                            isError={
                                (errors.telegram_login && touched.telegram_login && errors.telegram_login) ||
                                existsLogin
                            }
                            helperText={
                                (errors.telegram_login && touched.telegram_login && errors.telegram_login) ||
                                (existsLogin && "Указанный аккаунт уже существует")
                            }
                        />
                        <GeneralButton
                            type="submit"
                            className="modal-form__btn"
                            variant="outlined"
                            disabled={existsLogin}>
                            Сохранить
                        </GeneralButton>
                    </form>
                )
            }
        </Formik>
    );
};

const mapStateToProps = state => ({
    bloggerStatuses: state.bloggers.bloggerStatuses,
    countries: state.bloggers.countries,
    blogger: state.bloggers.blogger,
    existsLogin: state.bloggers.existsLogin
})
const mapDispatchToProps = dispatch => ({
    fetchBloggerStatuses: () => dispatch(fetchBloggerStatuses()),
    fetchCountriesList: () => dispatch(fetchCountriesList()),
    fetchCheckTelegramLogin: login => dispatch(fetchCheckTelegramLogin(login))
})

export default connect(mapStateToProps, mapDispatchToProps)(PersonalForm);