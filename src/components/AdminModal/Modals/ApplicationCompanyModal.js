import React, {useState} from 'react';
import AdminModal from "../AdminModal";
import {Formik} from "formik";
import InputForm from "../../kit/InputForm/InputForm";
import InputPhoneMask from "../../kit/InputPhoneMask/InputPhoneMask";
import DatePickerForm from "../../kit/DatePickerForm/DatePickerForm";
import GeneralButton from "../../kit/GeneralButton/GeneralButton";

import './index.scss';
import {
    fetchCreateAdvertiseCompany,
    fetchCreateApplicationCompany,
    fetchEditApplicationCompany
} from "../../../store/applications/applicationsActions";
import {connect} from "react-redux";
import {dateFormatter} from "../../../global/helpers";
import moment from 'moment';
import {CURRENT_DATE} from "../../../global/constants";
import {history} from "../../../store/configureStore";
import {ROUTE_ADVERTISE_PAGE} from "../../../global/Routes";

const MODAL_TYPES = {
    create: "Создать заявку",
    edit: "Редактирование"
}

const ApplicationCompanyModal = (
    {
        onClose,
        type = "create",
        fetchCreateApplicationCompany,
        initValues,
        fetchEditApplicationCompany,
        fetchCreateAdvertiseCompany
    }) => {

    const [state, setState] = useState({
        name: initValues ? initValues.name : "",
        company_name: initValues ? initValues.company_name : "",
        phone_number: initValues ? initValues.phone_number : "",
        deadline: initValues ? moment(initValues.deadline || CURRENT_DATE, "DD-MM-YYYY").toDate() : CURRENT_DATE,
        text: initValues ? initValues.text : "",
    })

    const dateHandleChange = setFieldValue => (date) => {
        setState(prevState => {
            return {...prevState, deadline: date}
        })
        setFieldValue('deadline', date)
    }

    function onSubmit(values) {
        const data = {...values};
        data.deadline = dateFormatter(state.deadline);

        if (type === "create") {
            fetchCreateApplicationCompany(data)
        }
        if (type === "edit") {
            fetchEditApplicationCompany(initValues.id, data)
        }
        if (type === "application") {
            if (initValues.campaign) {
                history.push(ROUTE_ADVERTISE_PAGE + `/${initValues.campaign}`)
            } else {
                fetchCreateAdvertiseCompany({request: initValues.id})
            }
        }
        onClose()
    }

    function validateForm(values) {
        const errors = {};
        if (!values.name) {
            errors.name = 'Обязательное поле';
        }
        if (!values.phone_number) {
            errors.phone_number = 'Обязательное поле';
        }
        if (!values.company_name) {
            errors.company_name = 'Обязательное поле';
        }
        if (!values.text) {
            errors.text = 'Обязательное поле';
        }
        return errors;
    }

    return (
        <AdminModal onClose={onClose}
                    title={MODAL_TYPES[type] || initValues.company_name}
                    application={initValues}>
            <Formik
                initialValues={state}
                validate={values => validateForm(values)}
                onSubmit={(values) => {
                    onSubmit(values)
                }}
            >
                {
                    ({
                         values,
                         errors,
                         touched,
                         handleChange,
                         setFieldValue,
                         dirty,
                         handleSubmit,
                     }) => (
                        <form className="modal-form" onSubmit={handleSubmit}>
                            <InputForm
                                label="Компания"
                                placeholder="Названия компании"
                                propertyName="company_name"
                                value={values.company_name}
                                disabled={type === "application"}
                                onChange={handleChange}
                                isError={errors.company_name && touched.company_name && errors.company_name}
                                helperText={errors.company_name && touched.company_name && errors.company_name}
                            />
                            <InputForm
                                label="ФИО"
                                propertyName="name"
                                value={values.name}
                                onChange={handleChange}
                                disabled={type === "application"}
                                placeholder="Введите ФИО"
                                isError={errors.name && touched.name && errors.name}
                                helperText={errors.name && touched.name && errors.name}
                            />
                            <InputPhoneMask
                                label="Телефон"
                                propertyName="phone_number"
                                value={values.phone_number}
                                onChange={handleChange}
                                type="number"
                                disabled={type === "application"}
                                isError={errors.phone_number && touched.phone_number && errors.phone_number}
                                helperText={errors.phone_number && touched.phone_number && errors.phone_number}/>
                            <div className="modal-form__form-datetime">
                                {type === "application" ?
                                    <p>дата дедлайна</p> :
                                    <p>выбрать дату дедлайна *</p>}
                                <DatePickerForm
                                    className="modal-form__form-datetime-calendar"
                                    selected={state.deadline}
                                    onChange={dateHandleChange(setFieldValue)}
                                    minDate={CURRENT_DATE}
                                    disabled={type === "application"}
                                />
                            </div>
                            <InputForm
                                label="описание ТЗ *"
                                placeholder="Опишите что вам нужно"
                                value={values.text}
                                propertyName="text"
                                onChange={handleChange}
                                multiline
                                rows={4}
                                disabled={type === "application"}
                                isError={errors.text && touched.text && errors.text}
                                helperText={errors.text && touched.text && errors.text}
                            />
                            <GeneralButton
                                type="submit"
                                className="modal-form__btn"
                                variant="outlined"
                                disabled={type === 'edit' && !dirty}>

                                {type !== "application" && "Сохранить"}
                                {type === "application" && initValues.campaign && "Перейти"}
                                {type === "application" && !initValues.campaign && "Создать"}

                            </GeneralButton>
                        </form>
                    )
                }
            </Formik>
        </AdminModal>
    );
};

const mapDispatchToProps = dispatch => ({
    fetchCreateApplicationCompany: data => dispatch(fetchCreateApplicationCompany(data)),
    fetchEditApplicationCompany: (id, data) => dispatch(fetchEditApplicationCompany(id, data)),
    fetchCreateAdvertiseCompany: data => dispatch(fetchCreateAdvertiseCompany(data))
})

export default connect(null, mapDispatchToProps)(ApplicationCompanyModal);