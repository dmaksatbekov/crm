import React, {useEffect, useRef} from 'react';
import './index.scss';
import CloseIcon from "../kit/Icons/CloseIcon";
import Backdrop from "@material-ui/core/Backdrop";
import {makeStyles} from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    backdrop: {
        zIndex: theme.zIndex.drawer + 9999999,
        color: '#fff',
        backgroundColor: "transparent"
    },
}));

const AdminModal = (
    {
        onClose,
        application,
        title,
        children,
        backdrop,
        width = "480px",
        className
    }) => {
    const classes = useStyles();
    const wrapRef = useRef(null);
    useEffect(function () {
        document.body.style.overflow = 'hidden';
        document.addEventListener('mousedown', handleClickOutside);
        return function () {
            document.removeEventListener('mousedown', handleClickOutside);
            document.body.style.overflow = 'unset';
        }
    }, []);

    function handleClickOutside(event) {
        if (wrapRef && wrapRef.current && !wrapRef.current.contains(event.target)) {
            onClose();
        }
    }


    return (
        <Backdrop className={classes.backdrop} open onClick={() => null}>
            <div className={["admin-modal", className].join(" ")} ref={wrapRef} style={{width: width}}>
            <span onClick={onClose}>
                <CloseIcon
                    width={20}
                    height={20}
                    color="black"
                    className="admin-modal__close-icon"/>
            </span>
                {application &&
                <p className="admin-modal__application_id">ID {application.id}</p>}
                {title &&
                <h2>{title}</h2>}
                {children}
            </div>
        </Backdrop>
    );
};

export default AdminModal;