import React from "react";
import {Transition} from "react-transition-group";
import BellIcon from "../kit/Icons/BellIcon";
import HomeIcon from "../kit/Icons/HomeIcon";
import {NavLink} from "react-router-dom";
import './index.scss';
import StarIcon from "../kit/Icons/StarIcon";
import UnionIcon from "../kit/Icons/UnionIcon";
import SettingsIcon from "../kit/Icons/SettingsIcon";
import FileIcon from "../kit/Icons/FileIcon";
import {connect} from "react-redux";

const duration = 1000
const sidebarStyle = {
    transition: `width ${duration}ms`
}
const sidebarTransitionStyles = {
    entering: {width: "244px"},
    entered: {width: '244px'},
    exiting: {width: '50px'},
    exited: {width: "50px"},
}
const linkStyle = {
    transition: `opacity ${duration}ms`
}
const linkTransitionStyles = {
    entering: {opacity: 0.5},
    entered: {opacity: 1},
    exiting: {opacity: 0},
    exited: {opacity: 1}
}


const NAVS = [
    {
        title: "Главная",
        icon: <HomeIcon/>,
        link: "/dashboard"
    },
    {
        title: "Заявки",
        icon: <BellIcon/>,
        content: [
            {
                title: "Заявки компаний",
                link: "/applications/company"
            },
            {
                title: "Заявки блогеров",
                link: "/applications/bloggers"
            },
            {
                title: "Архив",
                link: "/applications/archive"
            }
        ]
    },
    {
        title: "Блогеры",
        icon: <StarIcon/>,
        link: "/bloggers"
    },
    {
        title: "Рекл. кампании",
        icon: <UnionIcon/>,
        link: "/rekcampaign"
    },
    {
        title: "Настройки",
        icon: <SettingsIcon/>,
        link: "/settings"
    },
    {
        title: "Отчет",
        icon: <FileIcon width={24} height={24} color="white"/>,
        link: "/report"
    },
]

const SidebarContent = ({isOpen, isActive, toggleMenu, wrapperRef, notifications}) => {
    const location = window.location.pathname;

    const renderLinks = () => {
        return <Transition in={isOpen} timeout={duration}>
            {(state) => (
                <div style={{
                    ...linkStyle,
                    ...linkTransitionStyles[state]
                }}>
                    <ul className="sidebar__navs-ul">
                        {NAVS.map((nav, ndx) => (
                            <React.Fragment key={ndx}>
                                {!nav.content ?
                                    <li className="sidebar__navs-list">
                                     <span className="sidebar__navs-list-title">
                                    <NavLink to={nav.link}>{nav.icon}{nav.title}</NavLink>
                                    </span>
                                    </li>
                                    :
                                    <ul className={`sidebar__navs-ul-inner 
                                    ${isActive && "sidebar__navs-ul-inner--is-open"}`}
                                        ref={wrapperRef}>
                                        <>
                                            <li className={`sidebar__navs-list 
                                            ${nav.content.some(el => el.link === location) &&
                                            "sidebar__navs-list--active"}`}
                                                onClick={() => toggleMenu()}>
                                                {nav.icon}
                                                <span
                                                    className={`sidebar__navs-list-title
                                                     ${nav.content.some(el => el.link === location) &&
                                                    "sidebar__navs-list-title--active"}`}>
                                        {nav.title}
                                                 </span>
                                                {nav.title === "Заявки" && notifications && !isActive &&
                                                (notifications.company_request + notifications.bloggers_request) > 0 &&
                                                <span className="sidebar__navs-list-notify">
                                                    {notifications.company_request + notifications.bloggers_request}
                                                </span>
                                                }
                                            </li>
                                            {nav.content && isActive && isOpen &&
                                            nav.content.map((content, idx) => (
                                                <li key={idx} className="sidebar__navs-inner-list">
                                                    <NavLink to={content.link}>
                                                        {content.title}

                                                        {notifications && content.title === "Заявки компании" &&
                                                        notifications.company_request > 0 && (
                                                            <span className="sidebar__navs-list-notify">
                                                    {notifications.company_request}
                                                </span>)}

                                                        {notifications && content.title === "Заявки блогеров" &&
                                                        notifications.bloggers_request > 0 && (
                                                            <span className="sidebar__navs-list-notify">
                                                    {notifications.bloggers_request}
                                                </span>)}

                                                    </NavLink>
                                                </li>
                                            ))}
                                        </>
                                    </ul>
                                }
                            </React.Fragment>
                        ))}
                    </ul>
                </div>
            )}
        </Transition>
    }

    return <Transition in={isOpen} timeout={duration}>
        {(state) => (
            <div className="sidebar__navs-root" style={{
                ...sidebarStyle,
                ...sidebarTransitionStyles[state],
            }}>
                {renderLinks()}
            </div>
        )}
    </Transition>
}

const mapStateToProps = state => ({
    notifications: state.applicationsInfo.notifications
})

export default connect(mapStateToProps)(SidebarContent);
