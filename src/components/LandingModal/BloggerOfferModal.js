import React, {useState} from 'react';
import {Formik} from "formik";
import InputForm from "../kit/InputForm/InputForm";
import GeneralButton from "../kit/GeneralButton/GeneralButton";
import AdminModal from "../AdminModal/AdminModal";
import CheckIcon from "../kit/Icons/CheckIcon";

const BloggerOfferModal = ({onClose, onSubmit}) => {

    const [isOpen, setOpen] = useState(false);

    function onSubmitting(values) {
        setOpen(true)
        onSubmit(values)
    }

    return (
        <AdminModal onClose={onClose}
                    className="offer-modal"
                    title="Причина отказа">
            {!isOpen ?
                <Formik
                    initialValues={{reason: ""}}
                    validate={values => {
                        const error = {};
                        if (!values.reason) {
                            error.reason = "Обязательное поле"
                        }
                    }}
                    onSubmit={(values) => {
                        onSubmitting(values)
                    }}
                >
                    {
                        ({
                             values,
                             errors,
                             touched,
                             handleChange,
                             dirty,
                             handleSubmit,
                         }) => (
                            <form className="modal-form" onSubmit={handleSubmit}>
                                <InputForm
                                    label="описание причины *"
                                    placeholder="Введите текст"
                                    value={values.reason}
                                    propertyName="reason"
                                    onChange={handleChange}
                                    multiline
                                    rows={4}
                                    isError={errors.reason && touched.reason && errors.reason}
                                    helperText={errors.reason && touched.reason && errors.reason}
                                />
                                <GeneralButton
                                    type="submit"
                                    className="modal-form__btn"
                                    variant="outlined"
                                    disabled={!dirty}>
                                    Отправить
                                </GeneralButton>
                            </form>
                        )
                    }
                </Formik> :
                <div className="complete-message">
                    <div className="complete-message__circle">
                        <CheckIcon width={24} height={24} color="#24AF47"/>
                    </div>
                    <h3 className="complete-message__text">
                        Сообщение успешно отправлено
                    </h3>
                </div>
            }
        </AdminModal>
    );
};

export default BloggerOfferModal;