import React, {useEffect, useState} from 'react';
import InputForm from "../kit/InputForm/InputForm";
import InputPhoneMask from "../kit/InputPhoneMask/InputPhoneMask";
import {isEmptyArray} from "../../global/helpers";
import CustomSelect from "../kit/SocialSelect/CustomSelect";
import GeneralButton from "../kit/GeneralButton/GeneralButton";
import {Formik} from "formik";
import {
    fetchCreateApplicationBlogger,
    fetchSocialNetworks
} from "../../store/applications/applicationsActions";
import {connect} from "react-redux";

const BloggersForm = (
    {
        socialNetworks,
        fetchCreateApplicationBlogger,
        fetchSocialNetworks,
        onClose,
    }) => {

    const [state, setState] = useState({
        name: "",
        surname: "",
        middle_name: "",
        phone_number: "",
        email: "",
    })

    useEffect(function () {
        fetchSocialNetworks()
    }, [fetchSocialNetworks])

    const [inputFields, setInputFields] = useState([
        {social_network: '', link: ''}
    ]);

    const handleInputChange = (index, event, item) => {
        const values = [...inputFields];
        if (event.target.name === "link") {
            values[index].link = event.target.value;
        } else {
            values[index].social_network = item;
        }

        setInputFields(values);
    };

    const handleAddFields = () => {
        const values = [...inputFields];
        values.push({social_network: '', link: ''});
        setInputFields(values);
    };

    function onSubmit(values) {
        const data = {...values};
        data.social_network = [...inputFields]
        fetchCreateApplicationBlogger(data)
        onClose()
    }

    function validateForm(values) {
        const errors = {};
        if (!values.name) {
            errors.name = 'Обязательное поле';
        }
        if (!values.middle_name) {
            errors.middle_name = 'Обязательное поле';
        }
        if (!values.surname) {
            errors.surname = 'Обязательное поле';
        }
        if (!values.phone_number) {
            errors.phone_number = 'Обязательное поле';
        }
        if (!values.email) {
            errors.email = 'Обязательное поле';
        }
        return errors;
    }

    return (
        <Formik
            initialValues={state}
            validate={values => validateForm(values)}
            onSubmit={(values) => {
                onSubmit(values)
            }}
        >
            {
                ({
                     values,
                     errors,
                     touched,
                     handleChange,
                     dirty,
                     handleSubmit,
                 }) => (
                    <form className="modal-form" onSubmit={handleSubmit}>
                        <InputForm
                            label="Фамилия"
                            placeholder="Введите фамилия"
                            propertyName="surname"
                            value={values.surname}
                            onChange={handleChange}
                            isError={errors.surname && touched.surname && errors.surname}
                            helperText={errors.surname && touched.surname && errors.surname}
                        />
                        <InputForm
                            label="Имя"
                            propertyName="name"
                            value={values.name}
                            onChange={handleChange}
                            placeholder="Введите имя"
                            isError={errors.name && touched.name && errors.name}
                            helperText={errors.name && touched.name && errors.name}
                        />
                        <InputForm
                            label="Отчество"
                            propertyName="middle_name"
                            value={values.middle_name}
                            onChange={handleChange}
                            placeholder="Введите отчество"
                            isError={errors.middle_name && touched.middle_name && errors.middle_name}
                            helperText={errors.middle_name && touched.middle_name && errors.middle_name}
                        />
                        {socialNetworks &&
                        !isEmptyArray(socialNetworks) &&
                        <div className="modal-form__socials">
                            <p>соц.сеть *</p>
                            {inputFields.map((field, ndx) => (
                                <div className="modal-form__socials-fields" key={ndx}>
                                    <CustomSelect
                                        data={socialNetworks}
                                        index={ndx}
                                        onChange={handleInputChange}
                                    />
                                    <InputForm
                                        propertyName="link"
                                        value={field.link}
                                        onChange={event => handleInputChange(ndx, event)}
                                        placeholder="Ссылка на соц.сеть"
                                    />
                                </div>
                            ))}
                            {socialNetworks && socialNetworks.length > inputFields.length &&
                            <span onClick={handleAddFields} className="modal-form__socials-add">
                                    Добавить еще
                                </span>}
                        </div>
                        }
                        <InputPhoneMask
                            label="Телефон"
                            propertyName="phone_number"
                            value={values.phone_number}
                            onChange={handleChange}
                            isError={errors.phone_number && touched.phone_number && errors.phone_number}
                            helperText={errors.phone_number && touched.phone_number && errors.phone_number}/>
                        <InputForm
                            label="электронная почта"
                            placeholder="Введите почту"
                            value={values.email}
                            propertyName="email"
                            onChange={handleChange}
                            isError={errors.email && touched.email && errors.email}
                            helperText={errors.email && touched.email && errors.email}
                        />
                        <GeneralButton
                            type="submit"
                            className="modal-form__btn"
                            variant="outlined">
                            Сохранить
                        </GeneralButton>
                    </form>
                )
            }
        </Formik>
    );
};

const mapStateToProps = state => ({
    socialNetworks: state.applicationsInfo.socialNetworks
})

const mapDispatchToProps = dispatch => ({
    fetchCreateApplicationBlogger: data => dispatch(fetchCreateApplicationBlogger(data)),
    fetchSocialNetworks: () => dispatch(fetchSocialNetworks())
})

export default connect(mapStateToProps, mapDispatchToProps)(BloggersForm);