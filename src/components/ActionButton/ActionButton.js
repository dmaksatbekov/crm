import React, {useEffect, useRef, useState} from 'react';
import Button from "@material-ui/core/Button";
import Popper from "@material-ui/core/Popper";
import Grow from "@material-ui/core/Grow";
import Paper from "@material-ui/core/Paper";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import MenuList from "@material-ui/core/MenuList";
import MenuItem from "@material-ui/core/MenuItem";
import MoreIcon from "../kit/Icons/MoreIcon";
import './index.scss';
import EditIcon from "../kit/Icons/EditIcon";
import FolderPlusIcon from "../kit/Icons/FolderPlusIcon";
import BellIcon from "../kit/Icons/BellIcon";
import TrashIcon from "../kit/Icons/TrashIcon";

const ActionButton = ({onEdit, onSendToArchive, disabled, sendToApplication, onRemove, editable}) => {
    const [open, setOpen] = useState(false);
    const anchorRef = useRef(null);

    const handleToggle = (e) => {
        setOpen((prevOpen) => !prevOpen);
        e.stopPropagation()
    };

    const handleClose = (event) => {
        if (anchorRef.current && anchorRef.current.contains(event.target)) {
            return;
        }
        event.stopPropagation()
        setOpen(false);
    };

    function handleListKeyDown(event) {
        if (event.key === 'Tab') {
            event.preventDefault();
            setOpen(false);
        }
    }


    function onSelect(e) {
        e.stopPropagation()
        onEdit()
        setOpen(false);
    }

    function sendToArchive(e) {
        e.stopPropagation()
        onSendToArchive();
        setOpen(false);
    }

    function remove(e) {
        e.stopPropagation()
        onRemove()
        setOpen(false);
    }

    function sendToApplications(e) {
        e.stopPropagation()
        sendToApplication()
        setOpen(false);
    }

    const prevOpen = React.useRef(open);
    useEffect(() => {
        if (prevOpen.current === true && open === false) {
            anchorRef.current.focus();
        }

        prevOpen.current = open;
    }, [open]);

    return (
        <div className="action-button">
            <Button
                ref={anchorRef}
                aria-controls={open ? 'menu-list-grow' : undefined}
                aria-haspopup="true"
                onClick={handleToggle}
            >
                <MoreIcon color="#6A7083" className="icon"/>
            </Button>
            <Popper open={open} anchorEl={anchorRef.current} role={undefined} transition disablePortal>
                {({TransitionProps, placement}) => (
                    <Grow
                        {...TransitionProps}
                        style={{transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom'}}
                    >
                        <Paper>
                            <ClickAwayListener onClickAway={handleClose}>
                                {editable ?
                                    <MenuList autoFocusItem={open} id="menu-list-grow" onKeyDown={handleListKeyDown}>
                                        <MenuItem onClick={onSelect} disabled={disabled}>
                                            <EditIcon/> Редактировать
                                        </MenuItem>
                                        <MenuItem onClick={sendToArchive} disabled={disabled}>
                                            <FolderPlusIcon/> В архив
                                        </MenuItem>
                                    </MenuList>
                                    :
                                    <MenuList autoFocusItem={open} id="menu-list-grow" onKeyDown={handleListKeyDown}>
                                        <MenuItem onClick={sendToApplications} disabled={disabled}>
                                            <BellIcon
                                                width={16}
                                                height={16}
                                                color="#0099FF"
                                            /> В заявки
                                        </MenuItem>
                                        <MenuItem onClick={remove} disabled={disabled}>
                                            <TrashIcon/> Удалить
                                        </MenuItem>
                                    </MenuList>
                                }
                            </ClickAwayListener>
                        </Paper>
                    </Grow>
                )}
            </Popper>
        </div>
    );
};

export default ActionButton;