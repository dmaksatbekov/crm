import React, {useEffect, useState} from 'react';
import HeaderFilter from "../../components/HeaderFilter/HeaderFilter";
import {
    fetchBloggerApplications,
    fetchCompanyApplications,
    fetchEditApplicationStatus, fetchRemoveApplication, fetchSendToApplication
} from "../../store/applications/applicationsActions";
import {connect} from "react-redux";
import {dateFormatter, isEmptyArray, useDebounce} from "../../global/helpers";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";
import CompanyItem from "../ApplicationsCompany/CompanyItem";
import Table from "@material-ui/core/Table";
import NoResultIcon from "../../components/kit/Icons/NoResultIcon";
import CommonPagination from "../../components/CommonPagination/CommonPagination";
import './index.scss';
import CommonSpinner from "../../components/kit/CommonSpinner/CommonSpinner";
import BloggerItem from "../ApplicationBlogger/BloggerItem";
import CommonAlert from "../../components/CommonAlert/CommonAlert";
import {NUMBER_REQUEST_STAT} from "../ApplicationsCompany/ApplicationsCompany";

const TAB_NAMES = [
    {name: "ЗАЯВКИ КОМПАНИЙ"},
    {name: "ЗАЯВКИ БЛОГЕРОВ"}
]

const COMPANY_TAB = 0;
const BLOGGER_TAB = 1;

const ApplicationsArchive = (
    {
        companyApplicationsList,
        fetchBloggerApplications,
        fetchCompanyApplications,
        fetchEditApplicationStatus,
        bloggerApplications,
        bloggersFailure,
        companyFailure,
        fetchRemoveApplication,
        fetchSendToApplication
    }) => {

    const [activeTab, setTab] = useState(0);
    const [selectedDate, setDate] = useState(null);
    const [search, setSearch] = useState("");
    const [loading, setLoading] = useState(true)
    const [page, setPage] = useState(1);

    const debouncedSearchTerm = useDebounce(search, 500);

    useEffect(function () {
        if (activeTab === BLOGGER_TAB) {
            getBloggerApplications()
        } else {
            getCompanyApplications()
        }
    }, [activeTab, page, debouncedSearchTerm, selectedDate])

    function clearFilters() {
        setSearch("");
        setDate(null);
    }

    function onChangeTab(index) {
        setTab(index);
        setPage(1)
        if (index === BLOGGER_TAB) {
            getBloggerApplications()
        } else {
            getCompanyApplications()
        }
    }

    async function getCompanyApplications() {
        setLoading(true)
        await fetchCompanyApplications(
            page,
            undefined,
            debouncedSearchTerm,
            selectedDate ? dateFormatter(selectedDate) : null,
            true
        )
        setLoading(false)
    }

    async function getBloggerApplications() {
        setLoading(true)
        await fetchBloggerApplications(
            page,
            TAB_NAMES[activeTab].code,
            search,
            selectedDate ? dateFormatter(selectedDate) : null,
            true
        )
        setLoading(false)

    }

    async function onEditStatus(id, status) {
        if (activeTab === COMPANY_TAB) {
            await fetchEditApplicationStatus(id, status, "company");
            getCompanyApplications()
        } else {
            await fetchEditApplicationStatus(id, status, "blogger");
            getBloggerApplications()
        }
    }


    return (
        <>
            <div className="applications-archive">
                <div className="application-company__navs">
                    {TAB_NAMES.map((tab, ndx) => (
                        <button
                            key={ndx}
                            className={`application-company__nav
                             ${activeTab === ndx && "application-company__nav--active"}`}
                            onClick={() => onChangeTab(ndx)}>{tab.name}</button>
                    ))}
                </div>
                <HeaderFilter
                    selectedDate={selectedDate}
                    dateHandler={date => setDate(date)}
                    search={search}
                    inputHandler={e => setSearch(e.target.value)}
                    onClear={clearFilters}
                />

                {loading && <CommonSpinner isLoading={loading} centered/>}

                {!loading && activeTab === COMPANY_TAB &&
                <>
                    <div className="application-company__table-root">
                        <Table className="application-company__table">
                            <TableHead>
                                <TableRow>
                                    <TableCell align="left">ДАТА</TableCell>
                                    <TableCell align="left">ID</TableCell>
                                    <TableCell align="left">ФИО</TableCell>
                                    <TableCell align="left">Компания</TableCell>
                                    <TableCell align="left">Телефон</TableCell>
                                    <TableCell align="left">Дедлайн</TableCell>
                                    <TableCell align="left">Статус</TableCell>
                                    <TableCell align="left">Действия</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {!loading && companyApplicationsList &&
                                companyApplicationsList.results &&
                                !isEmptyArray(companyApplicationsList.results) &&
                                companyApplicationsList.results.sort((a, b) =>
                                    NUMBER_REQUEST_STAT[a.request_status] - NUMBER_REQUEST_STAT[b.request_status]
                                ).map(item => (
                                    <CompanyItem
                                        data={item}
                                        key={item.id}
                                        onChangeStatus={onEditStatus}
                                        onSendToApplication={fetchSendToApplication}
                                        onRemove={fetchRemoveApplication}
                                    />
                                ))
                                }
                            </TableBody>
                        </Table>

                        {!loading && companyApplicationsList &&
                        companyApplicationsList.results &&
                        isEmptyArray(companyApplicationsList.results) &&
                        <div className="no-result">
                            <NoResultIcon width={100} height={140}/>
                            <p className="no-result__text">Результатов не найдено</p>
                        </div>
                        }

                        {!loading && companyApplicationsList &&
                        companyApplicationsList.page_count &&
                        companyApplicationsList.page_count > 1 &&
                        <CommonPagination
                            count={companyApplicationsList.page_count}
                            currentPage={page}
                            onPageChange={(e, page) => setPage(page)}/>
                        }
                    </div>
                </>
                }


                {!loading && activeTab === BLOGGER_TAB &&
                <div className="application-blogger__table-root">
                    <Table className="application-blogger__table">
                        <TableHead>
                            <TableRow>
                                <TableCell align="left">ДАТА</TableCell>
                                <TableCell align="left">ID</TableCell>
                                <TableCell align="left">ФИО</TableCell>
                                <TableCell align="left">Соц.сети</TableCell>
                                <TableCell align="left">Телефон</TableCell>
                                <TableCell align="left">Статус</TableCell>
                                <TableCell align="left">Действия</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {!loading &&
                            bloggerApplications &&
                            Array.isArray(bloggerApplications.results) &&
                            bloggerApplications.results.sort((a, b) =>
                                NUMBER_REQUEST_STAT[a.request_status] - NUMBER_REQUEST_STAT[b.request_status]
                            ).map((item => (
                                <BloggerItem
                                    onChangeStatus={onEditStatus}
                                    data={item}
                                    onSendToApplication={fetchSendToApplication}
                                    onRemove={fetchRemoveApplication}
                                    key={item.id}/>
                            )))}
                        </TableBody>
                    </Table>

                    {loading && <CommonSpinner isLoading={loading} centered/>}

                    {!loading && bloggerApplications &&
                    bloggerApplications.results &&
                    isEmptyArray(bloggerApplications.results) &&
                    <div className="no-result">
                        <NoResultIcon width={100} height={140}/>
                        <p className="no-result__text">Результатов не найдено</p>
                    </div>
                    }

                    {!loading && bloggerApplications &&
                    bloggerApplications.page_count &&
                    bloggerApplications.page_count > 1 &&
                    <CommonPagination
                        count={bloggerApplications.page_count}
                        currentPage={page}
                        onPageChange={(e, page) => setPage(page)}/>
                    }

                    {!loading && bloggersFailure && <CommonAlert
                        status={bloggersFailure.status}
                        message={bloggersFailure.statusText}
                    />}
                </div>
                }
            </div>
        </>
    );
};

const mapStateToProps = state => ({
    companyApplicationsList: state.applicationsInfo.companyApplicationsList,
    companyFailure: state.applicationsInfo.companyFailure,
    bloggerApplications: state.applicationsInfo.bloggerApplications,
    bloggersFailure: state.applicationsInfo.bloggersFailure,
})

const mapDispatchToProps = dispatch => ({
    fetchCompanyApplications: (page, status, search, date, is_archive) =>
        dispatch(fetchCompanyApplications(page, status, search, date, is_archive)),
    fetchBloggerApplications: (page, status, search, date, is_archive) => dispatch(fetchBloggerApplications(page, status, search, date, is_archive)),
    fetchEditApplicationStatus: (id, status, type) => dispatch(fetchEditApplicationStatus(id, status, type)),
    fetchRemoveApplication: (id, type) => dispatch(fetchRemoveApplication(id, type)),
    fetchSendToApplication: (id, type) => dispatch(fetchSendToApplication(id, type)),

});

export default connect(mapStateToProps, mapDispatchToProps)(ApplicationsArchive);