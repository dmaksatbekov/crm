import React, {useEffect, useState} from 'react';
import './index.scss';
import LikeIcon from "../../components/kit/Icons/LikeIcon";
import BloggerOfferModal from "../../components/LandingModal/BloggerOfferModal";
import {fetchBloggerOffer} from "../../store/bloggers/bloggersActions";
import {connect} from "react-redux";
import CommonSpinner from "../../components/kit/CommonSpinner/CommonSpinner";
import {convertDateWithMonthName, numberWithSpaces} from "../../global/helpers";
import {fetchEditBloggerOfferStatus} from "../../store/advertisement/advertisementActions";
import {AGREEMENT_STATUS, REJECTED_STATUS} from "../../global/constants";
import moment from "moment";

const TZ = 0;

const TABS = ["Описание ТЗ", "Рекламные услуги"];

const BloggerOffer = ({fetchBloggerOffer, offer, fetchEditBloggerOfferStatus, ...props}) => {

    const {offerId} = props.match.params;

    useEffect(function () {
        fetchBloggerOffer(offerId)
    }, [fetchBloggerOffer, offerId])

    const [activeTab, setTab] = useState(TZ);

    const [isOpen, toggleModal] = useState(false);

    function onAccept() {
        fetchEditBloggerOfferStatus(offer.id, {answer: AGREEMENT_STATUS});
    }

    function onReject(value) {
        const data = {
            rejection_cause: value.reason,
            answer: REJECTED_STATUS
        }
        fetchEditBloggerOfferStatus(offer.id, data)
    }

    return (
        <>
            {offer ?
                <div className="offer-root">
                    <div className="offer-root__info-block">
                        <h6 className="offer-root__date">
                            Дата: {convertDateWithMonthName(moment(offer.deadline_date, "YYYY-MM-DD").toDate())}г.
                        </h6>
                        <h1 className="offer-root__title">
                            Техническое задание
                        </h1>
                        <div className="offer-root__tabs">
                            {TABS.map((tabName, ndx) => (
                                <button
                                    className={`tab_name 
                        ${activeTab === ndx ? "tab_name__active" : ""}`}
                                    key={ndx} onClick={() => setTab(ndx)}>
                                    <span className="tab-text">
                                        {tabName}
                                    </span>
                                </button>
                            ))}
                        </div>
                        <div className="offer-root__tab-info">
                            {activeTab === TZ ?
                                <div className="text-info">
                                    <p className="text-info__text">
                                        {offer.text_tz}
                                    </p>
                                </div>
                                :
                                <div className="offer-root__service-info">
                                    <ul>
                                        {offer.bill && offer.bill.map(item => (
                                            <li key={item.id}>
                                                <span className="name">
                                                    {item.advertisement_name}
                                                </span>
                                                <span className="value">
                                                    {item.price}сом
                                                </span>
                                            </li>
                                        ))}
                                    </ul>
                                </div>
                            }
                        </div>
                        <div className="offer-root__footer">
                            <h3 className="offer-root__total">
                                итого бюджет:
                                <span className="total-sum">{offer.bill &&
                                numberWithSpaces(offer.bill.reduce((acc, cur) =>
                                    acc + cur.price, 0))}
                                </span> сом
                            </h3>
                            <div className="offer-root__btns">
                                <button
                                    disabled={offer.answer === AGREEMENT_STATUS}
                                    className={`offer-root__btn 
                                    ${offer.answer === AGREEMENT_STATUS
                                        ? "offer-root__btn--disabled"
                                        : "offer-root__btn--active"}`} onClick={onAccept}>
                                    <LikeIcon color="#fff"/>
                                    Принять тз
                                </button>
                                <button
                                    disabled={(offer.answer === REJECTED_STATUS ||
                                        offer.answer === AGREEMENT_STATUS)}
                                    className={`offer-root__btn 
                                    ${(offer.answer === REJECTED_STATUS ||
                                        offer.answer === AGREEMENT_STATUS)
                                        ? "offer-root__btn--disabled"
                                        : "offer-root__btn--active"}`}
                                    onClick={() => toggleModal(true)}>
                                    <LikeIcon color="#fff" className="rotate-icon"/>
                                    отказаться
                                </button>
                            </div>

                        </div>

                    </div>
                </div>
                : <CommonSpinner isLoading={!offer} centered/>}
            {isOpen && <BloggerOfferModal
                onClose={() => toggleModal(false)}
                onSubmit={onReject}/>}
        </>
    );
};

const mapStateToProps = state => ({
    offer: state.bloggers.offer
});

const mapDispatchToProps = dispatch => ({
    fetchBloggerOffer: id => dispatch(fetchBloggerOffer(id)),
    fetchEditBloggerOfferStatus: (offerId, data) =>
        dispatch(fetchEditBloggerOfferStatus(offerId, data))
})

export default connect(mapStateToProps, mapDispatchToProps)(BloggerOffer);