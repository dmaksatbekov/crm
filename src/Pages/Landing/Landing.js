import React, {useEffect, useLayoutEffect, useState} from 'react';
import './index.scss';
import Button from "@material-ui/core/Button";
import PartnersItem from "./PartnersItem/PartnersItem";
import LandingExpansionPanel from "./LandingExpansionPanel/LandingExpansionPanel";
import blackLogo from '../../assets/logo_black-text.png';

import 'animate.css/animate.css'
import LandingModal from "../../components/LandingModal/LandingModal";
import Toolbar from "./Toolbar/Toolbar";
import {connect} from "react-redux";
import {
    fetchBloggers,
    fetchLandingInfo,
    fetchPartners,
    fetchPromoBloggers,
    fetchQuestions, fetchSeo
} from "../../store/landing/landingActions";
import {numberWithSpaces} from "../../global/helpers";
import AutoPlayCarousel from "./LandingCarousel";
import {fetchProjectSocialNetworks} from "../../store/settings/settingsActions";
import MetaHelmet from "../MetaHelmet/MetaHelmet";
import Carousel from 'react-multi-carousel';
import 'react-multi-carousel/lib/styles.css';
import CentralAsiaIcon from "../../components/kit/Icons/CentralAsiaIcon";

const autoPlayDelay = 5000;

const Landing = (
    {
        fetchLandingInfo,
        landingInfo,
        fetchBloggers,
        bloggers,
        partners,
        fetchPartners,
        fetchQuestions,
        questions,
        fetchPromoBloggers,
        promoBloggers,
        fetchProjectSocialNetworks,
        socials,

        fetchSeo,
        seo
    }) => {
    const [activeItemIndex, setActiveItemIndex] = useState(0);
    const [activePartnerIndex, setActivePartnerIndex] = useState(0);
    const [size, setSize] = useState(0);
    const [cardItem, setItem] = useState(6);
    const [partnerItem, setPartnerItem] = useState(6);
    const [isOpenMobileMenu, toggleMenu] = useState(false);

    const [isOpenModal, toggleModal] = useState(null);

    let noOfItems = 5;

    let noOfPartners = 6;

    useEffect(function () {
        fetchLandingInfo()
        fetchBloggers()
        fetchPartners()
        fetchQuestions()
        fetchPromoBloggers()
        fetchProjectSocialNetworks()
        fetchSeo()
    }, [fetchBloggers, fetchLandingInfo])

    useLayoutEffect(() => {
        function updateSize() {
            setSize(window.innerWidth);
        }

        window.addEventListener('resize', updateSize);
        updateSize();
        if (size < 1000 && size > 600) {
            setItem(3)
            setPartnerItem(4)
        } else if (size < 600) {
            setItem(1)
            setPartnerItem(2)
        } else {
            setItem(5)
            setPartnerItem(6)
        }
        return () => window.removeEventListener('resize', updateSize);
    }, [size]);


    useEffect(function () {

        const interval = setInterval(function () {
            // setActiveItemIndex((activeItemIndex + 1) % (noOfItems - cardItem));
            setActivePartnerIndex((activePartnerIndex + 1) % (noOfPartners - partnerItem));
        }, autoPlayDelay)
        return function () {
            clearInterval(interval);
        }
    }, [activeItemIndex, activePartnerIndex, cardItem, noOfItems, noOfPartners, partnerItem])

    function onChangePartnersItem(index) {
        setActivePartnerIndex(index)
    }

    function scrollToApplications() {
        const anchor = document.querySelector('#applications')
        anchor.scrollIntoView({behavior: 'smooth', block: size < 600 ? 'start' : 'center'})
        toggleMenu(false)
    }

    function scrollToFAQ() {
        const anchor = document.querySelector('#faq')
        anchor.scrollIntoView({behavior: 'smooth', block: 'start'})
        toggleMenu(false)
    }


    function scrollToBloggers() {
        const anchor = document.querySelector('#bloggers')
        anchor.scrollIntoView({behavior: 'smooth', block: 'start'})
        toggleMenu(false)
    }

    function scrollToInstruction() {
        const anchor = document.querySelector('#instruction')
        anchor.scrollIntoView({behavior: 'smooth', block: 'center'})
        toggleMenu(false)
    }

    function openModal(type) {
        toggleModal(prevState => {
            return {...prevState, type}
        })
    }


    const info = landingInfo && landingInfo[0];
    return (
        <>
            <div className="landing">
                <div className="landing__top-info"
                     style={{height: isOpenMobileMenu ? "1010px" : size < 900 ? "725px" : "800px"}}>
                    <Toolbar
                        scrollToApplications={scrollToApplications}
                        scrollToBloggers={scrollToBloggers}
                        scrollToFAQ={scrollToFAQ}
                        size={size}
                        isOpenMobileMenu={isOpenMobileMenu}
                        toggleMenu={toggleMenu}
                        number={info && info.number}
                        socials={socials}
                    />
                    <div className="landing__promo container">
                        <div className="landing__promo-text-root">
                            <h1>{info && info.upper_first_text}</h1>
                            <h2>{info && info.upper_second_text}</h2>
                            <p>{info && info.upper_third_text}</p>
                            <div className="landing__promo-application-btns">
                                <button onClick={scrollToApplications} className="landing__promo-application-btn">
                                    Оставить заявку
                                </button>
                                <button onClick={scrollToInstruction}
                                        className="landing__promo-application-instruction">
                                    Видеоинструкция
                                </button>
                            </div>
                        </div>
                        {promoBloggers &&
                        <div className="landing__promo-bloggers">
                            {promoBloggers.map((item, ndx) => {
                                if (ndx < 7 && item.blogger && item.blogger.image) {
                                    return (
                                        <div key={ndx} className={`landing__promo-image-${ndx + 1}`}>
                                            <img src={item.blogger.image} alt="#"/>
                                        </div>
                                    )
                                } else return null
                            })}
                        </div>
                        }
                    </div>
                </div>

                <div className="landing__applications container">
                    <div className="landing__applications-items" id="applications">
                        <div className="landing__applications-item">
                            <div className="landing__applications-item-image-root">
                                {info &&
                                <img className="landing__applications-item-image"
                                     src={info.im_blogger_image} alt="image"/>}
                            </div>
                            <h5>{info && info.im_blogger_title}</h5>
                            <p>{info && info.im_blogger_text}</p>
                            <Button
                                onClick={() => openModal("blogger")}
                                variant="contained" className="landing__our-question-btn">
                                Оставить заявку
                            </Button>
                        </div>
                        <div className="landing__applications-item">
                            <div className="landing__applications-item-image-root">
                                {info &&
                                <img className="landing__applications-item-image"
                                     src={info.im_rekl_image} alt="image"/>}
                            </div>
                            <h5>{info && info.im_rekl_title}</h5>
                            <p>{info && info.im_rekl_text}</p>
                            <Button
                                onClick={() => openModal("company")}
                                variant="contained" className="landing__our-question-btn">
                                Оставить заявку
                            </Button>
                        </div>
                    </div>
                </div>

                {bloggers && bloggers.length > 0 &&
                <div className="landing__our-bloggers" id="bloggers">
                    <h2>Наши блогеры</h2>
                    <div className="landing__our-bloggers-cards">
                        <AutoPlayCarousel cardItem={cardItem} bloggers={bloggers}/>
                    </div>
                    <Button onClick={scrollToApplications} variant="contained" className="landing__our-bloggers-btn">
                        оставить заявку
                    </Button>
                </div>
                }

                {partners &&
                <div className="landing__partners container">
                    <h3>С нами сотрудничают</h3>
                    <p>Наши партнеры уже пользуются нашим сервисом и уже получают поток клиентов</p>

                    <div className="landing__partners-items">
                        <Carousel
                            additionalTransfrom={1}
                            arrows={false}
                            autoPlay
                            autoPlaySpeed={5000}
                            centerMode={true}
                            className=""
                            containerClass="carousel-container"
                            dotListClass="landing__partners-items-navigation"
                            draggable
                            focusOnSelect={false}
                            infinite
                            keyBoardControl
                            minimumTouchDrag={80}
                            renderButtonGroupOutside={false}
                            renderDotsOutside
                            responsive={{
                                desktop: {
                                    breakpoint: {
                                        max: 3000,
                                        min: 1024
                                    },
                                    items: 4,
                                    partialVisibilityGutter: 40
                                },
                                mobile: {
                                    breakpoint: {
                                        max: 464,
                                        min: 0
                                    },
                                    items: 1,
                                    partialVisibilityGutter: 30
                                },
                                tablet: {
                                    breakpoint: {
                                        max: 1024,
                                        min: 464
                                    },
                                    items: 2,
                                    partialVisibilityGutter: 30
                                }
                            }}
                            showDots={true}
                            sliderClass=""
                            slidesToSlide={1}
                        >
                            {partners.map(partner => (
                                <PartnersItem key={partner.id} image={partner.image}/>
                            ))}
                        </Carousel>
                    </div>
                </div>
                }
                {questions &&
                <div className="landing__faq-root">
                    <div className="landing__faq" id="faq">
                        <h3>FAQ: Частые вопросы</h3>
                        <div className="landing__faq-panels">
                            {questions && questions.map(item => (
                                <LandingExpansionPanel key={item.id} title={item.title}>
                                    {item.text}
                                </LandingExpansionPanel>
                            ))}
                        </div>
                        <Button variant="contained" onClick={scrollToApplications}
                                className="landing__our-question-btn">
                            Задать свой вопрос
                        </Button>
                    </div>
                </div>
                }
                <div className="landing__video-instruction-root" id="instruction">
                    <div className="landing__video-instruction container">
                        <div className="landing__video-instruction-text">
                            <h3>{info && info.lower_title}</h3>
                            <p>{info && info.lower_text}</p>
                            <Button
                                onClick={() => openModal("video")}
                                variant="contained">
                                Смотреть видео
                            </Button>
                        </div>
                        <div className="landing__video-instruction-image-root">
                            {info && info.lower_image &&
                            <img
                                className="landing__video-instruction-image"
                                src={info.lower_image} alt="image"/>}
                        </div>
                    </div>
                </div>
            </div>
            <footer>
                <div className="landing__footer container">
                    <div className="landing__footer-info">
                        <img
                            className="landing__footer-info-logo"
                            src={blackLogo}
                            alt="logo"/>
                        <div className="landing__footer-info-nav">
                            <span onClick={scrollToFAQ}>
                                FAQ
                            </span>
                            <span onClick={scrollToBloggers}>
                                Блогеры
                            </span>
                            <span onClick={scrollToApplications}>
                                Работать с нами
                            </span>
                        </div>
                        {socials && (
                            <div className="landing__footer-info-socials">
                                {socials.map(social => (
                                    <a href={social.url} target="_blank" rel="noopener noreferrer"
                                       className="landing__footer-info-social-item"
                                       key={social.id}>
                                        <img src={social.image} alt="#"/>
                                    </a>
                                ))}
                            </div>
                        )}
                        <div className="landing__footer-info-application">
                            {info &&
                            <p className="landing__footer-info-application-phone">
                                <a className="mobile-number --black"
                                   href={`tel:${info.number}`}>{numberWithSpaces(info.number || "")}</a>
                            </p>
                            }
                            <button onClick={scrollToApplications} className="landing__footer-info-application-btn">
                                Оставить заявку
                            </button>
                        </div>
                    </div>
                    <div className="landing__footer-agreement">
                        <div className="landing__footer-copyright">
                            <p>Copyright © 2010-2020 All rights reserved.</p>
                            <div className="central_asia">
                                <p>Сайт создан при поддержке</p>
                                <CentralAsiaIcon/>
                            </div>
                        </div>
                        <a href="https://mitapp.pro/" target="_tab" rel="noopener noreferrer">
                            Сайт разработан <span>MITAPP</span></a>
                    </div>
                </div>
            </footer>

            {isOpenModal &&
            <LandingModal
                isOpen={isOpenModal}
                type={isOpenModal.type}
                video={info && info.how_to_work_with_us_link}
                onClose={() => toggleModal(null)}/>}

            <MetaHelmet metaData={seo}/>

        </>
    );
};

const mapStateToProps = state => ({
    landingInfo: state.landingInfo.landingInfo,
    bloggers: state.landingInfo.bloggers,
    partners: state.landingInfo.partners,
    questions: state.landingInfo.questions,
    promoBloggers: state.landingInfo.promoBloggers,
    socials: state.landingInfo.socials,

    seo: state.landingInfo.seo
});

const mapDispatchToProps = dispatch => ({
    fetchLandingInfo: () => dispatch(fetchLandingInfo()),
    fetchBloggers: () => dispatch(fetchBloggers()),
    fetchPartners: () => dispatch(fetchPartners()),
    fetchQuestions: () => dispatch(fetchQuestions()),
    fetchPromoBloggers: () => dispatch(fetchPromoBloggers()),
    fetchProjectSocialNetworks: () => dispatch(fetchProjectSocialNetworks()),

    fetchSeo: () => dispatch(fetchSeo())
});

export default connect(mapStateToProps, mapDispatchToProps)(Landing);