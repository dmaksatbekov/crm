import React, {useState} from 'react';
import {NavLink} from "react-router-dom";
import logo from "../../../assets/logo.png";
import menu from "../../../assets/menu.png";
import Animate from "animate.css-react";
import CloseIcon from "../../../components/kit/Icons/CloseIcon";
import {numberWithSpaces} from "../../../global/helpers";
import LogoIcon from "../../../components/kit/Icons/LogoIcon";

const Toolbar = (
    {
        scrollToFAQ,
        scrollToBloggers,
        scrollToApplications,
        size,
        isOpenMobileMenu,
        toggleMenu,
        number,

        socials
    }) => {

    return (
        <div className="header-container">
            <header className={size < 451 && isOpenMobileMenu ? "opened" : ""}>
                <div className="landing__header-logo-root">
                    <NavLink to="/">
                        <LogoIcon className="landing__logo"/>
                    </NavLink>
                </div>
                {size < 900 ?
                    <>
                        {isOpenMobileMenu ?
                            <span className="landing__header-menu-icon" onClick={() => toggleMenu(false)}>
                            <CloseIcon color={!isOpenMobileMenu ? "white" : "black"}/>
                        </span> :
                            <>
                                <a href={`tel:${number}`} className="mobile-menu-phone">
                                    {number ? numberWithSpaces(number) : ""}
                                        </a>
                                <img className="landing__header-menu-icon"
                                     onClick={() => toggleMenu(true)}
                                     src={menu} width={24} height={24} alt="#"/>
                            </>
                        }
                        <div className="landing__header-navi-mobile">
                            {isOpenMobileMenu &&
                            <Animate
                                component="div"
                                appear="fadeInDown"
                                durationAppear={1000}>
                                <div className="landing__mobile-navs">
                                    <span onClick={scrollToFAQ}>FAQ</span>
                                    <span onClick={scrollToBloggers}>Блогеры</span>
                                    <span onClick={scrollToApplications}>Работать с нами</span>
                                </div>
                                <div className="landing__mobile-application">
                                    {number && <span>
                                       <a className="mobile-number --black" href={`tel:${number}`}>
                                           {numberWithSpaces(number || "")}
                                       </a>
                                    </span>}
                                    {socials && (
                                        <div className="landing__footer-info-socials mobile-header-socials">
                                            {socials.map(social => (
                                                <a href={social.url} target="_blank" rel="noopener noreferrer"
                                                   className="landing__footer-info-social-item"
                                                   key={social.id}>
                                                    <img src={social.image} alt="#"/>
                                                </a>
                                            ))}
                                        </div>
                                    )}
                                    <button onClick={scrollToApplications}>Оставить заявку</button>
                                </div>
                            </Animate>}
                        </div>
                    </>
                    :
                    <div className="landing__header-navi">
                        <div className="landing__header-navs">
                            <span onClick={scrollToFAQ}>FAQ</span>
                            <span onClick={scrollToBloggers}>Блогеры</span>
                            <span onClick={scrollToApplications}>Работать с нами</span>
                        </div>
                        <div className="landing__header-application">
                            {socials && (
                                <div className="landing__footer-info-socials header-socials">
                                    {socials.map(social => (
                                        <a href={social.url} target="_blank" rel="noopener noreferrer"
                                           className="landing__footer-info-social-item"
                                           key={social.id}>
                                            <img src={social.image} alt="#"/>
                                        </a>
                                    ))}
                                </div>
                            )}
                            {number && <span><a className="mobile-number" href={`tel:${number}`}>{numberWithSpaces(number || "")}</a></span>}
                            <button onClick={scrollToApplications}>Оставить заявку</button>
                        </div>
                    </div>}
            </header>
        </div>
    );
};

export default Toolbar;