import React from 'react';

const PartnersItem = ({image}) => {
    return (
        <div className="landing__partners-item">
            <img src={image} alt="logo"/>
        </div>
    );
};

export default PartnersItem;