import React from 'react';
import logo from '../../../assets/logo_icon.png';

const BloggerCard = ({data}) => {
    const socials = data && data.social_network;
    return (
        <>
            {data && data.blogger &&
            <div className="landing__our-bloggers-card">
                <img className="landing__our-bloggers-card-avatar"
                     src={data.blogger.image} alt="#"/>
                <h6>{data.blogger.surname} {data.blogger.name}</h6>
                <p className="landing__our-bloggers-card-direction">{data.blogger.country_name}</p>
                <div className="landing__our-bloggers-card-socials">
                    {socials && socials[0] &&
                    <div className="landing__our-bloggers-card-social">
                        <img
                            src={socials[0].image || logo}
                            alt="#"/>
                        <p className="landing__our-bloggers-card-social-count-subs">
                            {socials[0].subscriber || ""}
                        </p>
                        <p className="landing__our-bloggers-card-social-subscriptions">
                            Подписчиков
                        </p>
                    </div>
                    }
                    {socials && socials[1] &&
                    <div className="landing__our-bloggers-card-social">
                        <img
                            src={socials[1].image || logo}
                            alt="#"/>
                        <p className="landing__our-bloggers-card-social-count-subs">
                            {socials[1].subscriber || ""}
                        </p>
                        <p className="landing__our-bloggers-card-social-subscriptions">
                            Подписчиков
                        </p>
                    </div>
                    }
                </div>
            </div>
            }
        </>
    );
};

export default BloggerCard;