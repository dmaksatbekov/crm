import React, {useEffect, useState} from 'react';

import './index.scss';
import FilterButton from "../../components/kit/FilterButton/FilterButton";
import StarIcon from "../../components/kit/Icons/StarIcon";
import TabIcon from "../../components/kit/Icons/TabIcon";
import SearchIcon from "../../components/kit/Icons/SearchIcon";
import GridIcon from "../../components/kit/Icons/GridIcon";
import ListIcon from "../../components/kit/Icons/ListIcon";
import BloggerGridItem from "./BloggerGridItem";
import {connect} from "react-redux";
import {
    fetchBloggerFilterOptions,
    fetchBloggers,
    fetchClearBloggerStore,
    fetchRemoveBlogger
} from "../../store/bloggers/bloggersActions";
import BloggerItemList from "./BloggerItemList";
import CommonAlert from "../../components/CommonAlert/CommonAlert";
import CommonSpinner from "../../components/kit/CommonSpinner/CommonSpinner";
import BloggerModal from "../../components/AdminModal/Modals/BloggerModal/BloggerModal";
import CommonPagination from "../../components/CommonPagination/CommonPagination";
import BloggerInfoModal from "../../components/AdminModal/Modals/BloggerModal/InfoModal/BloggerInfoModal";
import GlobeIcon from "../../components/kit/Icons/GlobeIcon";
import FlagIcon from "../../components/kit/Icons/FlagIcon";
import './index.scss';
import {useDebounce} from "../../global/helpers";

const RATING_OPTIONS = [{label: "ER-", value: "ER-", id: "er"}, {label: "ER+", value: "ER+", id: "-er"}];

const Bloggers = (
    {
        fetchBloggers,
        bloggersList,
        requested,
        fetchBloggerFilterOptions,
        filterOptions,
        bloggersError,
        fetchRemoveBlogger,
        fetchClearBloggerStore
    }
) => {
    const [isGrid, toggleShowing] = useState(true);
    const [bloggerItem, setItem] = useState(null)

    const [state, setState] = useState({
        search: "",
        social: "",
        price: "",
        country: "",
        rating: "",
        tags: "",
        page: 1
    })

    const [bloggerId, setBloggerId] = useState(null)

    useEffect(function () {
        fetchBloggerFilterOptions()
    }, [])

    useEffect(function () {
        fetchBloggers(state)
    }, [
        state.rating,
        state.price,
        state.social,
        state.country,
        state.tags,
        state.rating,
        state.page
    ])


    function handlePagination(e, page) {
        setState(prevState => {
            return {...prevState, page: page}
        })
    }

    function onSelectFilter(data) {
        setState(prevState => {
            return {...prevState, [data.type]: data.id, page: 1}
        })
    }

    function searchHandler(e) {
        e.persist()
        setState(prevState => {
            return {...prevState, search: e.target.value}
        })
    }

    const debouncedSearchTerm = useDebounce(state.search, 500);

    useEffect(function () {
        const copy = {...state};
        copy.search = debouncedSearchTerm;
        fetchBloggers(copy)
    }, [debouncedSearchTerm])

    function onEditBlogger() {
        setBloggerId(null);
        setItem(null)
        fetchClearBloggerStore()
        fetchBloggers(state)
    }

    async function onRemoveBlogger(id) {
        await fetchRemoveBlogger(id);
        await fetchBloggers(state)
    }

    const filterPrice = [];
    const filterTags = [];
    const filterSocials = [];
    const filterCountries = [];
    if (filterOptions) {
        filterOptions.prices &&
        filterOptions.prices.map(price => {
            if (!filterPrice.some(it => it.name === price.name)) {
                filterPrice.push({...price, value: price.name, label: price.name})
            }
        })

        filterOptions.tags &&
        filterOptions.tags.map(tag => filterTags.push({...tag, value: tag.name, label: tag.name}))
        filterOptions.social_networks &&
        filterOptions.social_networks.map(social =>
            filterSocials.push({...social, value: social.name, label: social.name}))
        filterOptions.countries &&
        filterOptions.countries.map(country =>
            filterCountries.push({...country, value: country.name, label: country.name}))
    }

    return (
        <div className="blogger-root">
            <div className="blogger-root__filters">
                {filterOptions &&
                <>
                    {filterOptions.tags &&
                    <FilterButton
                        key="Теги"
                        icon={<TabIcon/>}
                        label="Теги"
                        type="tags"
                        onClick={onSelectFilter}
                        options={filterTags}/>
                    }

                    <FilterButton
                        icon={<StarIcon color="#7400FF" width={20} height={20}/>}
                        label="ER"
                        type="rating"
                        key="rating"
                        onClick={onSelectFilter}
                        options={RATING_OPTIONS}/>

                    {filterOptions.social_networks &&
                    <FilterButton
                        icon={<GlobeIcon/>}
                        label="Соц. сети"
                        type="social"
                        key="social"
                        onClick={onSelectFilter}
                        options={filterSocials}/>
                    }
                    {filterOptions.prices &&
                    <FilterButton
                        icon={<TabIcon width={18} height={18} color="#7400FF"/>}
                        label="Цена"
                        onClick={onSelectFilter}
                        type="price"
                        key="price"
                        options={filterPrice}/>
                    }
                    {filterOptions.countries &&
                    <FilterButton
                        icon={<FlagIcon/>}
                        label="Страна"
                        type="country"
                        key="country"
                        onClick={onSelectFilter}
                        options={filterCountries}/>
                    }
                </>
                }
                <div className="header-filter__search">
                    <SearchIcon/>
                    <input
                        type="search"
                        value={state.search}
                        onChange={searchHandler}
                        placeholder="Поиск..."
                    />
                </div>

                <div className="blogger-root__filters-switch">
                    <div className={`switch-item ${isGrid && "switch-item--active"}`}
                         onClick={() => toggleShowing(true)}>
                        <GridIcon width={20} height={20} color={isGrid ? "#fff" : "#6A7083"}/>
                    </div>
                    <div className={`switch-item ${!isGrid && "switch-item--active"}`}
                         onClick={() => toggleShowing(false)}>
                        <ListIcon width={20} height={20} color={isGrid ? "#6A7083" : "#fff"}/>
                    </div>
                </div>
            </div>

            {requested && <CommonSpinner isLoading={requested} centered/>}

            <div className={`blogger-root__items ${isGrid ? "blogger-root__items--flex" : ""}`}>
                {!requested && bloggersList && bloggersList.results.map((item, ndx) => (
                    <React.Fragment key={ndx}>
                        {
                            isGrid ?
                                <BloggerGridItem
                                    onEdit={setBloggerId}
                                    data={item}
                                    key={ndx}
                                    onSelect={() => setItem(item)}
                                    onRemove={onRemoveBlogger}
                                />
                                :
                                <BloggerItemList
                                    onEdit={setBloggerId}
                                    data={item}
                                    key={item.id}
                                    onSelect={() => setItem(item)}
                                    onRemove={onRemoveBlogger}
                                />
                        }
                    </React.Fragment>
                ))}
            </div>

            {!requested && bloggersError && <CommonAlert
                status={bloggersError.status}
                message={bloggersError.statusText}
            />}

            {!requested && bloggersList &&
            bloggersList.page_count &&
            bloggersList.page_count > 1 &&
            <CommonPagination
                count={bloggersList.page_count}
                currentPage={state.page}
                onPageChange={handlePagination}/>
            }

            {bloggerId &&
            <BloggerModal
                onClose={onEditBlogger}
                bloggerId={bloggerId}
                type="edit"/>
            }

            {!!bloggerItem &&
            <BloggerInfoModal data={bloggerItem} onClose={() => setItem(null)}/>}
        </div>
    );
};

const mapStateToProps = state => ({
    requested: state.bloggers.requested,
    bloggersList: state.bloggers.bloggersList,
    filterOptions: state.bloggers.filterOptions,
    bloggersError: state.bloggers.bloggersError
});

const mapDispatchToProps = dispatch => ({
    fetchBloggers: (filter) => dispatch(fetchBloggers(filter)),
    fetchBloggerFilterOptions: () => dispatch(fetchBloggerFilterOptions()),
    fetchRemoveBlogger: id => dispatch(fetchRemoveBlogger(id)),
    fetchClearBloggerStore: () => dispatch(fetchClearBloggerStore())
})

export default connect(mapStateToProps, mapDispatchToProps)(Bloggers);