import React, {useState} from 'react';
import ava from '../../assets/logo_icon.png';
import {CURRENT_DATE, EXCLUSIVE_STATUS_NAME, GENDER_TYPES} from "../../global/constants";
import {isEmptyArray} from "formik";
import defaultIcon from '../../assets/clipboard.png'
import MoreIcon from "../../components/kit/Icons/MoreIcon";
import IconButton from "@material-ui/core/IconButton";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import EditIcon from "../../components/kit/Icons/EditIcon";
import TrashIcon from "../../components/kit/Icons/TrashIcon";

import './index.scss';
import moment from "moment";

const BloggerGridItem = ({data, onEdit, onRemove, onSelect}) => {

    const birthDate = moment(data.birthday_date, "DD-MM-YYYY").toDate()

    const bloggerAge = moment(CURRENT_DATE).diff(birthDate,'years')

    const [anchorEl, setAnchorEl] = useState(null);
    const open = Boolean(anchorEl);

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };
    function edit() {
        onEdit(data.id);
        setAnchorEl(null);
    }

    const handleClose = () => {
        setAnchorEl(null);
    }

    const remove = () => {
        setAnchorEl(null);
        onRemove(data.id)
    }


    return (
        <div className="grid-item">
            <div className="grid-item__header">
                <div className="grid-item__header-img">
                    <img src={data.image || ava} alt="#"/>
                    <span className="rating">{data.er}</span>
                </div>
                <div className="grid-item__header-names">
                    <p className={`status-name 
                    ${data.status_name === EXCLUSIVE_STATUS_NAME
                        ? "status-name--exclusive"
                        : ""}`}>
                        <img src={data.status_image} alt="" className="status-image"/>
                        {data.status_name}
                        {!data.telegram_status && (
                            <span className="list-item__header-names--status-lg">
                                не активен
                            </span>
                        )}
                    </p>
                    <p className="name">
                        <span>{data.middle_name && ` ${data.middle_name.charAt(0)}.`}</span>
                        <span>{data.name}</span>&nbsp;
                        <span>{data.surname},</span>
                        &nbsp;{bloggerAge}
                    </p>
                    <p className="gender-country">
                        {data.gender && GENDER_TYPES[data.gender]},
                        <span> {data.country_name}</span>
                    </p>
                </div>
                <div className="grid-item__header-options">
                    <IconButton
                        aria-label="more"
                        aria-controls="long-menu"
                        aria-haspopup="true"
                        className="grid-option-button"
                        onClick={handleClick}
                    >
                        <MoreIcon color="#6A7083"/>
                    </IconButton>
                    <Menu
                        id="long-menu"
                        anchorEl={anchorEl}
                        keepMounted
                        open={open}
                        onClose={handleClose}
                        PaperProps={{
                            style: {
                                width: '20ch',
                            },
                        }}
                    >
                        <MenuItem className="header-options-list" onClick={edit}>
                            <EditIcon className="icon"/> <span className="text">Редактировать</span>
                        </MenuItem>
                        <MenuItem className="header-options-list" onClick={remove}>
                            <TrashIcon className="icon" color="#FA445C"/>
                            <span className="text">Удалить</span>
                        </MenuItem>
                    </Menu>
                </div>
            </div>
            <div className="grid-item__body">
                <div className="grid-item__socials">
                    {data.social_network && !isEmptyArray(data.social_network) &&
                    data.social_network.map((social, ndx) => {
                        if (ndx < 5) {
                            return (
                                <div key={social.id} className="grid-item__social">
                                    <img className="icon" src={social.social_network_image} alt=""/>
                                    <a rel="noopener noreferrer" target="_blank" href={social.soc_network_url}
                                       className="link">
                                        {social.link}
                                    </a>
                                    <span className="subscribers-count">
                                {social.subscribers}
                            </span>
                                </div>
                            )
                        } else return null
                    })}
                </div>
                <div className="grid-item__services">
                    {data.service && !isEmptyArray(data.service) &&
                    data.service.map((service, ndx) => {
                        if (ndx < 4) {
                            return (
                                <div key={service.id} className="grid-item__service">
                                    <img className="icon"
                                         src={service.advertisement_type_image || defaultIcon} alt=""/>
                                    <span className="link">{service.advertisement_type_name}</span>
                                    <span className="price">
                                От {service.price}c
                            </span>
                                </div>
                            )
                        } else return null
                    })
                    }
                </div>
                {data.subject_list && !isEmptyArray(data.subject_list) &&
                <div className="grid-item__subjects">
                    {data.subject_list.map((item, ndx) => {
                        if (ndx < 7) {
                            return (
                                <div key={item.id} className="subject" style={{backgroundColor: item.back_color}}>
                            <span className="subject-name" style={{color: item.color}}>
                                {item.subject_name}
                            </span>
                                </div>
                            )
                        } else return null
                    })}
                </div>}
            </div>
            <button onClick={onSelect} className="grid-item__btn">
                Подробнее
            </button>
        </div>
    );
};

export default React.memo(BloggerGridItem);