import React, {useEffect, useState} from 'react';
import {
    downloadFile,
    fetchAdvertise,
    fetchEditAdvertisement, fetchSendAdvertisementToBloggers,
} from "../../store/advertisement/advertisementActions";
import {connect} from "react-redux";
import {history} from "../../store/configureStore";
import {ROUTE_ADVERTISEMENTS} from "../../global/Routes";
import AboutProject from "./AboutProject";
import BloggersInfo from "./BloggersInfo";

const AdvertiseCompany = (
    {
        fetchAdvertise,
        requested,
        advertise,
        fetchSendAdvertisementToBloggers,
        downloadFile,
        fetchEditAdvertisement,
        ...props
    }
) => {
    const id = props.match.params.advertisementId;
    const [activeTab, setTab] = useState(0);


    useEffect(function () {
        if (id) {
            fetchAdvertise(id)
        } else {
            history.push(ROUTE_ADVERTISEMENTS)
        }

    }, [])

    function onSendMessages() {
        fetchSendAdvertisementToBloggers({campaign: id})
    }

    return (
        <>
            <div className="advertisement__company-tabs">
                <button
                    onClick={() => setTab(0)}
                    className={`tab-name ${activeTab === 0 ? "tab-name--active" : ""}`}>
                    О проекте
                </button>
                <button
                    onClick={() => setTab(1)}
                    className={`tab-name ${activeTab === 1 ? "tab-name--active" : ""}`}>
                    Список блогеров
                </button>
            </div>

            {advertise &&
            <div className="advertisement__company">
                {activeTab === 0 &&
                <AboutProject
                    data={advertise}
                    downloadFile={downloadFile}
                    onComplete={fetchEditAdvertisement}
                    sendTz={onSendMessages}/>}
                {activeTab === 1 &&
                <BloggersInfo data={advertise}/>}
            </div>
            }
        </>
    );
};
const mapStateToProps = state => ({
    advertise: state.advertisement.advertise,
    requested: state.advertisement.requested
});

const mapDispatchToProps = dispatch => ({
    fetchAdvertise: id => dispatch(fetchAdvertise(id)),
    fetchEditAdvertisement: (data, id, creatable) => dispatch(fetchEditAdvertisement(data, id, creatable)),
    fetchSendAdvertisementToBloggers: data => dispatch(fetchSendAdvertisementToBloggers(data)),
    downloadFile: campaign => dispatch(downloadFile(campaign))
});
export default connect(mapStateToProps, mapDispatchToProps)(AdvertiseCompany);