import React, {useEffect, useState} from 'react';
import './index.scss';
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import Grid from "@material-ui/core/Grid";

import avatar from '../../assets/human_avatar.png';
import CardInfo from "./CardInfo/CardInfo";
import {connect} from "react-redux";
import {fetchApplicationById} from "../../store/applications/applicationsActions";
import {NavLink} from "react-router-dom";
import {ROUTE_ADVERTISEMENT_BLOGGERS} from "../../global/Routes";
import {
    fetchAdvertise,
    fetchEditAdvertisement, fetchPersonalTz,
    fetchRemoveAdvertisementBlogger
} from "../../store/advertisement/advertisementActions";
import CommonSpinner from "../../components/kit/CommonSpinner/CommonSpinner";
import {isEmptyArray} from "formik";
import BloggerAdvertisementModal from "../../components/AdminModal/Modals/Advertisement/BloggerAdvertisementModal";
import BloggerItem from "./BloggerIem";

const Advertisement = (
    {
        advertise,
        fetchAdvertise,
        fetchRemoveAdvertisementBlogger,
        fetchEditAdvertisement,
        fetchPersonalTz,
        ...props
    }) => {

    const [selectedItem, setItem] = useState(null)


    const id = props.match.params.advertisementId;
    useEffect(function () {
        fetchAdvertise(id);
    }, [id])

    function sendPersonalMailing(blogger) {
        fetchPersonalTz(id, blogger)
    }

    return (
        <>
            <div className="advertisement">
                <Grid container spacing={3}>
                    <Grid item xs={12} lg={9}>
                        <Table className="advertisement__table">
                            <TableHead>
                                <TableRow>
                                    <TableCell align="left">ФИО блогера</TableCell>
                                    <TableCell align="left">Тип рекламы</TableCell>
                                    <TableCell align="left">Цена рекламы</TableCell>
                                    <TableCell align="left">Общая коммисия</TableCell>
                                    {/*<TableCell align="left">Агентский процент</TableCell>*/}
                                    <TableCell align="left">Итого</TableCell>
                                    <TableCell align="left">Статус Тз</TableCell>
                                    <TableCell align="center">Действия</TableCell>
                                </TableRow>
                            </TableHead>
                            {!props.requested &&
                            advertise && !isEmptyArray(advertise.blogger) &&
                            <TableBody>
                                {advertise.blogger.map(item => (
                                    <BloggerItem
                                        data={item}
                                        key={item.id}
                                        onRemove={fetchRemoveAdvertisementBlogger}
                                        percent={advertise.agent_comission_percent}
                                        // sendPersonalMailing={sendPersonalMailing}
                                        onSelect={setItem}/>
                                ))}
                            </TableBody>
                            }
                        </Table>

                        {props.requested && <CommonSpinner isLoading={props.requested} centered/>}

                        {!props.requested &&
                        advertise &&
                        advertise.blogger &&
                        isEmptyArray(advertise.blogger) &&
                        <div className="advertisement__create-block">
                            <img src={avatar} alt="#"/>
                            <h6 className="advertisement__create-title">
                                Список пуст
                            </h6>
                            <p className="advertisement__create-sub-title">
                                Добавьте блогера, что бы создать компанию
                            </p>
                            <NavLink className="nav-link" to={ROUTE_ADVERTISEMENT_BLOGGERS + `/${id}`}>
                                Выбрать блогера
                            </NavLink>
                        </div>}


                    </Grid>
                    <Grid item xs={12} lg={3} className="advertisement__card-info">
                        {advertise &&
                        <CardInfo
                            data={advertise}
                            percent={advertise.agent_comission_percent}
                            onCreate={fetchEditAdvertisement}
                            />
                        }
                    </Grid>
                </Grid>

            </div>
            {selectedItem &&
            <BloggerAdvertisementModal
                onClose={() => setItem(null)}
                type={selectedItem.type}
                initData={selectedItem}
            />
            }
        </>
    );
};

const mapStateToProps = state => ({
    application: state.applicationsInfo.application,
    advertise: state.advertisement.advertise,
    requested: state.advertisement.requested
});

const mapDispatchToProps = dispatch => ({
    fetchApplicationById: id => dispatch(fetchApplicationById(id)),
    fetchAdvertise: id => dispatch(fetchAdvertise(id)),
    fetchEditAdvertisement: (data, id, creatable) => dispatch(fetchEditAdvertisement(data, id, creatable)),
    fetchRemoveAdvertisementBlogger: (bloggerId, advertiseId) =>
        dispatch(fetchRemoveAdvertisementBlogger(bloggerId, advertiseId)),
    fetchPersonalTz: (campaign, blogger) => dispatch(fetchPersonalTz(campaign, blogger))
});

export default connect(mapStateToProps, mapDispatchToProps)(Advertisement);