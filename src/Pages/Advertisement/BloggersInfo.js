import React, {useEffect, useState} from 'react';
import './index.scss';
import Grid from "@material-ui/core/Grid";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import {isEmptyArray} from "formik";
import TableBody from "@material-ui/core/TableBody";
import CardInfo from "./CardInfo/CardInfo";
import {
    fetchAdvertise,
    fetchEditAdvertisement, fetchEditBloggerStatus, fetchPersonalTz,
    fetchRemoveAdvertisementBlogger
} from "../../store/advertisement/advertisementActions";
import {connect} from "react-redux";
import BloggerAdvertisementModal from "../../components/AdminModal/Modals/Advertisement/BloggerAdvertisementModal";
import {AGREEMENT_STATUS, REJECTED_STATUS} from "../../global/constants";
import {COMPLETE_STATUS} from "./AboutProject";
import BloggerItem from "./BloggerIem";
import avatar from "../../assets/human_avatar.png";
import {NavLink} from "react-router-dom";
import {ROUTE_ADVERTISEMENT_BLOGGERS} from "../../global/Routes";
import NoResultIcon from "../../components/kit/Icons/NoResultIcon";
import CommonSpinner from "../../components/kit/CommonSpinner/CommonSpinner";


const TAB_NAMES = [
    {
        name: "Все",
        status: ""
    },
    {
        name: "Приняли",
        status: AGREEMENT_STATUS
    },
    {
        name: "Отказались",
        status: REJECTED_STATUS
    }
]

const BloggersInfo = (
    {
        data,
        fetchRemoveAdvertisementBlogger,
        fetchEditAdvertisement,
        fetchAdvertise,
        fetchPersonalTz,
        fetchEditBloggerStatus,
        requested
    }
) => {

    const [activeTab, setTab] = useState(TAB_NAMES[0]);
    const [selectedItem, setItem] = useState(null);

    function onChangeTab(tab) {
        setTab(tab)
        fetchAdvertise(data.id, tab.status)
    }

    function sendPersonalMailing(blogger) {
        fetchPersonalTz(data.id, blogger)
    }

    function onChangeBloggerStatus(item, bloggerId) {
        fetchEditBloggerStatus(item, bloggerId, data.id)
    }

    return (
        <>
            <div className="advertisement__bloggers-info">
                <div className="tabs">
                    {TAB_NAMES.map((tab, ndx) => (
                        <button
                            key={ndx}
                            onClick={() => onChangeTab(tab)}
                            className={`tab
                         ${activeTab.status === tab.status ? "tab--active" : ""}`}>
                            {tab.name}
                        </button>
                    ))}

                </div>
                <div className="advertisement">
                    <Grid container spacing={2}>
                        <Grid item sm={9}>
                            <Table className="advertisement__table">
                                <TableHead>
                                    <TableRow>
                                        <TableCell align="left">ФИО блогера</TableCell>
                                        <TableCell align="left">Тип рекламы</TableCell>
                                        <TableCell align="left">Цена рекламы</TableCell>
                                        <TableCell align="left">Общая комиссия</TableCell>
                                        <TableCell align="left">Итого</TableCell>
                                        <TableCell align="left">Статус ТЗ</TableCell>
                                        <TableCell align="left">Статус</TableCell>
                                        <TableCell align="left">Действия</TableCell>
                                    </TableRow>
                                </TableHead>
                                {!requested && data &&
                                <TableBody>
                                    {data.blogger.map(item => (
                                        <BloggerItem
                                            data={item}
                                            key={item.id}
                                            statusTz
                                            onRemove={fetchRemoveAdvertisementBlogger}
                                            percent={data.agent_comission_percent}
                                            onSelect={setItem}
                                            sendPersonalMailing={sendPersonalMailing}
                                            disabled={data.status === COMPLETE_STATUS}
                                            editStatus={onChangeBloggerStatus}
                                        />
                                    ))}
                                </TableBody>
                                }
                            </Table>

                            {requested && <CommonSpinner isLoading={requested} centered/>}

                            {!requested && data && isEmptyArray(data.blogger) && activeTab.status === "" &&
                            <div className="advertisement__create-block">
                                <img src={avatar} alt="#"/>
                                <h6 className="advertisement__create-title">
                                    Список пуст
                                </h6>
                                <p className="advertisement__create-sub-title">
                                    Добавьте блогера, что бы создать компанию
                                </p>
                                <NavLink className="nav-link" to={ROUTE_ADVERTISEMENT_BLOGGERS + `/${data.id}`}>
                                    Выбрать блогера
                                </NavLink>
                            </div>
                            }

                            {!requested && data && isEmptyArray(data.blogger) &&
                            (activeTab.status === AGREEMENT_STATUS || activeTab.status === REJECTED_STATUS) &&
                            <div className="no-result">
                                <NoResultIcon width={100} height={140}/>
                                <p className="no-result__text">
                                    Результатов не найдено
                                </p>
                            </div>
                            }

                        </Grid>
                        <Grid item sm={3} className="advertisement__card-info">
                            {!requested && data &&
                            <CardInfo
                                data={data}
                                percent={data.agent_comission_percent}
                                onCreate={fetchEditAdvertisement}
                            />}
                        </Grid>
                    </Grid>
                </div>
            </div>
            {selectedItem &&
            <BloggerAdvertisementModal
                onClose={() => setItem(null)}
                type={selectedItem.type}
                initData={selectedItem}
            />
            }
        </>
    );
};

const mapStateToProps = state => ({
    requested: state.advertisement.requested
});

const mapDispatchToProps = dispatch => ({
    fetchAdvertise: (id, status) => dispatch(fetchAdvertise(id, status)),
    fetchEditAdvertisement: (data, id, creatable) => dispatch(fetchEditAdvertisement(data, id, creatable)),
    fetchRemoveAdvertisementBlogger: (bloggerId, advertiseId) =>
        dispatch(fetchRemoveAdvertisementBlogger(bloggerId, advertiseId)),
    fetchPersonalTz: (campaign, blogger) => dispatch(fetchPersonalTz(campaign, blogger)),
    fetchEditBloggerStatus: (data, bloggerId, advertiseId) =>
        dispatch(fetchEditBloggerStatus(data, bloggerId, advertiseId))
});


export default connect(mapStateToProps, mapDispatchToProps)(BloggersInfo);