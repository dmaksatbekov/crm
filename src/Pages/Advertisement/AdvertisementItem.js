import React from 'react';
import WalletIcon from "../../components/kit/Icons/WalletIcon";
import CalendarIcon from "../../components/kit/Icons/CalendarIcon";
import {NavLink} from "react-router-dom";
import FileIcon from "../../components/kit/Icons/FileIcon";
import LikeIcon from "../../components/kit/Icons/LikeIcon";
import avatar from '../../assets/avatar.png';
import Button from "@material-ui/core/Button";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import Fade from "@material-ui/core/Fade";
import {numberWithDot} from "../../global/helpers";
import Tooltip from "@material-ui/core/Tooltip";
import {isEmptyArray} from "formik";
import IconButton from "@material-ui/core/IconButton";
import MoreIcon from "../../components/kit/Icons/MoreIcon";
import EditIcon from "../../components/kit/Icons/EditIcon";
import TrashIcon from "../../components/kit/Icons/TrashIcon";
import {history} from "../../store/configureStore";
import {ROUTE_ADVERTISE_PAGE, ROUTE_ADVERTISEMENT} from "../../global/Routes";
import UserIcon from "../../components/kit/Icons/UserIcon";
import {AGREEMENT_STATUS, REJECTED_STATUS} from "../../global/constants";
import {COMPLETE_STATUS, IN_PROGRESS_STATUS} from "./AboutProject";

const MAX_BLOGGER_LENGTH = 3;

const AdvertisementItem = ({data, onRemove}) => {
    const [anchorEl, setAnchorEl] = React.useState(null);
    const [anchorDotEl, setAnchorDotEl] = React.useState(null);

    const isOpen = Boolean(anchorDotEl)

    const open = Boolean(anchorEl);

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const handleDotClick = (event) => {
        setAnchorDotEl(event.currentTarget);
    };

    const handleDotClose = () => {
        setAnchorDotEl(null);
    };

    const onEdit = () => {
        history.push(ROUTE_ADVERTISE_PAGE + `/${data.id}`);
        setAnchorDotEl(null);
    }

    const remove = () => {
        setAnchorDotEl(null);
        onRemove(data.id)
    }


    const names = data.customer && data.customer.split(" ");

    const bloggersAccepted = data.blogger ? data.blogger.filter(it => it.answer === AGREEMENT_STATUS) : [];
    const bloggersRejected = data.blogger ? data.blogger.filter(it => it.answer === REJECTED_STATUS) : [];

    return (
        <div className="advertisement__item">
            <div className="advertisement__item__head">
                <div className="date-id">
                    <p className="id">ID: #{data.id}</p>
                    <p className="date">Дата создания: <span>{data.date}</span></p>
                </div>
                <div className="info_icon">
                    <IconButton
                        aria-label="more"
                        aria-controls="long-menu"
                        aria-haspopup="true"
                        className="dot-rotate-icon"
                        onClick={handleDotClick}
                    >
                        <MoreIcon color="#6A7083" className="icon-rotate"/>
                    </IconButton>
                    <Menu
                        anchorEl={anchorDotEl}
                        keepMounted
                        open={isOpen}
                        onClose={handleDotClose}
                    >
                        {/*/!*<MenuItem className="header-options-list" onClick={onEdit}>*!/ // TODO not need (duplicated functionality)*/}
                        {/*    <EditIcon className="icon"/> <span className="text">Редактировать</span>*/}
                        {/*</MenuItem>*/}
                        <MenuItem className="header-options-list" onClick={remove}>
                            <TrashIcon className="icon" color="#FA445C"/>
                            <span className="text">Удалить</span>
                        </MenuItem>
                    </Menu>
                </div>
            </div>
            {data.company_name && data.company_name.length > 10
                ?
                <Tooltip title={data.company_name}>
                    <h3 className="title">
                        {data.company_name}
                    </h3>
                </Tooltip>
                :
                <h3 className="title">
                    {data.campaign_name}
                </h3>
            }
            <ul>
                <li>
                    <WalletIcon className="icon"/>
                    <span className="text-grey">
                        Общ. бюджет:
                    </span>
                    <span className="text-bold">{numberWithDot(data.budget)}c</span>
                </li>
                <li>
                    <CalendarIcon className="icon"/>
                    <span className="text-grey">
                        Дедлайн:
                    </span>
                    <span className={
                        data.status === COMPLETE_STATUS ?
                            "text text-completed" :
                            `text ${data.status === IN_PROGRESS_STATUS && !data.is_created && 'text-in-progress'}`}>
                        {data.status === COMPLETE_STATUS && "Завершено"}
                        {data.status === IN_PROGRESS_STATUS && data.is_created && data.deadline}
                        {data.status === IN_PROGRESS_STATUS && !data.is_created && "В процессе создания"}
                    </span>
                </li>
                <li>
                    <UserIcon className="icon"/>
                    <span className="text-grey">
                        Заказчик:
                    </span>
                    {names &&
                    <span className="text">
                        {names[0] && `${names[0]}`}
                        {names[1] && ` ${names[1].charAt(0)}`}
                        {names[2] && `. ${names[2].charAt(0)}`}
                    </span>}
                </li>
                <li>
                    <FileIcon className="icon"/>
                    <span className="text-blue">
                        ТЗ:
                    </span>
                    <NavLink
                        className="text-blue"
                        to={ROUTE_ADVERTISE_PAGE + `/${data.id}`}>
                        Смотреть
                    </NavLink>
                </li>
            </ul>
            <h6 className="bloggers-status">Статус участия:</h6>
            <ul>
                <li>
                    <LikeIcon className="icon" width={20} height={20} color="#24AF47"/>
                    <span className="status-text">Приняли: </span>
                    {!isEmptyArray(bloggersAccepted) ?
                        <div className="blogger-icons-root">
                            {bloggersAccepted.map((item, ndx) => (
                                <React.Fragment key={ndx}>
                                    {ndx < MAX_BLOGGER_LENGTH ?
                                        <Tooltip title={`${item.surname} ${item.name}`}>
                                <span className="blogger-icon">
                                    <img src={item.image || avatar} alt="#"/>
                                </span>
                                        </Tooltip>
                                        : null}
                                </React.Fragment>
                            ))
                            }
                            {bloggersAccepted.length > MAX_BLOGGER_LENGTH &&
                            <>
                                <Button
                                    aria-controls="fade-menu"
                                    aria-haspopup="true"
                                    onClick={handleClick}
                                    className="more-bloggers-btn"
                                >
                                    +{bloggersAccepted.length - (MAX_BLOGGER_LENGTH + 1)}
                                </Button>
                                <Menu
                                    id="fade-menu"
                                    anchorEl={anchorEl}
                                    keepMounted
                                    open={open}
                                    anchorOrigin={{
                                        vertical: "bottom",
                                        horizontal: "left"
                                    }}
                                    transformOrigin={{
                                        vertical: "bottom",
                                        horizontal: "left"
                                    }}
                                    getContentAnchorEl={null}
                                    onClose={handleClose}
                                    TransitionComponent={Fade}
                                    className="bloggers-popup"
                                >
                                    {bloggersAccepted.map((blogger, ndx) => {
                                        if (ndx > MAX_BLOGGER_LENGTH) {
                                            return (
                                                <MenuItem onClick={handleClose} key={ndx}>
                             <span className="blogger-icon">
                                 <img src={blogger.image || avatar} alt="#"/>
                             </span>
                                                    <span className="bloggers-popup__name">
                                      {blogger.surname && `${blogger.surname}`}
                                                        {blogger.name && ` ${blogger.name.charAt(0)}`}
                                                        {blogger.middle_name && `. ${blogger.middle_name.charAt(0)}`}
                                </span>
                                                </MenuItem>
                                            )
                                        } else return null
                                    })}

                                </Menu>
                            </>
                            }
                        </div> :
                        <span className="no-answer">Ответа нет</span>
                    }
                </li>

                <li>
                    <LikeIcon className="icon like-icon-rotate" width={20} height={20} color="#FA445C"/>
                    <span className="status-text">Отказались: </span>
                    {!isEmptyArray(bloggersRejected) ?
                        <div className="blogger-icons-root">
                            {bloggersRejected.map((item, ndx) => (
                                <React.Fragment key={ndx}>
                                    {ndx < MAX_BLOGGER_LENGTH ?
                                        <Tooltip title={`${item.surname} ${item.name}`}>
                                <span className="blogger-icon">
                                    <img src={item.image || avatar} alt="#"/>
                                </span>
                                        </Tooltip>
                                        : null}
                                </React.Fragment>
                            ))
                            }
                            {bloggersRejected.length > MAX_BLOGGER_LENGTH &&
                            <>
                                <Button
                                    aria-controls="fade-menu"
                                    aria-haspopup="true"
                                    onClick={handleClick}
                                    className="more-bloggers-btn"
                                >
                                    +{bloggersRejected.length - (MAX_BLOGGER_LENGTH + 1)}
                                </Button>
                                <Menu
                                    id="fade-menu"
                                    anchorEl={anchorEl}
                                    keepMounted
                                    open={open}
                                    anchorOrigin={{
                                        vertical: "bottom",
                                        horizontal: "left"
                                    }}
                                    transformOrigin={{
                                        vertical: "bottom",
                                        horizontal: "left"
                                    }}
                                    getContentAnchorEl={null}
                                    onClose={handleClose}
                                    TransitionComponent={Fade}
                                    className="bloggers-popup"
                                >
                                    {bloggersRejected.map((blogger, ndx) => {
                                        if (ndx > MAX_BLOGGER_LENGTH) {
                                            return (
                                                <MenuItem onClick={handleClose} key={ndx}>
                             <span className="blogger-icon">
                                 <img src={blogger.image || avatar} alt="#"/>
                             </span>
                                                    <span className="bloggers-popup__name">
                                      {blogger.surname && `${blogger.surname}`}
                                                        {blogger.name && ` ${blogger.name.charAt(0)}`}
                                                        {blogger.middle_name && `. ${blogger.middle_name.charAt(0)}`}
                                </span>
                                                </MenuItem>
                                            )
                                        } else return null
                                    })}

                                </Menu>
                            </>
                            }
                        </div> :
                        <span className="no-answer">Ответа нет</span>
                    }
                </li>
            </ul>
        </div>
    );
};

export default AdvertisementItem;