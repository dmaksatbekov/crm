import React, {useEffect, useState} from 'react';
import AdvertisementItem from "./AdvertisementItem";
import {fetchAdvertisements, fetchRemoveAdvertisement} from "../../store/advertisement/advertisementActions";
import {connect} from "react-redux";
import {isEmptyArray} from "formik";
import CommonSpinner from "../../components/kit/CommonSpinner/CommonSpinner";
import HeaderFilter from "../../components/HeaderFilter/HeaderFilter";
import {useDebounce} from "../../global/helpers";
import CommonPagination from "../../components/CommonPagination/CommonPagination";
import NoResultIcon from "../../components/kit/Icons/NoResultIcon";

const INITIAL_PAGE = 1;
const ACTIVE_STATUS = "P";
const COMPLETED_STATUS = "D";

const TAB_NAMES = [{name: "Активные", status: ACTIVE_STATUS}, {name: "Завершенные", status: COMPLETED_STATUS}];

const SORT_OPTIONS = [
    {label: "по дате", value: "-date"},
    {label: "по дедлайну", value: "deadline"},
    {label: "по бюджету", value: "budget"},
]

const Advertisements = (
    {
        advertisements,
        fetchAdvertisements,
        fetchRemoveAdvertisement,
        requested
    }
) => {


    const [selectedDate, setDate] = useState(null);
    const [order, setOrder] = useState(null)
    const [search, setSearch] = useState("");
    const [page, setPage] = useState(INITIAL_PAGE);

    const [activeTab, setTab] = useState(ACTIVE_STATUS);

    function clearFilters() {
        setSearch("");
        setDate(null);
        fetchAdvertisements(INITIAL_PAGE, activeTab, selectedDate, order)
    }

    function changeTab(status) {
        setTab(status)
        setPage(INITIAL_PAGE)
        fetchAdvertisements(INITIAL_PAGE, status, selectedDate, order, search)
    }

    const debouncedSearchTerm = useDebounce(search, 500);

    useEffect(function () {
        if (search) {
            fetchAdvertisements(INITIAL_PAGE, activeTab, undefined, undefined, debouncedSearchTerm)
        }
    }, [debouncedSearchTerm])

    useEffect(function () {
        fetchAdvertisements(INITIAL_PAGE, activeTab, selectedDate, order)
    }, [selectedDate, order])

    function pageHandler(page) {
        setPage(page)
        fetchAdvertisements(page, activeTab, selectedDate, order)
    }

    function onSelectSort(option) {
        setOrder(option)
        fetchAdvertisements(INITIAL_PAGE, activeTab, selectedDate, option)
    }

    async function onRemove(id) {
        await fetchRemoveAdvertisement(id)
        await fetchAdvertisements(page, activeTab, selectedDate, order)

    }

    return (
        <>
            <div className="application-company__navs">
                {TAB_NAMES.map((tab, ndx) => (
                    <button
                        key={ndx}
                        className={`application-company__nav
                             ${activeTab === tab.status && "application-company__nav--active"}`}
                        onClick={() => changeTab(tab.status)}>{tab.name}</button>
                ))}
            </div>
            <HeaderFilter
                selectedDate={selectedDate}
                dateHandler={date => setDate(date)}
                search={search}
                inputHandler={e => setSearch(e.target.value)}
                options={SORT_OPTIONS}
                sortActiveValue={order}
                sortHandler={onSelectSort}
                onClear={clearFilters}
            />
            <div className="advertisement__list-items">

                {requested && <CommonSpinner isLoading={requested} centered/>}

                {!requested && advertisements && advertisements.results &&
                !isEmptyArray(advertisements.results) &&
                advertisements.results.map(item => (
                    <AdvertisementItem data={item} key={item.id} onRemove={onRemove}/>
                ))}
            </div>

            {!requested && advertisements &&
            advertisements.results &&
            isEmptyArray(advertisements.results) &&
            <div className="no-result">
                <NoResultIcon width={100} height={140}/>
                <p className="no-result__text">Результатов не найдено</p>
            </div>
            }

            {!requested && advertisements &&
            advertisements.total_pages > 1 &&
            <CommonPagination
                count={advertisements.total_pages}
                currentPage={page}
                onPageChange={(e, page) => pageHandler(page)}/>
            }
        </>
    );
};

const mapStateToProps = state => ({
    advertisements: state.advertisement.advertisements,
    requested: state.advertisement.requested
});

const mapDispatchToProps = dispatch => ({
    fetchAdvertisements: (page, status, selectedDate, order, search) =>
        dispatch(fetchAdvertisements(page, status, selectedDate, order, search)),
    fetchRemoveAdvertisement: id => dispatch(fetchRemoveAdvertisement(id))
})

export default connect(mapStateToProps, mapDispatchToProps)(Advertisements);