import React, {useEffect} from 'react';
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";
import Table from "@material-ui/core/Table";
import {fetchAdvertisements} from "../../store/settings/settingsActions";
import {connect} from "react-redux";
import IconButton from "@material-ui/core/IconButton";
import MoreIcon from "../../components/kit/Icons/MoreIcon";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import EditIcon from "../../components/kit/Icons/EditIcon";
import TrashIcon from "../../components/kit/Icons/TrashIcon";

const AdvertisementType = ({fetchAdvertisements, advertisement_types}) => {

    useEffect(function () {
        fetchAdvertisements()
    }, [fetchAdvertisements]);

    const [anchorEl, setAnchorEl] = React.useState(null);
    const [anchorDotEl, setAnchorDotEl] = React.useState(null);

    const isOpen = Boolean(anchorDotEl)

    const open = Boolean(anchorEl);

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const handleDotClick = (event) => {
        setAnchorDotEl(event.currentTarget);
    };

    const handleDotClose = () => {
        setAnchorDotEl(null);
    };

    const onEdit = () => {
        setAnchorDotEl(null);
    }

    const remove = () => {
        setAnchorDotEl(null);
    }

    return (
        <Table className="application-blogger__table">
            <TableHead>
                <TableRow>
                    <TableCell align="left">ID</TableCell>
                    <TableCell align="left">Дата создания</TableCell>
                    <TableCell align="left">тип рекламы</TableCell>
                    <TableCell align="left">Действия</TableCell>
                </TableRow>
            </TableHead>
            <TableBody>
                {advertisement_types && advertisement_types.sort((a, b) => a.id - b.id).map(item => (
                    <TableRow>
                        <TableCell align="left">09-05-2020</TableCell>
                        <TableCell align="left">{item.id}</TableCell>
                        <TableCell align="left">{item.type_name}</TableCell>
                        <TableCell align="left">
                            <IconButton
                                aria-label="more"
                                aria-controls="long-menu"
                                aria-haspopup="true"
                                className="dot-rotate-icon"
                                onClick={handleDotClick}
                            >
                                <MoreIcon color="#6A7083" className="icon-rotate"/>
                            </IconButton>
                            <Menu
                                anchorEl={anchorDotEl}
                                keepMounted
                                open={isOpen}
                                onClose={handleDotClose}
                            >
                                <MenuItem className="header-options-list" onClick={onEdit}>
                                    <EditIcon className="icon"/> <span className="text">Редактировать</span>
                                </MenuItem>
                                <MenuItem className="header-options-list" onClick={remove}>
                                    <TrashIcon className="icon" color="#FA445C"/>
                                    <span className="text">Удалить</span>
                                </MenuItem>
                            </Menu>
                        </TableCell>
                    </TableRow>
                ))}
            </TableBody>
        </Table>
    );
};

const mapStateToProps = state => ({
    advertisement_types: state.settings.advertisement_types
})

const mapDispatchToProps = dispatch => ({
    fetchAdvertisements: data => dispatch(fetchAdvertisements(data))
})

export default connect(mapStateToProps, mapDispatchToProps)(AdvertisementType);