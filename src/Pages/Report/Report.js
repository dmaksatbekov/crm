import React, {useEffect, useState} from 'react';
import './index.scss';
import DatePickerForm from "../../components/kit/DatePickerForm/DatePickerForm";
import Button from "@material-ui/core/Button";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";
import ReportItem from "./ReportItem";
import {fetchExportReports, fetchReports} from "../../store/report/reportActions";
import {connect} from "react-redux";
import CommonSpinner from "../../components/kit/CommonSpinner/CommonSpinner";
import {isEmptyArray} from "../../global/helpers";
import NoResultIcon from "../../components/kit/Icons/NoResultIcon";

const Report = (
    {
        fetchReports,
        reports,
        requested,
        fetchExportReports
    }
) => {

    useEffect(function () {
        fetchReports()
    }, [])


    const [startDate, setStartDate] = useState(new Date())
    const [endDate, setEndDate] = useState(new Date())

    function onSubmit() {
        fetchReports(startDate, endDate)
    }

    function exportReport() {
        fetchExportReports(startDate, endDate)
    }

    return (
        <div className="report-root">
            <div className="report-root__filters">
                <h6>Выбрать дату</h6>
                <div className="report-root__datepicker">
                    <span>От,</span>
                    <DatePickerForm
                        placeholder="Выбрать дату"
                        selected={startDate}
                        onChange={value => setStartDate(value)}
                        className="header-filter__date-custom"
                        maxDate={new Date()}
                    />
                </div>
                <div className="report-root__datepicker">
                    <span>До,</span>
                    <DatePickerForm
                        placeholder="Выбрать дату"
                        selected={endDate}
                        onChange={value => setEndDate(value)}
                        className="header-filter__date-custom"
                        maxDate={new Date()}
                        minDate={startDate}
                    />
                </div>
                <Button disabled={requested}  className="report-root__show-btn" onClick={onSubmit}>Показать</Button>
                <Button
                disabled={requested || (reports && isEmptyArray(reports))}
                    className="report-root__download-btn"
                    onClick={exportReport}>
                    Скачать в Excel
                </Button>
            </div>

            {requested && <CommonSpinner isLoading={requested} centered/>}

            {!requested && reports &&
            <div className="report-root__table-root">
                <Table className="report-root__table">
                    <TableHead>
                        <TableRow>
                            <TableCell align="left">ID</TableCell>
                            <TableCell align="left">компания</TableCell>
                            <TableCell align="left">заказчик</TableCell>
                            <TableCell align="left">Дата начала</TableCell>
                            <TableCell align="left">Дата окончания</TableCell>
                            <TableCell align="left">общий бюджет</TableCell>
                            <TableCell align="left">Верхняя комиссия</TableCell>
                            <TableCell align="left">Нижняя комиссия</TableCell>
                            <TableCell align="left">Расходы на блогеров</TableCell>
                            {/*<TableCell align="left">Действия</TableCell>*/}
                        </TableRow>
                    </TableHead>

                    <TableBody>
                        {!requested && Array.isArray(reports) && !isEmptyArray(reports) && reports.map(item => (
                            <ReportItem data={item} key={item.id}/>
                        ))}
                    </TableBody>
                </Table>
            </div>}

            {!requested && reports && isEmptyArray(reports) &&
            <div className="no-result">
                <NoResultIcon width={100} height={140}/>
                <p className="no-result__text">
                   Результатов не найдено
                </p>
            </div>
            }
        </div>
    );
};

const mapStateToProps = state => ({
    reports: state.reports.reports,
    requested: state.reports.requested
})

const mapDispatchToProps = dispatch => ({
    fetchReports: (startPeriod, endPeriod) => dispatch(fetchReports(startPeriod, endPeriod)),
    fetchExportReports: (startPeriod, endPeriod) => dispatch(fetchExportReports(startPeriod, endPeriod)),
})

export default connect(mapStateToProps, mapDispatchToProps)(Report);