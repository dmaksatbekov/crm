import React from 'react'
//

const options = {
    elementType: ['line', 'area', 'bar', 'bubble'],
    primaryAxisType: ['linear', 'time', 'log', 'ordinal'],
    secondaryAxisType: ['linear', 'time', 'log', 'ordinal'],
    primaryAxisPosition: ['top', 'left', 'right', 'bottom'],
    secondaryAxisPosition: ['top', 'left', 'right', 'bottom'],
    secondaryAxisStack: [true, false],
    primaryAxisShow: [true, false],
    secondaryAxisShow: [true, false],
    grouping: ['single', 'series', 'primary', 'secondary'],
    tooltipAnchor: [
        'closest',
        'top',
        'bottom',
        'left',
        'right',
        'center',
        'gridTop',
        'gridBottom',
        'gridLeft',
        'gridRight',
        'gridCenter',
        'pointer'
    ],
    tooltipAlign: [
        'auto',
        'top',
        'bottom',
        'left',
        'right',
        'topLeft',
        'topRight',
        'bottomLeft',
        'bottomRight',
        'center'
    ],
    snapCursor: [true, false]
}

const optionKeys = Object.keys(options)

export default function useChartConfig({
                                           series,
                                           useR,
                                           show = [],
                                           count = 1,
                                           resizable = true,
                                           canRandomize = true,
                                           dataType = 'time',
                                           elementType = 'line',
                                           primaryAxisType = 'time',
                                           secondaryAxisType = 'linear',
                                           primaryAxisPosition = 'bottom',
                                           secondaryAxisPosition = 'left',
                                           primaryAxisStack = false,
                                           secondaryAxisStack = true,
                                           primaryAxisShow = true,
                                           secondaryAxisShow = true,
                                           tooltipAnchor = 'closest',
                                           tooltipAlign = 'auto',
                                           grouping = 'primary',
                                           snapCursor = true,
                                           datums,
                                           initCompanies,
                                           initBloggers
                                       }) {
    const [state, setState] = React.useState({
        count,
        resizable,
        canRandomize,
        dataType,
        elementType,
        primaryAxisType,
        secondaryAxisType,
        primaryAxisPosition,
        secondaryAxisPosition,
        primaryAxisStack,
        secondaryAxisStack,
        primaryAxisShow,
        secondaryAxisShow,
        tooltipAnchor,
        tooltipAlign,
        grouping,
        snapCursor,
        datums,
        initCompanies,
        initBloggers,
        data: makeDataFrom(dataType, series, useR, datums, initCompanies, initBloggers)
    })

    React.useEffect(() => {
        setState(old => ({
            ...old,
            data: makeDataFrom(dataType, series, useR, datums, initCompanies, initBloggers),

        }))
    }, [count, dataType, datums, series, useR])

    const Options = optionKeys
        .filter(option => show.indexOf(option) > -1)
        .map(option => (
            <div key={option}>
                {option}: &nbsp;
                <select
                    value={state[option]}
                    onChange={({target: {value}}) =>
                        setState(old => ({
                            ...old,
                            [option]:
                                typeof options[option][0] === 'boolean'
                                    ? value === 'true'
                                    : value
                        }))
                    }
                >
                    {options[option].map(d => (
                        <option value={d} key={d.toString()}>
                            {d.toString()}
                        </option>
                    ))}
                </select>
                <br/>
            </div>
        ))

    return {
        ...state,
        Options
    }
}

function makeDataFrom(dataType, series, useR, datums, initCompanies, initBloggers) {
    return [makeSeries("рекл. кампаний", dataType, useR, datums, initCompanies),
        makeSeries("блогеров", dataType, useR, datums, initBloggers)]
}

function makeSeries(label, dataType, useR, datums, initData) {
    const start = 0
    const rMin = 2
    const rMax = 20
    const nullChance = 0;
    const month = [];
    initData.forEach(it => month.push(it.month))
    return {
        label: `${label}`,
        datums: month.map((value, i) => {
            let x = start
            if (dataType === 'ordinal') {
                x = `${value}`
            }

            const distribution = 1.1
            const y =
                (initData[i] && initData[i].count && initData[i].count > nullChance)
                    ? initData[i].count
                    : null
            const r = !useR
                ? undefined
                : rMax -
                Math.floor(
                    Math.log(50 * (distribution ** rMax - rMin) + rMin) /
                    Math.log(distribution)
                )

            return {
                x,
                y,
                r
            }
        })
    }
}