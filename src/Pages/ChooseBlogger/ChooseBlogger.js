import React, {useEffect, useState} from 'react';
import FilterButton from "../../components/kit/FilterButton/FilterButton";
import TabIcon from "../../components/kit/Icons/TabIcon";
import StarIcon from "../../components/kit/Icons/StarIcon";
import GlobeIcon from "../../components/kit/Icons/GlobeIcon";
import FlagIcon from "../../components/kit/Icons/FlagIcon";
import SearchIcon from "../../components/kit/Icons/SearchIcon";
import {
    fetchBloggerFilterOptions,
    fetchBloggers,
} from "../../store/bloggers/bloggersActions";
import {connect} from "react-redux";
import CommonSpinner from "../../components/kit/CommonSpinner/CommonSpinner";
import BloggerItemList from "../Bloggers/BloggerItemList";
import CommonAlert from "../../components/CommonAlert/CommonAlert";
import CommonPagination from "../../components/CommonPagination/CommonPagination";
import {Scrollbars} from "react-custom-scrollbars";
import {fetchAdvertise, fetchEditAdvertisement} from "../../store/advertisement/advertisementActions";

const RATING_OPTIONS = [{label: "ER-", value: "ER-", id: "er"}, {label: "ER+", value: "ER+", id: "-er"}];

const ChooseBlogger = (
    {
        fetchBloggerFilterOptions,
        fetchBloggers,
        filterOptions,
        requested,

        bloggersList,
        bloggersError,

        application,
        fetchApplicationById,

        fetchAdvertise,
        advertise,
        fetchEditAdvertisement,

        ...props
    }) => {

    const {advertisementId} = props.match.params

    const [state, setState] = useState({
        search: "",
        social: "",
        price: "",
        country: "",
        rating: "",
        tags: "",
        page: 1,
        telegram_status: true
    })

    const [ids, setIds] = useState([]);

    useEffect(function () {
        fetchBloggerFilterOptions();
        fetchAdvertise(advertisementId)
    }, [])

    useEffect(function () {
        const bloggerIds = []
        advertise && advertise.blogger.map(it => bloggerIds.push(it.blogger))
        setIds(bloggerIds)
    }, [advertise])


    useEffect(function () {
        const filter = {...state};
        fetchBloggers(filter)
    }, [
        state.search,
        state.rating,
        state.price,
        state.social,
        state.country,
        state.tags,
        state.rating,
        state.page
    ])

    function onSelectFilter(data) {
        setState(prevState => {
            return {...prevState, [data.type]: data.id, page: 1}
        })
        setIds([]);
    }

    function searchHandler(e) {
        e.persist()
        setState(prevState => {
            return {...prevState, search: e.target.value}
        })
    }

    function handlePagination(e, page) {
        setState(prevState => {
            return {...prevState, page: page}
        })
    }

    function onSelect(id) {
        const idsList = [...ids];
        if (idsList.includes(id)) {
            const index = idsList.findIndex(it => it === id);
            idsList.splice(index, 1)
        } else {
            idsList.push(id)
        }
        setIds(idsList)
    }

    function onSubmit() {
        const data = {
            blogger: ids
        }
        const {advertisementId} = props.match.params;
        fetchEditAdvertisement(data, advertisementId)
    }

    const filterPrice = [];
    const filterTags = [];
    const filterSocials = [];
    const filterCountries = [];
    if (filterOptions) {
        filterOptions.prices &&
        filterOptions.prices.map(price => {
            if (!filterPrice.some(it => it.name === price.name)) {
                filterPrice.push({...price, value: price.name, label: price.name})
            }
        })

        filterOptions.tags &&
        filterOptions.tags.map(tag => filterTags.push({...tag, value: tag.name, label: tag.name}))
        filterOptions.social_networks &&
        filterOptions.social_networks.map(social =>
            filterSocials.push({...social, value: social.name, label: social.name}))
        filterOptions.countries &&
        filterOptions.countries.map(country =>
            filterCountries.push({...country, value: country.name, label: country.name}))
    }

    return (
        <div className="blogger-root">
            <div className="blogger-root__filters">
                {filterOptions &&
                <>
                    {filterOptions.tags &&
                    <FilterButton
                        key="Теги"
                        icon={<TabIcon/>}
                        label="Теги"
                        type="tags"
                        onClick={onSelectFilter}
                        options={filterTags}/>
                    }

                    <FilterButton
                        icon={<StarIcon color="#7400FF" width={20} height={20}/>}
                        label="ER рейтинг"
                        type="rating"
                        key="rating"
                        onClick={onSelectFilter}
                        options={RATING_OPTIONS}/>

                    {filterOptions.social_networks &&
                    <FilterButton
                        icon={<GlobeIcon/>}
                        label="Соц. сети"
                        type="social"
                        key="social"
                        onClick={onSelectFilter}
                        options={filterSocials}/>
                    }
                    {filterOptions.prices &&
                    <FilterButton
                        icon={<TabIcon width={18} height={18} color="#7400FF"/>}
                        label="Цена"
                        onClick={onSelectFilter}
                        type="price"
                        key="price"
                        options={filterPrice}/>
                    }
                    {filterOptions.countries &&
                    <FilterButton
                        icon={<FlagIcon/>}
                        label="Страна"
                        type="country"
                        key="country"
                        onClick={onSelectFilter}
                        options={filterCountries}/>
                    }
                </>
                }
                <div className="header-filter__search">
                    <SearchIcon/>
                    <input
                        type="search"
                        value={state.search}
                        onChange={searchHandler}
                        placeholder="Поиск блогера..."
                    />
                </div>

            </div>

            {requested && <CommonSpinner isLoading={requested} centered/>}

            <div className={`blogger-root__items`}>
                <Scrollbars style={{width: "100%", height: 700}}>
                    {!requested && bloggersList && [...bloggersList.results]
                        .map((item, ndx) => (
                            <BloggerItemList
                                onEdit={() => null}
                                data={item}
                                key={ndx}
                                onSelect={() => onSelect(item.id)}
                                idsList={ids}
                                collection
                                onRemove={() => null}
                            />
                        ))}
                </Scrollbars>
            </div>

            <div className="blogger-root__bloggers-count">
                <h6>Выбрано блогеров: <span>{ids.length}</span></h6>
                <button
                    disabled={!ids.length || requested}
                    className={`list-item__btn
                     ${!ids.length ? "list-item__btn--disabled" : ""}`}
                    onClick={onSubmit}>
                    Подтвердить
                </button>
            </div>

            {!requested && bloggersError && <CommonAlert
                status={bloggersError.status}
                message={bloggersError.statusText}
            />}

            {!requested && bloggersList &&
            bloggersList.page_count &&
            bloggersList.page_count > 1 &&
            <CommonPagination
                count={bloggersList.page_count}
                currentPage={state.page}
                onPageChange={handlePagination}/>
            }

        </div>
    );
};


const mapStateToProps = state => ({
    requested: state.bloggers.requested,
    bloggersList: state.bloggers.bloggersList,
    filterOptions: state.bloggers.filterOptions,
    bloggersError: state.bloggers.bloggersError,
    application: state.applicationsInfo.application,
    advertise: state.advertisement.advertise
});

const mapDispatchToProps = dispatch => ({
    fetchBloggers: (filter) => dispatch(fetchBloggers(filter)),
    fetchBloggerFilterOptions: () => dispatch(fetchBloggerFilterOptions()),
    fetchEditAdvertisement: (data, id) => dispatch(fetchEditAdvertisement(data, id)),
    fetchAdvertise: id => dispatch(fetchAdvertise(id))
})

export default connect(mapStateToProps, mapDispatchToProps)(ChooseBlogger);