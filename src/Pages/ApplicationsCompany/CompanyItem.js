import React from 'react';
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import StatusSplitButton from "../../components/StatusSplitButton/StatusSplitButton";
import ActionButton from "../../components/ActionButton/ActionButton";
import './index.scss';

const CompanyItem = (
    {
        data,
        onChangeStatus,
        onEdit,
        onSendToArchive,
        onRemove,
        onSendToApplication,
        onSelect
    }) => {
    return (
        <TableRow className={`application-company__table-row
        ${data.request_status === "N" ? "application-company__table-row--red" : ""}`}
                  onClick={() => onSelect(data)}>
            <TableCell align="left">{data.created_date}</TableCell>
            <TableCell align="left">{data.id}</TableCell>
            <TableCell align="left">{data.name}</TableCell>
            <TableCell align="left">{data.company_name}</TableCell>
            <TableCell align="left">{data.phone_number}</TableCell>
            <TableCell align="left">{data.deadline}</TableCell>
            <TableCell align="left">
                <StatusSplitButton
                    onChangeStatus={onChangeStatus}
                    initStatus={data.request_status}
                    disabled={data.request_status === "D"}
                    applicationId={data.id}
                />
            </TableCell>
            <TableCell align="left">
                <ActionButton
                    onEdit={() => onEdit(data)}
                    onSendToArchive={() => onSendToArchive(data.id)}
                    onRemove={() => onRemove(data.id, "company")}
                    sendToApplication={() => onSendToApplication(data.id, "company")}
                    editable={!!onEdit}
                />
            </TableCell>
        </TableRow>
    );
};

export default CompanyItem;