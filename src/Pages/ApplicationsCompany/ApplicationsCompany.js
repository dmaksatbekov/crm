import React, {useEffect, useState} from 'react';
import HeaderFilter from "../../components/HeaderFilter/HeaderFilter";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";
import {connect} from "react-redux";
import {
    fetchCompanyApplications,
    fetchEditApplicationArchive,
    fetchEditApplicationStatus
} from "../../store/applications/applicationsActions";
import CompanyItem from "./CompanyItem";
import {dateFormatter, isEmptyArray, useDebounce} from "../../global/helpers";
import NoResultIcon from "../../components/kit/Icons/NoResultIcon";
import CommonPagination from "../../components/CommonPagination/CommonPagination";
import ApplicationCompanyModal from "../../components/AdminModal/Modals/ApplicationCompanyModal";
import './index.scss';
import CommonSpinner from "../../components/kit/CommonSpinner/CommonSpinner";
import CommonAlert from "../../components/CommonAlert/CommonAlert";

export const TAB_NAMES = [
    {name: "Не обработанные", code: "N"},
    // {name: "В процессе", code: "P"},
    {name: "Обработанные", code: "D"}]

export const NUMBER_REQUEST_STAT = {
    "N": 1,
    "P": 2,
    "D": 3,
}

const ApplicationsCompany = (
    {
        companyApplicationsList,
        companyFailure,
        fetchCompanyApplications,
        fetchEditApplicationStatus,
        fetchEditApplicationArchive,

        notifications
    }
) => {
    const [activeTab, setTab] = useState(0);
    const [selectedDate, setDate] = useState(null);
    const [search, setSearch] = useState("");
    const [page, setPage] = useState(1);
    const [editingObject, setObject] = useState(null);
    const [loading, setLoading] = useState(true);
    const [selectedItem, setItem] = useState(null)

    const debouncedSearchTerm = useDebounce(search, 500);

    useEffect(function () {
        getApplications()
    }, [activeTab, page, debouncedSearchTerm, selectedDate])

    function changeTab(ndx) {
        setTab(ndx)
        setPage(1)
    }

    async function getApplications() {
        setLoading(true)
        await fetchCompanyApplications(
            page,
            TAB_NAMES[activeTab].code,
            debouncedSearchTerm,
            selectedDate ? dateFormatter(selectedDate) : null,
            false
        )
        setLoading(false)

    }

    async function onEditStatus(id, status) {
        await fetchEditApplicationStatus(id, status, "company");
        getApplications()
    }

    function clearFilters() {
        setSearch("");
        setDate(null);
    }

    function editApplication(item) {
        setObject(item)
    }

    async function sendToArchive(id) {
        await fetchEditApplicationArchive(id, "company");
        getApplications()
    }

    function onSelectApplication(data) {
        setItem(data)
    }

    return (
        <>
            <div className="application-company">
                <div className="application-company__navs">
                    {TAB_NAMES.map((tab, ndx) => (
                        <button
                            key={ndx}
                            className={`application-company__nav
                             ${activeTab === ndx && "application-company__nav--active"}
                             ${(notifications && notifications.hasCompany) && "application-company__nav--red"}`}
                            onClick={() => changeTab(ndx)}>{tab.name}</button>
                    ))}
                </div>
                <HeaderFilter
                    selectedDate={selectedDate}
                    dateHandler={date => setDate(date)}
                    search={search}
                    inputHandler={e => setSearch(e.target.value)}
                    onClear={clearFilters}
                />
                <div className="application-company__table-root">
                    <Table className="application-company__table">
                        <TableHead>
                            <TableRow>
                                <TableCell align="left">ДАТА</TableCell>
                                <TableCell align="left">ID</TableCell>
                                <TableCell align="left">ФИО</TableCell>
                                <TableCell align="left">Компания</TableCell>
                                <TableCell align="left">Телефон</TableCell>
                                <TableCell align="left">Дедлайн</TableCell>
                                <TableCell align="left">Статус</TableCell>
                                <TableCell align="left">Действия</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {!loading && companyApplicationsList &&
                            companyApplicationsList.results &&
                            !isEmptyArray(companyApplicationsList.results) &&
                            companyApplicationsList.results.sort((a, b) =>
                                    NUMBER_REQUEST_STAT[a.request_status] - NUMBER_REQUEST_STAT[b.request_status])
                                .map(item => (
                                    <CompanyItem
                                        data={item}
                                        key={item.id}
                                        onChangeStatus={onEditStatus}
                                        onEdit={editApplication}
                                        onSelect={onSelectApplication}
                                        onSendToArchive={sendToArchive}
                                    />
                                ))
                            }
                        </TableBody>
                    </Table>

                    {loading && <CommonSpinner isLoading={loading} centered/>}

                    {!loading && companyApplicationsList &&
                    companyApplicationsList.results &&
                    isEmptyArray(companyApplicationsList.results) &&
                    <div className="no-result">
                        <NoResultIcon width={100} height={140}/>
                        <p className="no-result__text">Результатов не найдено</p>
                    </div>
                    }
                </div>
            </div>
            {!loading && companyApplicationsList &&
            companyApplicationsList.page_count &&
            companyApplicationsList.page_count > 1 &&
            <CommonPagination
                count={companyApplicationsList.page_count}
                currentPage={page}
                onPageChange={(e, page) => setPage(page)}/>
            }
            {editingObject &&
            <ApplicationCompanyModal
                initValues={editingObject}
                type="edit"
                onClose={() => setObject(null)}/>
            }

            {selectedItem &&
            <ApplicationCompanyModal
                initValues={selectedItem}
                type="application"
                onClose={() => setItem(null)}/>}

            {!loading && companyFailure && <CommonAlert
                status={companyFailure.status}
                message={companyFailure.statusText}
            />}
        </>
    );
};

const mapStateToProps = state => ({
    companyApplicationsList: state.applicationsInfo.companyApplicationsList,
    companyFailure: state.applicationsInfo.companyFailure,
    notifications: state.applicationsInfo.notifications
})

const mapDispatchToProps = dispatch => ({
    fetchCompanyApplications: (page, status, search, date, is_archive) =>
        dispatch(fetchCompanyApplications(page, status, search, date, is_archive)),
    fetchEditApplicationStatus: (id, status, type) => dispatch(fetchEditApplicationStatus(id, status, type)),
    fetchEditApplicationArchive: (id, type) => dispatch(fetchEditApplicationArchive(id, type))
});

export default connect(mapStateToProps, mapDispatchToProps)(ApplicationsCompany);