import React, {useEffect, useRef, useState} from 'react';
import clsx from 'clsx';
import {makeStyles} from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import MoreIcon from "../../components/kit/Icons/MoreIcon";
import AdminHeader from "../../components/AdminHeader/AdminHeader";

import './index.scss';
import SidebarContent from "../../components/SideBar/SidebarContent";
import {history} from "../../store/configureStore";
import {ROUTE_ADVERTISE_PAGE, ROUTE_ADVERTISEMENT, ROUTE_ADVERTISEMENT_BLOGGERS} from "../../global/Routes";

const drawerWidth = 244;

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        minHeight: "100vh"
    },
    appBar: {
        transition: theme.transitions.create(['margin', 'width'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        backgroundColor: "#ffffff",
        boxShadow: "none",
        position: "absolute"
    },
    appBarShift: {
        width: `calc(100% - ${drawerWidth}px)`,
        marginLeft: drawerWidth,
        transition: theme.transitions.create(['margin', 'width'], {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    hide: {
        display: 'none',
    },
    drawer: {
        width: drawerWidth,
        flexShrink: 0,
    },
    drawerPaper: {
        width: drawerWidth,
        backgroundColor: "#33105E"
    },
    drawerHeader: {
        display: 'flex',
        alignItems: 'center',
        padding: "15px 0",
        // necessary for content to be below app bar
        ...theme.mixins.toolbar,
        justifyContent: 'flex-end',
    },
    content: {
        flexGrow: 1,
        padding: "7px 0",
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        marginLeft: -drawerWidth,
        background: "#F8F8FB"
    },
    contentShift: {
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
        }),
        marginLeft: 0,
    },
}));

const PAGE_SETTINGS = {
    "/dashboard": {
        title: "Главная",
        type: 1
    },
    "/applications/company": {
        title: "Заявки компании",
        type: 2
    },
    "/applications/bloggers": {
        title: "Заявки блогеров",
        type: 3
    },
    "/applications/archive": {
        title: "Архив отчетов",
        type: 4
    },
    "/bloggers": {
        title: "Блогеры",
        type: 5
    },
    "/advertisement": {
        title: "Рекламные кампании",
        type: 6
    },
    "/select_bloggers": {
        title: "Выбрать блогера",
        type: 7
    },
    "/rekcampaign": {
        title: "Рекламные кампании",
        type: 8
    },
    "/campaign": {
        title: "Рекламные кампании",
        type: 9
    },
    "/settings": {
        title: "Настройки",
        type: 10
    },
    "/report": {
        title: "Отчет",
        type: 11
    },
    default: {
        title: "Главная",
        type: 1
    }
}

function pageHeaderStrategy(path) {
    if (PAGE_SETTINGS[path]) {
        return PAGE_SETTINGS[path]
    } else if (path.includes(ROUTE_ADVERTISEMENT)) {
        return PAGE_SETTINGS[ROUTE_ADVERTISEMENT]
    } else if (path.includes(ROUTE_ADVERTISEMENT_BLOGGERS)) {
        return PAGE_SETTINGS[ROUTE_ADVERTISEMENT_BLOGGERS]
    } else if (path.includes(ROUTE_ADVERTISE_PAGE)) {
        return PAGE_SETTINGS[ROUTE_ADVERTISE_PAGE]
    } else {
        return PAGE_SETTINGS.default
    }
}


export default function Dashboard({children, ...props}) {
    const classes = useStyles();
    const [open, setOpen] = useState(true);
    const [isActive, toggleMenu] = useState(false);
    const handleDrawerOpen = () => {
        setOpen(true);
    };

    const handleDrawerClose = () => {
        setOpen(false);
    };

    function onOpenMenu() {
        toggleMenu(!isActive)
    }

    const wrapperRef = useRef(null);

    function useOutsideClosing(ref) {
        useEffect(() => {
            function handleClickOutside(event) {
                if (ref.current && !ref.current.contains(event.target)) {
                    toggleMenu(false)
                }
            }

            document.addEventListener("mousedown", handleClickOutside);
            return () => {
                document.removeEventListener("mousedown", handleClickOutside);
            };
        }, [ref]);
    }

    useOutsideClosing(wrapperRef);


    return (
        <div className={[classes.root, "dashboard"].join(" ")}>
            <AppBar
                position="fixed"
                className={clsx(classes.appBar, {
                    [classes.appBarShift]: open,
                })}
            >
                <Toolbar>
                    <IconButton
                        color="inherit"
                        aria-label="open drawer"
                        onClick={handleDrawerOpen}
                        edge="start"
                        className={clsx(classes.menuButton, open && classes.hide)}
                    >
                        <MoreIcon color="#33105E"/>
                    </IconButton>

                    <AdminHeader
                        title={pageHeaderStrategy(history.location.pathname).title}
                        type={pageHeaderStrategy(history.location.pathname).type}
                        {...props}
                    />

                </Toolbar>
            </AppBar>
            <Drawer
                className={classes.drawer}
                variant="persistent"
                anchor="left"
                open={open}
                classes={{
                    paper: classes.drawerPaper,
                }}
            >
                <div className={classes.drawerHeader}>
                    <IconButton onClick={handleDrawerClose}>
                        <MoreIcon/>
                    </IconButton>
                </div>
                <Divider/>
                <SidebarContent
                    isOpen={open}
                    wrapperRef={wrapperRef}
                    isActive={isActive}
                    toggleMenu={onOpenMenu}/>
                <Divider/>
            </Drawer>
            <main
                className={clsx(classes.content, {
                    [classes.contentShift]: open,
                })}
            >
                <div className={classes.drawerHeader}/>
                {children}
            </main>
        </div>
    );
}