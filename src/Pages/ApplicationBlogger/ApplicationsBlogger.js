import React, {useEffect, useState} from 'react';

import './index.scss';
import {NUMBER_REQUEST_STAT, TAB_NAMES} from "../ApplicationsCompany/ApplicationsCompany";
import HeaderFilter from "../../components/HeaderFilter/HeaderFilter";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import {
    fetchBloggerApplications, fetchEditApplicationArchive, fetchEditApplicationStatus,
} from "../../store/applications/applicationsActions";
import {connect} from "react-redux";
import {dateFormatter, isEmptyArray, useDebounce} from "../../global/helpers";
import CommonSpinner from "../../components/kit/CommonSpinner/CommonSpinner";
import CommonAlert from "../../components/CommonAlert/CommonAlert";
import TableBody from "@material-ui/core/TableBody";
import BloggerItem from "./BloggerItem";
import CommonPagination from "../../components/CommonPagination/CommonPagination";
import NoResultIcon from "../../components/kit/Icons/NoResultIcon";
import ApplicationBlogerModal from "../../components/AdminModal/Modals/ApplicationBlogerModal";
import BloggerModal from "../../components/AdminModal/Modals/BloggerModal/BloggerModal";

const ApplicationsBlogger = (
    {
        fetchBloggerApplications,
        bloggerApplications,
        bloggersFailure,
        fetchEditApplicationStatus,
        fetchEditApplicationArchive,

        notifications
    }) => {
    const [activeTab, setTab] = useState(0);
    const [selectedDate, setDate] = useState(null);
    const [search, setSearch] = useState("");
    const [page, setPage] = useState(1);
    const [editingObject, setObject] = useState(null);
    const [loading, setLoading] = useState(true);
    const [bloggerItem, setItem] = useState(null);

    const debouncedSearchTerm = useDebounce(search, 500);

    useEffect(function () {
        getApplications()
    }, [activeTab, page, debouncedSearchTerm, selectedDate])


    async function getApplications() {
        setLoading(true)
        await fetchBloggerApplications(
            page,
            TAB_NAMES[activeTab].code,
            debouncedSearchTerm,
            selectedDate ? dateFormatter(selectedDate) : null,
            false
        )
        setLoading(false)

    }

    function changeTab(ndx) {
        setTab(ndx)
        setPage(1)
    }

    async function onEditStatus(id, status) {
        if (status === "D") {
            const element = bloggerApplications.results.find(it => it.id === id);
            setItem(element)
        }
        await fetchEditApplicationStatus(id, status, "blogger");
    }

    async function sendToArchive(id) {
        await fetchEditApplicationArchive(id, "blogger");
        await getApplications()
    }

    function editApplication(item) {
        setObject(item)
    }

    function clearFilters() {
        setSearch("");
        setDate(null);
    }

    return (
        <>
            <div className="application-blogger">
                <div className="application-blogger__navs">
                    {TAB_NAMES.map((tab, ndx) => (
                        <button
                            key={ndx}
                            className={`application-blogger__nav
                             ${activeTab === ndx && "application-blogger__nav--active"}
                             ${(notifications && notifications.hasBlogger)
                            && "application-blogger__nav--red"}`}
                            onClick={() => changeTab(ndx)}>{tab.name}</button>
                    ))}
                </div>
                <HeaderFilter
                    selectedDate={selectedDate}
                    dateHandler={date => setDate(date)}
                    search={search}
                    inputHandler={e => setSearch(e.target.value)}
                    onClear={clearFilters}
                />

                <div className="application-blogger__table-root">
                    <Table className="application-blogger__table">
                        <TableHead>
                            <TableRow>
                                <TableCell align="left">ДАТА</TableCell>
                                <TableCell align="left">ID</TableCell>
                                <TableCell align="left">ФИО</TableCell>
                                <TableCell align="left">Соц.сети</TableCell>
                                <TableCell align="left">Телефон</TableCell>
                                <TableCell align="left">Статус</TableCell>
                                <TableCell align="left">Действия</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {!loading &&
                            bloggerApplications &&
                            Array.isArray(bloggerApplications.results) &&
                            bloggerApplications.results.sort((a, b) =>
                                NUMBER_REQUEST_STAT[a.request_status] - NUMBER_REQUEST_STAT[b.request_status]
                            ).map((item => (
                                <BloggerItem
                                    onChangeStatus={onEditStatus}
                                    onEdit={editApplication}
                                    onSendToArchive={sendToArchive}
                                    status={TAB_NAMES[activeTab].code}
                                    data={item}
                                    key={item.id}/>
                            )))}
                        </TableBody>
                    </Table>

                    {loading && <CommonSpinner isLoading={loading} centered/>}

                    {!loading && bloggerApplications &&
                    bloggerApplications.results &&
                    isEmptyArray(bloggerApplications.results) &&
                    <div className="no-result">
                        <NoResultIcon width={100} height={140}/>
                        <p className="no-result__text">Результатов не найдено</p>
                    </div>
                    }

                    {!loading && bloggerApplications &&
                    bloggerApplications.page_count &&
                    bloggerApplications.page_count > 1 &&
                    <CommonPagination
                        count={bloggerApplications.page_count}
                        currentPage={page}
                        onPageChange={(e, page) => setPage(page)}/>
                    }

                    {!loading && bloggersFailure && <CommonAlert
                        status={bloggersFailure.status}
                        message={bloggersFailure.statusText}
                    />}
                </div>
            </div>

            {editingObject &&
            <ApplicationBlogerModal
                initValues={editingObject}
                type="edit"
                onClose={() => setObject(null)}/>
            }

            {!!bloggerItem &&
            <BloggerModal item={bloggerItem} onClose={() => setItem(null)}/>}
        </>
    );
};

const mapStateToProps = state => ({
    bloggerApplications: state.applicationsInfo.bloggerApplications,
    bloggersFailure: state.applicationsInfo.bloggersFailure,
    notifications: state.applicationsInfo.notifications
})

const mapDispatchToProps = dispatch => ({
    fetchBloggerApplications: (page, status, search, date, is_archive) =>
        dispatch(fetchBloggerApplications(page, status, search, date, is_archive)),
    fetchEditApplicationStatus: (id, status, type) => dispatch(fetchEditApplicationStatus(id, status, type)),
    fetchEditApplicationArchive: (id, type) => dispatch(fetchEditApplicationArchive(id, type))
})

export default connect(mapStateToProps, mapDispatchToProps)(ApplicationsBlogger);