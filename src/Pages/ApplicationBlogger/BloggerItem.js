import React from 'react';
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import StatusSplitButton from "../../components/StatusSplitButton/StatusSplitButton";
import ActionButton from "../../components/ActionButton/ActionButton";
import './index.scss';

const BloggerItem = (
    {
        onChangeStatus,
        onEdit,
        onSendToArchive,
        data,
        onRemove,
        onSendToApplication
    }) => {
    return (
        <TableRow className={`application-blogger__table-row`}>
            <TableCell align="left">{data.created_date}</TableCell>
            <TableCell align="left">{data.id}</TableCell>
            <TableCell align="left">{data.surname} {data.name} {data.middle_name}</TableCell>
            <TableCell align="left">
                {data.social_network &&
                data.social_network.sort((a, b) => a.social_network - b.social_network)
                    .map(social => (
                        <img
                            key={social.id}
                            className="application-blogger__table-social-img"
                            src={social.social_network_image}
                            alt="#"
                            width={20}
                            height={20}
                        />
                    ))}
            </TableCell>
            <TableCell align="left">{data.phone_number}</TableCell>
            <TableCell align="left">
                <StatusSplitButton
                    onChangeStatus={onChangeStatus}
                    initStatus={data.request_status}
                    disabled={data.request_status === "D"}
                    applicationId={data.id}
                />
            </TableCell>
            <TableCell align="left">
                <ActionButton
                    sendToApplication={() => onSendToApplication(data.id, "blogger")}
                    onSendToArchive={() => onSendToArchive(data.id, "blogger")}
                    onRemove={() => onRemove(data.id, "blogger")}
                    onEdit={() => onEdit(data)}
                    editable={!!onEdit}
                />
            </TableCell>
        </TableRow>
    );
};

export default BloggerItem;