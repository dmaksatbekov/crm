import React, {useState} from 'react';
import './index.scss';
import InputForm from "../../components/kit/InputForm/InputForm";
import GeneralButton from "../../components/kit/GeneralButton/GeneralButton";
import {Formik} from "formik";
import {fetchUserAuthorization} from "../../store/user/userActions";
import {connect} from "react-redux";
import InputMask from "react-input-mask";

const Login = (
    {fetchUserAuthorization, userError}
) => {

    const [state, _] = useState({username: "", password: ""});

    function validateForm(values) {
        const errors = {};
        if (!values.username) {
            errors.username = 'Обязательное поле';
        }
        if (!values.password) {
            errors.password = 'Обязательное поле';
        }
        return errors;
    }

    function onSubmit(values) {
        fetchUserAuthorization(values)
    }

    return (
        <div className="login-root">
            <Formik
                initialValues={state}
                validate={values => validateForm(values)}
                onSubmit={(values) => {
                    onSubmit(values)
                }}
            >
                {
                    ({
                         values,
                         errors,
                         touched,
                         handleChange,
                         dirty,
                         handleSubmit,
                     }) => (
                        <form className="login-root__form" onSubmit={handleSubmit}>
                            <div className="login-root__date-mask">
                                <p style={
                                    {
                                        color: (errors.username &&
                                        touched.username &&
                                        errors.username) ? "#FA445C" : (!errors.username && values.username) ? "#24AF47" : "#1A051D"
                                    }
                                }>Логин</p>
                                <InputMask
                                    mask="\9\96999999999"
                                    maskChar={null}
                                    name="username"
                                    onChange={handleChange}
                                    className={`custom-input ${values.username ? "--green" : ""}`}
                                    value={values.username}
                                    placeholder=" +996 000-000-000"
                                />
                                {errors.username && touched.username && errors.username &&
                                <span className="error-text">{errors.username}</span>
                                }
                                {!errors.username && userError && userError.non_field_errors &&
                                <span className="error-text">{userError.non_field_errors}</span>
                                }
                            </div>

                            <InputForm
                                label="ПАРОЛЬ"
                                propertyName="password"
                                value={values.password}
                                onChange={handleChange}
                                type="password"
                                isError={errors.password && touched.password && errors.password}
                                helperText={errors.password && touched.password && errors.password}
                            />
                            {!errors.password && userError && userError.non_field_errors &&
                            <span className="error-text">
                                {userError.non_field_errors}
                            </span>
                            }

                            <GeneralButton
                                type="submit"
                                className="modal-form__btn"
                                variant="outlined">
                                Войти
                            </GeneralButton>
                        </form>
                    )
                }
            </Formik>
        </div>
    );
};

export const mapStateToProps = state => ({
    userError: state.user.userError
})

export const mapDispatchToProps = dispatch => ({
    fetchUserAuthorization: data => dispatch(fetchUserAuthorization(data))
})

export default connect(mapStateToProps, mapDispatchToProps)(Login);