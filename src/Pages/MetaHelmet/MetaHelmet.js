import React from 'react';
import {Helmet} from "react-helmet";

const MetaHelmet = ({metaData}) => {
    return (
        <Helmet>
            <script type="text/javascript">
                {
                    `(function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
                m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
                (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

                ym(62584933, "init", {
                clickmap:true,
                trackLinks:true,
                accurateTrackBounce:true,
                webvisor:true
            })`
                }
            </script>
            <script async src="https://www.googletagmanager.com/gtag/js?id=UA-166345826-1"></script>
            <script>
                {
                    `window.dataLayer = window.dataLayer || [];
                function gtag(){dataLayer.push(arguments);}
                gtag('js', new Date());

                gtag('config', 'UA-166345826-1');`
                }
            </script>
            {metaData && metaData.map(it => {
               return Object.keys(it).map(key => (
                    <meta name={key} content={it[key]}/>
                ))
            })}
        </Helmet>
    );
};

export default MetaHelmet;