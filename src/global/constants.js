export const ERROR_TEXT = "Не удалось выполнить запрос, попробуйте позднее"

export const EXCLUSIVE_STATUS_NAME = "Exclusive";
export const GENDER_TYPES = {
    M: "Мужчина",
    F: "Женщина"
}

export const AGREEMENT_STATUS = "A";
export const REJECTED_STATUS = "R";
export const WAITING_STATUS = "N";

export const ANSWER_TZ_STATUSES = {
    'A': 'Согласен',
    'R': 'Отказано',
    'N': 'Нет ответа'
}

export const TAG_COLOR_VARIANTS = [
    "#FFFFFF",
    "#7400FF",
    "#3DD5FF",
    "#2C59E5",
    "#FCD816",
    "#FBBD0E",
    "#24AF47",
    "#D600FF",
    "#0099FF",
    "#FA445C",
    "#110421"
]

export const CURRENT_DATE = new Date()