import React from 'react';
import Landing from "../Pages/Landing/Landing";
import {Redirect, Route, Switch} from "react-router-dom";
import Admin from "../Pages/Admin/Admin";
import BloggerOffer from "../Pages/BloggerOffer/BloggerOffer";
import Login from "../Pages/Login/Login";

export const ROUTE_ADMIN = '/dashboard'
const ROUTE_LANDING = '/';
export const ROUTE_ADVERTISEMENT = '/advertisement';
export const ROUTE_ADVERTISEMENT_BLOGGERS = '/select_bloggers';
export const ROUTE_APPLICATIONS_COMPANY = '/applications/company';
export const ROUTE_APPLICATIONS_BLOGGERS = '/applications/bloggers';
export const ROUTE_APPLICATIONS_ARCHIVE = '/applications/archive';
export const ROUTE_BLOGGERS = '/bloggers';
export const ROUTE_ADVERTISEMENTS = '/rekcampaign';
export const ROUTE_ADVERTISE_PAGE = '/campaign'
export const ROUTE_SETTINGS = '/settings'
export const ROUTE_REPORT = '/report'
export const ROUTE_BLOGGER_TZ = '/blogger_offer/:offerId'
export const ROUTE_AUTH = '/login'

const ProtectedRoute = ({isAllowed, ...props}) => isAllowed ? <Route {...props} /> : <Redirect to="/login"/>;


const Routes = ({user}) => {

    return (
        <Switch>
            <Route path={ROUTE_LANDING} exact component={Landing}/>
            <Route path={ROUTE_AUTH} exact component={Login}/>

            <ProtectedRoute
                isAllowed={true}
                path={ROUTE_BLOGGER_TZ} exact component={BloggerOffer}
            />

            <ProtectedRoute
                isAllowed={!!user}
                path={ROUTE_ADMIN} exact component={Admin}
            />
            <ProtectedRoute
                isAllowed={!!user}
                path={ROUTE_ADVERTISEMENT + "/:advertisementId"} exact component={Admin}
            />
            <ProtectedRoute
                isAllowed={!!user}
                path={ROUTE_ADVERTISEMENT_BLOGGERS + "/:advertisementId"}
                exact component={Admin}
            />

            <ProtectedRoute
                isAllowed={!!user}
                path={ROUTE_ADVERTISE_PAGE + "/:advertisementId"}
                exact component={Admin}
            />
            <ProtectedRoute
                isAllowed={!!user}
                path={ROUTE_APPLICATIONS_COMPANY}
                exact component={Admin}
            />
            <ProtectedRoute
                isAllowed={!!user}
                path={ROUTE_APPLICATIONS_BLOGGERS}
                exact component={Admin}
            />
            <ProtectedRoute
                isAllowed={!!user}
                path={ROUTE_APPLICATIONS_ARCHIVE}
                exact component={Admin}
            />
            <ProtectedRoute
                isAllowed={!!user}
                path={ROUTE_BLOGGERS}
                exact component={Admin}
            />
            <ProtectedRoute
                isAllowed={!!user}
                path={ROUTE_ADVERTISEMENTS}
                exact component={Admin}
            />
            <ProtectedRoute
                isAllowed={!!user}
                path={ROUTE_SETTINGS}
                exact component={Admin}
            />
            <ProtectedRoute
                isAllowed={!!user}
                path={ROUTE_REPORT}
                exact component={Admin}
            />

        </Switch>
    );
};

export default Routes;